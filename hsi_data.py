# -*- coding: utf-8 -*-
"""
Created on Wed Apr 20 20:04:55 2016
HSI_IO class

Rev 0.1 - 13-Sept15
This is a snap shot to be submitted with the thesis.

This class will be used to:
    capture,
    filter,
    standardize
    and
    write the
hyper spectral imagery (HSI) data

@author: dgiudici
"""
import csv
import re
import logging
import numpy as np
from sklearn import preprocessing

#logging.basicConfig(filename='LOG_hsi_data_io.txt',level = logging.DEBUG, format= '%(asctime)s - %(levelname)s - %(message)s')
logging.debug('imported hsi_data_io')


########################################
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
##Non-MemberFunctions
#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
########################################
def randomize(dataset, labels):
    """ Randomize dataset and labels
    These data will be shuffeled in place and then returned
    Args:
        dataset and labels: each row corresponds to a peice of data
    Returns:
        shuffled data
    """
    print ">>>>>>>> randomize data <<<<<<<<"
    permutation = np.random.permutation(labels.shape[0])
    shuffled_dataset = dataset[permutation,:]
    shuffled_labels = labels[permutation]
    return shuffled_dataset, shuffled_labels
def labels_to_num(labels, set_classes):
    """ Convert labels_to_numbers
    This function will convert a label back to a number based off of the sorted
    set of classes

    Args:
        labels to convert
        sorted set of classes
    Return
        the coresponding number that the label matches to the sorted set of
        labels
    """
    print ">>>>>>>>randomize data<<<<<<<<"
    sortedSuperSetLabels = list(set_classes)
    labels_arry = np.asarray(labels)
    dict_idx2cls = {x:sortedSuperSetLabels[x] for x in range(len(sortedSuperSetLabels))}
    dict_cls2idx = dict(zip(dict_idx2cls.values(),dict_idx2cls.keys()))
    labels_num = np.asarray([dict_cls2idx[labels_arry[x]] for x in range(labels_arry.shape[0])],dtype=np.float32)
    labels_num = np.int64(labels_num)
    return labels_num

def num_to_label(nums, sorted_classes):
    """This function converts a number to the representative sorted_class
    Stated explicitly for clarity.
    """
    print ">>>>>>>>convert num to label based on classes<<<<<<<<"
    a = [sorted_classes[nums[i]] for i in range(len(nums))]
    return a

def reformatOneHot(labelsAsNum,num_labels):
    """ reformat labels corresponding to the number of labels
    The input labels (represented in number format) will be broken down to
    one hot labels
    ie. labelsAsNum = 0 & num_labels = 3
        return [1 0 0]
    """
    print ">>>>>>>>format to one hot<<<<<<<<"
    #dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
    # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
    labelsOneHot = (np.arange(num_labels) == labelsAsNum[:,None]).astype(np.float32)
    print type(labelsOneHot)
    return labelsOneHot

########################################
#<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
##Class Definition
#>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
########################################
class hsi_data:
    """
        HSI_IO - Performs the basic IO functions of reading, prepping the data
        from a .csv file. The end result is to provide an prepped data array
        and labels array.
    """
    ####################################
    ####################################
    ##Class Variables
    ####################################
    ####################################

    data_desc = 'This field should hold a description of the data'
    input_filename = 'input_filename.csv'   #This file should either be the
                                            #absolute path or located locally
                                            #to the main thread
    output_filename = 'output_filename.csv' #This file should either be the
                                            #absolute path or located locally
                                            #to the main thread
    data = []               #Holds the data
    labels = []             #Holds the labels
    num_data = []           #Number of data points
    num_labels = []         #Number of labels len(set(labels))
    data_length = 0         #Length of an individual data point (as 1-d vector) data.shape[0]
    good_data_idx_lst = []  #This is the index into the .csv file for which are the bands of interest
    set_labels = []         #This is the sorted array of the set_labels = np.set(labels)
    wavelengths = []        #This creates the matching wavelength for each data element
    header = []
    scaler = []             #This is the scaler value used for scaling the data set
    standardized = False    #If the data has been standardized set to true (this occurs in the standardize function)
    class_header_label = 'VegClass'

    var1 = []   #Debug Variables
    var2 = []   #Debug Variables
    var3 = []   #Debug Variables
    var4 = []   #Debug Variables
    ####################################
    ##GDAL members - defaults
    hyspiri_filename = []   # 'BayArea_48-1_f150430t01p00r06_f150611t01p00r20_f151002t01p00r06_masked_30m'
    hyspiri_filepath = []   # '/media/dgiudici/Seagate Backup Plus Drive/raw_2015_data/'

    ####################################
    ####################################
    ##MEMBER FUNCTIONS
    ####################################
    ####################################
    def open_training_file(self,in_filename,n_seasons):
        """
        This will open a single or three season training data file.

        n_seasons needs to be either [1 or 3]

        This function will return the respective data:
            data_array and the labels_array as represented within the file
            (additional post processing of the data to convert to numbers and
            one hot representations may be required)
        """
    def load_data_any_1season(self,fileName):
        """ This function loads the data set from a file
        Currently the dataset can be 'train' or 'valid' basically loading data
        from either:
            self.data_path+self.train_filename or
            self.data_path+self.valid_filename
        the data will be put into:
            self.train_data_arry and self.train_labels or
            self.valid_data_arry and self.valid_labels
        and a corresopnding set of labels will be created for each set of data
            self.valid_set or self.train_set
        """
        #%% TRAINING DATASETS
        print ">>>>>>>>>>READ IN A DATASET<<<<<<<<<<"
        print "READING IN:", fileName

        file = open(fileName)
        reader = csv.reader(file)
        header = reader.next()
        dataDB = []

        #locate the VegClass col
        element = 'VegClass'
        if element in header:
            print 'VegClass Index: ', header.index(element)
            VegClassIdx = header.index(element)
        self.var1 = 0
        #dataDB.append(header[0:5]+header[191:377]+header[VegClassIdx:VegClassIdx+1])                                   #single season (spring)
        dataDB.append(header[0:5]+header[229:325]+header[345:382]+header[400:453]+header[VegClassIdx:VegClassIdx+1])    #single season (summer)
        header2 = header[0:5]+header[229:325]+header[345:382]+header[400:453]+header[VegClassIdx:VegClassIdx+1]
        #raw_input("Press Enter to continue...")
        for data in reader:
            if(re.search('NA|Inf|-Inf|1.#INF|Quercus spp.|Open|Mixed|Fresh',str(data)) != None ): #pass on any bad data
                self.var1 +=1
            else:
                #print 'data 0:5 + data[191:377]', data[0:5] + data[191:377]
                #print 'dataDB', dataDB
                #raw_input("Press Enter to continue...")
                #bad bands = sum97-sum116 and sum154-sum171
                #good indexes = sum1:229-sum96:325 and sum117:345-sum153:382 and sum172:400-sum223:452
                dataDB.append(data[0:5]+data[229:325]+data[345:382]+data[400:453]+data[VegClassIdx:VegClassIdx+1])
        data = [item[5:-1] for item in dataDB]
        self.head = header[5:-1]
        labels = [item[-1] for item in dataDB] #grab the last item in the array
        #remove the first element the label of the class
        data.pop(0);
        labels.pop(0);
        #Convert the data to arrays
        data_arry = np.asarray(data, dtype = float)
        labels_arry = np.asarray(labels, dtype = str)

        #################################
        #Create matching wavelengths for each captured element (single season)
        a = np.arange(365.913659,2496.22365,(2496.22365-365.913659)/224)
        #a = [a]*3
        b = np.reshape(np.asarray(a),np.asarray(a).shape[0])
        a = list(b)
        wavelengths = np.asarray(a[0:96]+a[116:153]+a[171:224])
                            #+a[224:320]+a[340:377]+a[395:448]\
                            #+a[448:544]+a[564:601]+a[619:672])
        labels = [item[-1] for item in dataDB] #grab the last item in the array

        data.pop(0);            #remove the first element the label of the class
        labels.pop(0);          #remove the first element the label of the class

        data_array = np.asarray(data, dtype = float)     #Convert the data to arrays
        labels_array = np.asarray(labels, dtype = str)
        #################################
        # PRINT SOME INFORMATION ABOUT THE DATASET
        print 'shape of data', data_array.shape
        print 'shape of labels', labels_array.shape
        self.set_labels = np.sort(list(set(labels)))
        print 'len(set_labels)', len(self.set_labels)
        for line in self.set_labels:
            print ' --- ', line
        ################################
        #Set information about the data set
        self.data = data_array
        self.labels = labels_array
        self.header = header2
        self.wavelengths = wavelengths
        self.data_length = self.data.shape[1]
        self.num_labels = len(self.set_labels)
        self.num_data = self.data.shape[0]

        return data_arry, labels_arry,header2,wavelengths
    def load_data_any_3season(self,fileName):
        """ This function loads the data set from a file
        Currently the dataset can be 'train' or 'valid' basically loading data
        from either:
            self.data_path+self.train_filename or
            self.data_path+self.valid_filename
        the data will be put into:
            self.train_data_array and self.train_labels or
            self.valid_data_array and self.valid_labels
        and a corresopnding set of labels will be created for each set of data
            self.valid_set or self.train_set
        """
        print ">>>>>>>>load 3 season data<<<<<<<<"

        file = open(fileName)
        reader = csv.reader(file)
        header = reader.next()
        dataDB = []

        #locate the VegClass col
        element = 'VegClass'
        if element in header:
            #print 'VegClass Index: ', header.index(element)
            VegClassIdx = header.index(element)
        self.var1 = 0
        #dataDB.append(header[0:5]+header[191:377]+header[VegClassIdx:VegClassIdx+1])
        dataDB.append(header[0:5]+header[5:101]+header[121:158]+header[176:229]\
                            +header[229:325]+header[345:382]+header[400:453]\
                            +header[453:549]+header[569:606]+header[624:677]\
                            +header[VegClassIdx:VegClassIdx+1])
        header2 = header[0:5]+header[5:101]+header[121:158]+header[176:229]\
                            +header[229:325]+header[345:382]+header[400:453]\
                            +header[453:549]+header[569:606]+header[624:677]\
                            +header[VegClassIdx:VegClassIdx+1]

        #raw_input("Press Enter to continue...")
        for data in reader:
            if(re.search('NA|Inf|-Inf|1.#INF|Quercus spp.|Open|Mixed|Fresh',str(data)) != None ): #pass on any bad data
                self.var1 +=1
            else:
                #print 'data 0:5 + data[191:377]', data[0:5] + data[191:377]
                #print 'dataDB', dataDB
                #raw_input("Press Enter to continue...")
                #bad bands = sum97-sum116 and sum154-sum171
                #good indexes = sum1:229-sum96:325 and sum117:345-sum153:382 and sum172:400-sum223:452
                dataDB.append(data[0:5]+data[5:101]+data[121:158]+data[176:229]\
                            +data[229:325]+data[345:382]+data[400:453]\
                            +data[453:549]+data[569:606]+data[624:677]\
                            +data[VegClassIdx:VegClassIdx+1])

        data = [item[5:-1] for item in dataDB]
        self.head = header2[5:-1]

        #################################
        #Create matching wavelengths for each captured element
        a = np.arange(365.913659,2496.22365,(2496.22365-365.913659)/224)
        a = [a]*3
        b = np.reshape(np.asarray(a),np.asarray(a).shape[0]*np.asarray(a).shape[1])
        a = list(b)
        wavelengths = np.asarray(a[0:96]+a[116:153]+a[171:224]\
                            +a[224:320]+a[340:377]+a[395:448]\
                            +a[448:544]+a[564:601]+a[619:672])
        labels = [item[-1] for item in dataDB] #grab the last item in the array

        data.pop(0);            #remove the first element the label of the class
        labels.pop(0);          #remove the first element the label of the class

        data_array = np.asarray(data, dtype = float)     #Convert the data to arrays
        labels_array = np.asarray(labels, dtype = str)

        #################################
        # PRINT SOME INFORMATION ABOUT THE DATASET
        print 'shape of data', data_array.shape
        print 'shape of labels', labels_array.shape
        self.set_labels = np.sort(list(set(labels)))
        print 'len(set_labels)', len(self.set_labels)
        for line in self.set_labels:
            print ' --- ', line

        ################################
        #Set information about the data set
        self.data = data_array
        self.labels = labels_array
        self.header = header2
        self.wavelengths = wavelengths
        self.data_length = self.data.shape[1]
        self.num_labels = len(self.set_labels)
        self.num_data = self.data.shape[0]
        return data_array, labels_array,header2,wavelengths

    def standardize(self,scaler = 0):
        """This function applies a scaler and "smooths" the data
        with a 3 tap box car filter
        """
        print ">>>>>>>>standardize data<<<<<<<<"
        if scaler == 0 or self.scaler == 0 :
            #if there isn't a scaler yet create one (only for training)
            self.scaler = preprocessing.StandardScaler().fit(self.data)
        else:
            if self.scaler == 0:
                self.scaler = scaler
            else:
                scaler = self.scaler

        self.data = self.scaler.transform(self.data)
        N = 3
        self.data = np.asarray([np.convolve(self.data[i],np.ones((N,))/N,mode='same') for i in range(self.data.shape[0])])
        self.standardized = True
        return self.data,scaler

    def write_file(self,out_filename,additional_data):
        """
        This function will set the output_filename and
        write the additional_data to the input data
        based on the filter_remove_class
        and the filter_keep_class dictionaries
            ie. only rows of data that match these criteria
            will data be appended onto.

        additional_data should contain the header as the
        first row
        """
    def create_variants_labels(self):
        print ">>>>>>>>create label variants : num/one_hot<<<<<<<<"
        self.labels_num = labels_to_num(self.labels,self.set_labels)
        self.labels_one_hot = reformatOneHot(self.labels_num,self.num_labels)
        return



    def remove_bad_bands_hyspiri(self,input_array):
        output_array = np.hstack((input_array[:,0:96],input_array[:,116:153]))
        output_array = np.hstack((output_array,input_array[:,171:224]))

        first_season = output_array

        output_array =  np.hstack((output_array,input_array[:,224:224+96]))
        output_array  =  np.hstack((output_array ,input_array[:,224+116:224+153]))
        output_array  =  np.hstack((output_array ,input_array[:,224+171:224+224]))


        output_array  =  np.hstack((output_array ,input_array[:,448:448+96]))
        output_array  =  np.hstack((output_array ,input_array[:,448+116:448+153]))
        output_array  =  np.hstack((output_array ,input_array[:,448+171:448+224]))


        second_season =  input_array[:,224:224+96]
        second_season  =  np.hstack((second_season ,input_array[:,224+116:224+153]))
        second_season  =  np.hstack((second_season ,input_array[:,224+171:224+224]))

        third_season  =  input_array[:,448:448+96]
        third_season  =  np.hstack((third_season ,input_array[:,448+116:448+153]))
        third_season  =  np.hstack((third_season ,input_array[:,448+171:448+224]))

        return output_array,first_season,second_season,third_season #bad bands removed

    def filter_remove(self,idx,field_val):#likely done in open_file
        """
        This function will filter out the data based on an index value
        and remove this data from the .data
            -Specifically created to filter train from test datasets

            - This function will set filter_keep_class dict and should look
            something like filter_keep_class = {38:'train'} indicating that
            at index 38 within the .csv file we are only keeping items that
            are contain 'train'
        """
        return
    def filter_keep(self,idx,field_val): #likely done in open_file
        """
        This function will filter out the data based on an index value
        and keep the data within .data
            -Specifically created to filter train from test datasets

            - This function will set filter_keep_class dict and should look
            something like filter_keep_class = {38:'train'} indicating that
            at index 38 within the .csv file we are only keeping items that
            are contain 'train'
        """
        return
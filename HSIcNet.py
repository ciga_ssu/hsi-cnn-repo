# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 11:27:19 2016

Rev 0.1 - 13-Sept15
This is a snap shot to be submitted with the thesis.

@author: dgiudici

This class will capture the functionality
of the HSI convolutional neural network classifier
it will:


    Graph Definition - in a class context
        -[level set at definition] Dropout
        -[level set at definition] regularization at each level
        -[level set at definition] pooling
        -[level set at definition] num kernels/size of kernels
        -[level set at definition] weight decay
    Graph Training
    Graph running - classification
        - Open session that needs to be closed externally (good for large files)
        - Scopped session that will open and close within its own function call.

It relies on the hsi_data_io class to take in data

"""
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range
import time as time
import logging
import matplotlib.pyplot as plt
from matplotlib import cm as cm
import gdal
from gdalconst import *
import hsi_data as hsi_data
import sys
import os


#logging.basicConfig(filename='LOG_HSIcNet.txt',level = logging.DEBUG, format= '%(asctime)s - %(levelname)s - %(message)s')
logging.debug('imported hsicnet classifier class')
from matplotlib.colors import Normalize
class MidpointNormalize(Normalize):
    def __init__(self, vmin=None, vmax=None, midpoint=None, clip=False):
        self.midpoint = midpoint
        Normalize.__init__(self, vmin, vmax, clip)

    def __call__(self, value, clip=None):
        # I'm ignoring masked values and all kinds of edge cases to make a
        # simple example...
        x, y = [self.vmin, self.midpoint, self.vmax], [0, 0.5, 1]
        return np.ma.masked_array(np.interp(value, x, y))


def accuracy(pred, labels):
    """ extract the accuracy from prediction against truthlabels
    This function "counts"/sums where the argmax of each of the predictions
    match with the labels, divides this from the total and multiplies by 100.
    """
    pred = np.asarray(pred)

    accuracy = (100.0 * np.sum( np.argmax(pred,axis = 1) == np.argmax(labels, axis = 1)) / float(pred.shape[0]) )
    return accuracy

def labels_to_num(labels, set_classes):
    """ Convert labels_to_numbers
    This function will convert a label back to a number based off of the sorted
    set of classes

    Args:
        labels to convert
        sorted set of classes
    Return
        the coresponding number that the label matches to the sorted set of
        labels
    """

    sortedSuperSetLabels = list(set_classes)
    print ">>>>>>>>>>TEXT LABELS TO NUMBERS Function<<<<<<<<<<"
    labels_arry = np.asarray(labels)
    dict_idx2cls = {x:sortedSuperSetLabels[x] for x in range(len(sortedSuperSetLabels))}
    dict_cls2idx = dict(zip(dict_idx2cls.values(),dict_idx2cls.keys()))
    labels_num = np.asarray([dict_cls2idx[labels_arry[x]] for x in range(labels_arry.shape[0])],dtype=np.float32)
    labels_num = np.int64(labels_num)
    return labels_num
def reformatOneHot(labelsAsNum,num_labels):
    """ reformat labels corresponding to the number of labels
    The input labels (represented in number format) will be broken down to
    one hot labels
    ie. labelsAsNum = 0 & num_labels = 3
        return [1 0 0]
    """
    #dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
    # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
    labelsOneHot = (np.arange(num_labels) == labelsAsNum[:,None]).astype(np.float32)
    print type(labelsOneHot)
    return labelsOneHot

def kappa(data):
        """ KAPPA function metric
        To remove the probability that the classification could have happend by chance.
        The kappa function removes this from the calculation

        to use this function provide the kappa function with the confusion matrix counts
        with truth on the left hand side and predicted classes in the columns
                      <--------+predictied class+--------------------->
                ^    +--------------------------------------------------+
                |    |--------------------------------------------------|
                +    |--------------------------------------------------|
          truth class|------data----------------------------------------|
                +    |--------------------------------------------------|
                |    |--------------------------------------------------|
                v    |--------------------------------------------------|
                     +--------------------------------------------------+
        """
        row_sum = np.sum(data,axis=1)
        col_sum = np.sum(data,axis=0)
        pe = 0
        den = np.square(float(np.sum(row_sum)))
        for x in range(len(row_sum)):
            pe += row_sum[x]*col_sum[x]
        pe = pe/den
        p0 = np.sum(np.diag(data))/float(np.sum(np.sum(data,axis=1)))
        kappa = (p0-pe)/(1-pe)

        return kappa


class HSIcNet:
    logging.debug('Created HSIcNet')
    """ HSIcNet - HyperSpectralImagery Convolutional Network
    This class captures:
        definition of graph/architecture for the convlutional network
        saving of graph and reloading of said graph
        training of graph
        saving of graph
        opening of computational session
        closing of computational session
        classification of data (under open and closed sessions)
        visualizatoin methods
            confusion matrix (balanced)
            confusion matrix (unbalanced)
            kernel impact/importance matrix
    """

    #Network Hyper Parameters
    #-------These values are defaults and should be changed at the discretion of the
    #       Implementer.
    imported_params = []    #when capturing graph params from saved file they will be saved here

    num_kernel = 20         #The default number of kernels (this should be tuned)
    kernel_size = 9         #The size of the local receptive field of the convolutional layer
    num_hidden = 500        #The default number of hidden nodes
    reg_level_hidden = 1e-2 #This is the regularization level for the hidden layer
    reg_level_kernel = 1e-2 #<== Not verified
    dropout_level = .5      #50% dropout rate
    learning_rate = .1      #this is going to be over written with exponential learning rate decay
    pooling = True          #is pooling used in this network (default = true) * this is more informational (DO NOT CHANGE WITH OUT CHANGING ARCH)
    threeseason = False     #does the data contain 3 season data
    standardized = False    #is standardization used on this data set
    num_steps = 2000000     #the training is controlled by the number of steps performed
                            #   there is a balance between this and batch size.
    batch_size = 20         #this is the number of data points applied to the graph per epoch
    data_length = 186       #this is the size of the input vector (default is single season)
    restore_graph = False   #By default we will not restore a graph and will train a new network from scratch
    init_lrn_rate = .1      #Exponential learning Weight Decay is used in this network for training
    train_size = 1000000    #This determines how fast the learning factor decays

    HyperParamNote = ''     #This note will be added to the graph params file add contextual note

    run_descriptor = ''
    def get_run_desc(self): #To capture a unique name for the graph this will concatenate the parameters and an abreviated name for it
        """ This function encapsulates characteristics from the run
        It main purpose is for labeling model/graphics and other files
        that are extracted from the class.
        """
        self.run_descriptor = str(     'nk' + str(self.num_kernel) \
                            + '_ks' + str(self.kernel_size) \
                            + '_nh' + str(self.num_hidden) \
                            + '_ns' + str(int(self.num_steps)) \
                            + '_rlh' + str(self.reg_level_hidden) \
                            + '_rlk' + str(self.reg_level_kernel) \
                            + '_dol' + str(self.dropout_level) \
                            + '_lr' + str(self.learning_rate) \
                            + '_p' + str(self.pooling)\
                            + '_ts' + str(self.threeseason))
        return self.run_descriptor

    #Data Parameters
    scaler = []             #This is the scalar for the standardization of the data
    head = []               #This is the header from the training file
    wavelength = []         #This is the wavelength map generated by the hsi_data_io class
    num_training_data = []  #Number of training data points - utilized for training
    num_validation_data = []#Number of validation data points - gauge training performance
    num_test_data=[]        #Number of test data points - gauge overall performance [due to limited data not used here]
    num_classes = []        #number of classes

    #Directory Paths
    data_path =         '/home/ciga/CNN-Project/hsi-cnn-repo/TrainingRuns/TensorflowModels'  #This should be the directory of the training data
    model_path =        '/home/ciga/CNN-Project/hsi-cnn-repo/TrainingRuns/TensorflowModels' #Location to store the trained model
    model_params_path = '/home/ciga/CNN-Project/hsi-cnn-repo/TrainingRuns/TensorflowModels' #This should be colocated with the model
    kernel_path=        '/home/ciga/CNN-Project/hsi-cnn-repo/TrainingRuns/TensorflowModels'#This is the location to extract the kernels to

    data_path =         '/home/dgiudici/repos/hsi-cnn-repo/TrainingRuns/TensorflowModels'  #This should be the directory of the training data
    model_path =        '/home/dgiudici/repos/hsi-cnn-repo/TrainingRuns/TensorflowModels' #Location to store the trained model
    model_params_path = '/home/dgiudici/repos/hsi-cnn-repo/TrainingRuns/TensorflowModels' #This should be colocated with the model
    kernel_path=        '/home/dgiudici/repos/hsi-cnn-repo/TrainingRuns/TensorflowModels'#This is the location to extract the kernels to
    current_model_path =''

    #VARIABLES USED IN NON-TENSORFLOW VARIABLE SPACE
    pred_lst = []
    vpred_lst = []
    loss_lst = []
    kernels = []
    hid_weights = []
    conv1 = []

    full_train_data = np.asarray([])
    train_data_3season = np.asarray([])
    train_data = np.asarray([])
    train_labels = np.asarray([])
    train_labels_num = np.asarray([])
    train_labels_one_hot = np.asarray([])
    train_set = np.asarray([])
    sorted_train_set = set([])

    full_valid_data = np.asarray([])
    valid_data_3season = np.asarray([])
    valid_data = np.asarray([])
    valid_labels = np.asarray([])
    valid_labels_num = np.asarray([])
    valid_labels_one_hot = np.asarray([])
    valid_set = np.asarray([])
    sorted_valid_set = set([])

    num_labels = 0
    full_data = np.asarray([])      #this should be removed
    full_labels = np.asarray([])    #this should be removed
    sorted_full_set = set([])
    sorted_full_labels = np.asarray([])  #this will hold the data's labels that is
                                         #sorted
    sorted_full_labels_num = np.asarray([])
    sorted_full_one_hot_labels = np.asarray([])
    sorted_data = np.asarray([])    #this will hold the labels that are sorted

    dict_idx2cls ={}
    dict_cls2idx ={}

    flt_train_data = []
    flt_train_labels = []
    flt_train_labels_num = []
    flt_train_labels_one_hot = []

    var1 = []
    var2 = []
    var3 = []
    var4 = []

    #TENSORFLOW DEFINITIONS
    graph = []# do not define graph here .. .other wise breaks linear param search tf.Graph()
    tf_ph_dataset = tf.placeholder(tf.float32,shape=(batch_size, data_length,1,1))
    tf_ph_dataset = tf.placeholder(tf.float32,shape=(batch_size, data_length,1,1))
    tf_ph_labels = tf.placeholder(tf.float32, shape=(batch_size, num_labels))
    tf_loss = tf.Variable(1000,name='loss')#declare it as a variable so that tensorboard can look at it
    cross_entropy = 0       #this will be modified through training
    regularizer = 0         #this will be modified through training
    regularizer2 = 0
        #Tensorflow operations
        #These operations cannot be used until define_graph() has been called
    saver_op = []       #tf.train.Saver()
    optimizer_op = []   #tf.train.AdagradOptimizer(3e-3).minimize(tf_loss)
    prediction_op = []  #tf.nn.softmax([],name='train_prediction')
    summary_op = []     #tf.merge_all_summaries()

    tf_kernel = []         #tf.Variable(tf.truncated_normal([kernel_size,1,1,num_kernel],name='kernel'))
    tf_ph_kernels = []
    tf_pool1 = []
    tf_hid_weights = []
    tf_unshape_hid_weights = []
    tf_conv = []
    tf_conv1 = []
    tf_corrrect_pred = []
    tf_accuracy = []
    tf_learning_rage = []

    #Output Data
    conf_mat_bal = []           #Stores the balanced confusion matrix data
    conf_mat_unbal = []         #Stores the unbalanced confusion matrix data
    overall_accuracy_bal = 0    #Stores the overall accuracy with balanced data
    overall_accuracy_unbal = 0  #Stores the overall accuracy with unbalanced data

    def set_graph_def_params(self,baseFilePath):
        """Once the graph hyper parameters are set save those to a file
        there exists the problem when a model runs and the graph definition changes
        the saved model and parameters cannot be reloaded.
        Therefore the same graph definition must be used for most of the hyper parameters"""
        logging.debug('setting graph parameters')
        print ">>>>>>>>>set the graph hyper parameters<<<<<<<"

        filePath = baseFilePath + "GRAPH_PARAMS_" + self.get_run_desc() + ".PKL"
        pkl_lst =  {'num_kernel': self.num_kernel,\
                    'kernel_size': self.kernel_size, \
                    'num_hidden':self.num_hidden, \
                    'num_steps':self.num_steps,\
                    'reg_level_hidden':self.reg_level_hidden,\
                    'reg_level_kernel':self.reg_level_kernel,\
                    'dropout_level':self.dropout_level,\
                    'learning_rate':self.learning_rate,\
                    'init_lrn_rate':self.init_lrn_rate,\
                    'pooling':self.pooling,\
                    'threeseason':self.threeseason,\
                    'standardized':self.standardized,\
                    'NOTE':self.HyperParamNote}

        pickle.dump(pkl_lst,open(filePath,"wb"))
        print "SAVED Run File Parameters to:", filePath
        return filePath

    def get_graph_def_params(self,filePath):
        """Load graph parameters from filePath"""
        logging.debug('getting graph parameters')
        print ">>>>>>>>get the graph parameters from .pkl<<<<<<<<"
        pkl_file = open(filePath,'rb')
        self.imported_params = pickle.load(pkl_file)

        self.num_hidden = self.imported_params['num_hidden']
        self.dropout_level = self.imported_params['dropout_level']
        self.reg_level_hidden = self.imported_params['reg_level_hidden']
        self.num_kernel = self.imported_params['num_kernel']
        self.kernel_size = self.imported_params['kernel_size']
        self.standardized = self.imported_params['standardized']

        #next time training a model should include the following parameters
        #self.data_length = self.imported_params['data_lengt']

    def define_graph(self):#data_length,num_labels,batch_size):
        """ This function defines the tensorflow graph for classification
        NOTE: Hyper parameters of the graph need to be defined inadvanced

        The graph consists of:
        Convolution Layer
            - Size of kernel (hyper param)
            - Number of kernels (hyper param)
            - Weight decay (hyper param)
        Pooling Layer (optional but included by default) - reduce total train param by 2 for hid layer
        Hidden Layer
            - with dropout (hyper param)
            - with weight decay (hyper param)
        Softmax Layer (output)

        Regulariazation = aggregate weight decay params, weights and cross ent

        Definition of loss function
        Definition of optimization op
        Definition of tensorboard variable summaries (throughout network)

        returns:
            Basically what this graph creates is operations that can be called
            when running a tensorflow session. Each operation is ran and only
            variables are trained and or computed as needed by that operation
            ANY tensorflow operation is defined within this function.
        """
        logging.debug('defining graph')
        print ">>>>>>>>>>DEFINE THE TENSORFLOW GRAPH - CONV->POOL->Logistic Classifier <<<<<<<<<<"

        inputSize = self.data_length
        data_length = self.data_length
        kernel_size = self.kernel_size
        num_kernels = self.num_kernel
        numHidden1 = self.num_hidden
        numOutput = self.num_labels
        batch_size = tf.shape(self.tf_ph_dataset)[0]
        batch_size = self.batch_size
        num_labels = self.num_labels

        self.graph = tf.Graph()   #create a new graph at every define_graph

        with self.graph.as_default():
            self.tf_ph_dataset = tf.placeholder(tf.float32,shape=(None, data_length,1,1)) #None = used for variable batch size
            self.tf_ph_labels = tf.placeholder(tf.float32, shape=(None, num_labels))#None = used for variable batch size

            self.tf_loss = tf.Variable(1000,name='loss')#declare it as a variable so that tensorboard can look at it

            # Convolutional Layer #1
            with tf.variable_scope('conv1') as scope:
                self.tf_kernel = tf.Variable(tf.truncated_normal([kernel_size,1,1,num_kernels],name='kernel'))

                #tf_train_dataset needs to have shape (batch_size, 188,1,1)
                self.tf_conv = tf.nn.conv2d(self.tf_ph_dataset, self.tf_kernel, strides=[1, 1, 1, 1],use_cudnn_on_gpu = True , padding='SAME')
                biases = tf.Variable(tf.truncated_normal([num_kernels],name = 'biases'))
                biased = tf.nn.bias_add(self.tf_conv, biases)
                self.tf_conv1 = tf.nn.tanh(biased, name=scope.name)
                #Add .scalar_summary for activations of "conv1"  _activation_summary(conv1)
                self.tf_pool1 = tf.nn.max_pool(self.tf_conv1, ksize=[1, 2, 1, 1], strides=[1, 2, 1, 1],
                                     padding='SAME', name='pool1')
                                  #need to look at the middle values for ksize and strides
                                  #(1,2) pooling size, stride is
                                  #1 down (doesn't really matter) as its only 1d vert
                                  #2 across so that it doens't overlap.
            # Hidden#1 Layer
            with tf.name_scope('hidden1'):
                #Reshape the 4-d Tensor to be 1d for the fully connected layer
                #   Pool1 has the shape [batch_size, training_data_array[0].shape[0] = 188/2 , 1 , # of kernels)]
                #   So reshaping should consist of [batch_size, inputSize(188/2*20)] # if data length = 188, pooling = 2 and # of kernels = 20
                #reshape = tf.reshape(self.tf_conv1,[-1, inputSize*num_kernels]) #the -1 allows for dynamic resizing ie variable batch_size
                reshape = tf.reshape(self.tf_pool1,[-1,int(round(inputSize/float(2)))*num_kernels])# self.tf_pool1.get_shape()[1]*self.tf_pool1.get_shape()[3]])
                                #inputSize*num_kernels/2]) #the -1 allows for dynamic resizing ie variable batch_size
                #self.tf_hid_weights = tf.Variable(tf.truncated_normal([inputSize*num_kernels, numHidden1]),name='weights')
                self.tf_hid_weights = tf.Variable(tf.truncated_normal([int(round(inputSize/float(2))*num_kernels), numHidden1]),name='weights')
                tf.histogram_summary('hidden1/weights', self.tf_hid_weights)
                biases = tf.Variable(tf.zeros([numHidden1]),name='biases') # 1024 is number of hidden1 units
                tf.histogram_summary('hidden1/biases', biases)
                hidden1 = tf.nn.tanh(tf.matmul(reshape,self.tf_hid_weights) + biases)
                #Dropout
                hidden1 = tf.nn.dropout(hidden1, 0.5)

            # Logistic Output Layer
            with tf.name_scope('logisticOutput'):
                tf_log_weights = tf.Variable(tf.truncated_normal([numHidden1, numOutput]),name='weights')
                tf.histogram_summary('logisticOutput/weights', tf_log_weights)
                biases = tf.Variable(tf.zeros([numOutput]),'name=biases')
                tf.histogram_summary('logisticOutput/name=biases', biases)
                logits = tf.matmul(hidden1, tf_log_weights) + biases
            # Loss Computation -- Need to be one-hot
            self.cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits, self.tf_ph_labels)
            #self.tf_loss = tf.reduce_mean(self.cross_entropy)
            self.regularizer = (tf.nn.l2_loss(self.tf_hid_weights) + tf.nn.l2_loss(tf_log_weights))#+ tf.nn.l2_loss(biased)
            #self.regularizer2 = tf.nn.l2_loss(biased)
            self.tf_loss = tf.reduce_mean(self.cross_entropy) + self.reg_level_hidden * self.regularizer# + self.reg_level_kernel * self.regularizer2
            tf.scalar_summary('regularizer',self.regularizer,name = 'regularizer')
            tf.scalar_summary('total_loss',self.tf_loss,name = 'total_loss')

            batch = tf.Variable(0)
            self.tf_learning_rate = tf.train.exponential_decay(
                                      self.init_lrn_rate,   # Base learning rate.
                                      batch * self.batch_size,  # Current index into the dataset.
                                      self.train_size,          # Decay step.
                                      0.95,                # Decay rate.
                                      staircase=True)
            #self.tf_learning_rate = batch
            self.optimizer_op = tf.train.AdagradOptimizer(self.learning_rate).minimize(self.tf_loss,global_step=batch)
            #self.optimizer_op = tf.train.MomentumOptimizer(self.learning_rate,.9).minimize(self.tf_loss,global_step=batch)
            # Predictions for the training, validation, and test data.
            self.prediction_op = tf.nn.softmax(logits,name='train_prediction')

            self.tf_correct_pred = tf.equal(tf.argmax(self.prediction_op,1),tf.argmax(self.tf_ph_labels,1),name = 'tf_correct_pred')
            self.tf_accuracy = tf.reduce_mean(tf.cast(self.tf_correct_pred,tf.float32), name = 'tf_accuracy')

            tf_accuracy_summary = tf.scalar_summary('Accuracy',self.tf_accuracy,name='overall_accuracy')

            tf.histogram_summary('prediction',self.prediction_op)

            self.summary_op = tf.merge_all_summaries()

            # Add ops to save and restore all the variables.
            self.saver_op = tf.train.Saver()

            return self.graph,self.tf_ph_dataset,self.tf_ph_labels,self.tf_loss,self.tf_kernel, self.prediction_op,self.optimizer_op,self.summary_op,self.saver_op
    def define_graph_set_kernel(self,kernels=0):#data_length,num_labels,batch_size):
        """ This function defines the tensorflow graph
        NOTE: Hyper parameters of the graph need to be defined inadvanced

        this define_graph sets the kernels within the graph

        The graph consists of:
        Convolution Layer
            - Size of kernel (hyper param)
            - Number of kernels (hyper param)
            - Weight decay (hyper param)
        Pooling Layer (optional) - reduce total train param by 2 for hid layer
        Hidden Layer
            - with dropout (hyper param)
            - with weight decay (hyper param)
        Softmax Layer (output)

        Regulariazation = aggregate weight decay params, weights and cross ent

        Definition of loss function
        Definition of optimization op
        Definition of tensorboard variable summaries (throughout network)

        returns:
            Basically what this graph creates is operations that can be called
            when running a tensorflow session. Each operation is ran and only
            variables are trained and or computed as needed by that operation
            ANY tensorflow operation is defined within this function.
        """
        print ">>>>>>>>>>DEFINE THE TENSORFLOW GRAPH SET KERNEL - CONV(modified)->POOL->MLP CLASSIFIER <<<<<<<<<<"

        inputSize = self.data_length
        data_length = self.data_length
        kernel_size = self.kernel_size
        num_kernels = self.num_kernel
        numHidden1 = self.num_hidden
        numOutput = self.num_labels
        batch_size = tf.shape(self.tf_ph_dataset)[0]
        batch_size = self.batch_size
        num_labels = self.num_labels

        tf_ph_kernels = self.tf_ph_kernels

        self.graph = tf.Graph()   #create a new graph at every define_graph

        #tf.reset_default_graph() #Clear the default graph **Just in case.
        with self.graph.as_default():
            self.tf_ph_dataset = tf.placeholder(tf.float32,shape=(None, data_length,1,1)) #None = used for variable batch size
            self.tf_ph_labels = tf.placeholder(tf.float32, shape=(None, num_labels))#None = used for variable batch size

            #THIS ENALBES THE SETTING OF THE KERNELS WITHIN THE NETWORK!!!
            self.tf_ph_kernels = tf.placeholder(tf.float32,shape=(kernel_size,1,1,num_kernels) )

            self.tf_loss = tf.Variable(1000,name='loss')#declare it as a variable so that tensorboard can look at it

            # Convolutional Layer #1
            with tf.variable_scope('conv1') as scope:
                self.tf_kernel = tf.Variable(tf.truncated_normal([kernel_size,1,1,num_kernels],name='kernel'))
                #self.tf_kernel = tf.Variable(tf_ph_kernels)

                #tf_train_dataset needs to have shape (batch_size, 188,1,1)
                self.tf_conv = tf.nn.conv2d(self.tf_ph_dataset, self.tf_ph_kernels, strides=[1, 1, 1, 1],use_cudnn_on_gpu = True , padding='SAME')
                biases = tf.Variable(tf.truncated_normal([num_kernels],name = 'biases'))
                biased = tf.nn.bias_add(self.tf_conv, biases)
                self.tf_conv1 = tf.nn.tanh(biased, name=scope.name)
                #Add .scalar_summary for activations of "conv1"  _activation_summary(conv1)
                self.tf_pool1 = tf.nn.max_pool(self.tf_conv1, ksize=[1, 2, 1, 1], strides=[1, 2, 1, 1],
                                     padding='SAME', name='pool1')
                                  #need to look at the middle values for ksize and strides
                                  #(1,2) pooling size, stride is
                                  #1 down (doesn't really matter) as its only 1d vert
                                  #2 across so that it doens't overlap.
            # Hidden#1 Layer
            with tf.name_scope('hidden1'):
                #Reshape the 4-d Tensor to be 1d for the fully connected layer
                #   Pool1 has the shape [batch_size, training_data_array[0].shape[0] = 188/2 , 1 , # of kernels)]
                #   So reshaping should consist of [batch_size, inputSize(188/2*20)] # if data length = 188, pooling = 2 and # of kernels = 20
                #reshape = tf.reshape(self.tf_conv1,[-1, inputSize*num_kernels]) #the -1 allows for dynamic resizing ie variable batch_size
                reshape = tf.reshape(self.tf_pool1,[-1, inputSize*num_kernels/2]) #the -1 allows for dynamic resizing ie variable batch_size
                #self.tf_hid_weights = tf.Variable(tf.truncated_normal([inputSize*num_kernels, numHidden1]),name='weights')
                self.tf_hid_weights = tf.Variable(tf.truncated_normal([inputSize*num_kernels/2, numHidden1]),name='weights')

                tf.histogram_summary('hidden1/weights', self.tf_hid_weights)
                biases = tf.Variable(tf.zeros([numHidden1]),name='biases') # 1024 is number of hidden1 units
                tf.histogram_summary('hidden1/biases', biases)
                #weights = tf.Variable(tf.truncated_normal([IMAGE_PIXELS, hidden1_units],
                #                                          stddev=1.0 / math.sqrt(float(IMAGE_PIXELS))),
                #                      name='weights')
                #biases = tf.Variable(tf.zeros([hidden1_units]),name='biases')
                #hidden1 = tf.nn.relu(tf.matmul(tf_train_dataset,weights) + biases)
                hidden1 = tf.nn.tanh(tf.matmul(reshape,self.tf_hid_weights) + biases)
                #Dropout
                hidden1 = tf.nn.dropout(hidden1, 0.5)

            # Logistic Output Layer
            with tf.name_scope('logisticOutput'):
                tf_log_weights = tf.Variable(tf.truncated_normal([numHidden1, numOutput]),name='weights')
                tf.histogram_summary('logisticOutput/weights', tf_log_weights)
                biases = tf.Variable(tf.zeros([numOutput]),'name=biases')
                tf.histogram_summary('logisticOutput/name=biases', biases)
                logits = tf.matmul(hidden1, tf_log_weights) + biases
            # Loss Computation  -- Need to be one-hot
            self.cross_entropy = tf.nn.softmax_cross_entropy_with_logits(logits, self.tf_ph_labels)
            self.tf_loss = tf.reduce_mean(self.cross_entropy)
            self.regularizer = (tf.nn.l2_loss(self.tf_hid_weights) + tf.nn.l2_loss(tf_log_weights)) + tf.nn.l2_loss(biased)
            self.regularizer2 = tf.nn.l2_loss(biased)
            self.tf_loss += self.reg_level_hidden * self.regularizer #+ self.reg_level_kernel * self.regularizer2
            tf.scalar_summary('regularizer',self.regularizer,name = 'regularizer')
            tf.scalar_summary('total_loss',self.tf_loss,name = 'total_loss')

            batch = tf.Variable(0)
            self.tf_learning_rate = tf.train.exponential_decay(
                                      self.init_lrn_rate,   # Base learning rate.
                                      batch * self.batch_size,  # Current index into the dataset.
                                      self.train_size,          # Decay step.
                                      0.95,                # Decay rate.
                                      staircase=True)

            self.optimizer_op = tf.train.AdagradOptimizer(self.learning_rate).minimize(self.tf_loss,global_step=batch)
            #self.optimizer_op = tf.train.MomentumOptimizer(self.learning_rate,.9).minimize(self.tf_loss,global_step=batch)
            # Predictions for the training, validation, and test data.
            self.prediction_op = tf.nn.softmax(logits,name='train_prediction')

            self.tf_correct_pred = tf.equal(tf.argmax(self.prediction_op,1),tf.argmax(self.tf_ph_labels,1),name = 'tf_correct_pred')
            self.tf_accuracy = tf.reduce_mean(tf.cast(self.tf_correct_pred,tf.float32), name = 'tf_accuracy')

            tf_accuracy_summary = tf.scalar_summary('Accuracy',self.tf_accuracy,name='overall_accuracy')

            tf.histogram_summary('prediction',self.prediction_op)

            self.summary_op = tf.merge_all_summaries()

            # Add ops to save and restore all the variables.
            self.saver_op = tf.train.Saver()

            return self.graph,self.tf_ph_dataset,self.tf_ph_labels,self.tf_loss,self.tf_kernel, self.prediction_op,self.optimizer_op,self.summary_op,self.saver_op
    def train_graph(self, data = 0, v_data = 0 ):
        """ Outlines the standard training of a tensorflow graph
        NOTE: Hyper the graph needs to be defined before this function
        is called unless self.restore_graph  == True, if, then restore graph
        located at self.current_model_path

        1. creates session for self.graph
        2. restores graph is applicable
        3. prepare data - feed dict
        4. enter while loop to continue self.num_steps and if stopping criteria
            has been met
        5. perform a train step
        6. continue and every 500 epochs write to terminal, some accuracies
            run the summary operation for tensorboard
            save model as well.
        7. once have reached the stopping criteria exit while loop
        8. save key characteristics from the model for additional analysis in
            non-tensorflow data space (these should already be captured within
            the tensorboard):
            -kernels - these are useful for additional post analysis
            -predictions - captures same information presented to tensorboard
            -loss - captures same information presented to tensorboard
        """
        if data == 0:
            print "ERROR Enter In appropriate data"

        if v_data == 0:
            print "ERROR Enter in appropriate validation data"

        graph = self.graph
        tf_ph_dataset = self.tf_ph_dataset
        tf_ph_labels = self.tf_ph_labels
        tf_loss = self.tf_loss
        prediction_op = self.prediction_op
        optimizer_op = self.optimizer_op
        summary_op = self.summary_op
        saver_op = self.saver_op
        batch_size = self.batch_size
        num_steps = self.num_steps
        tf_kernel = self.tf_kernel
        tf_hid_weights = self.tf_hid_weights
        tf_conv1 = self.tf_conv1
        tf_unshape_hid_weights = self.tf_unshape_hid_weights
        data_arry = data.data
        labels_1_hot = data.labels_one_hot

        valid_data_arry = v_data.data
        valid_labels_1_hot = v_data.labels_one_hot

        step = 0
        with tf.Session(graph=graph) as session:#graph=graph
        # or
        #session = tf.Session(graph=graph)
        #with session:
        # or
        #session = tf.InteractiveSession(graph=graph)

            if self.restore_graph == True:
                #TO RESTORE SAVED MODEL
                tf.initialize_all_variables().run(session=session) #included to initialize new vars
                saver = tf.train.Saver()
                saver.restore(session, self.current_model_path)
                print 'Model restored.'
            else:
                tf.initialize_all_variables().run(session=session)
                print("Initialized")

            summary_writer = tf.train.SummaryWriter(self.data_path, session.graph.as_graph_def()) #graph_def = session.graph_def)

            #for step in range(int(self.num_steps))
            while step < self.num_steps:
                if step > self.num_steps-self.num_steps*.1:    #if within 10% of stopping
                    pass
                #    if self.ave_cur_std_dev > self.ave_learn_rate_stop:          #and learning rate is greater than plateau
                #        self.num_steps += self.batch_size*2    #continue training
                #        print "adding :", str(self.batch_size*2) ,"steps"
                #print "step: ", step
                self.var1 = step

                # Pick an offset within the training data, which has been randomized.
                offset = (step * batch_size) % (labels_1_hot.shape[0] - batch_size)
                # Generate a minibatch. Data should be randomized
                batch_data = np.copy(data_arry[offset:(offset + batch_size), :])
                batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                #batch_data = train_data_arry[offset:(offset + batch_size), :]
                batch_labels = labels_1_hot[offset:(offset + batch_size), :]
                # Prepare a dictionary telling the session where to feed the minibatch.
                # The key of the dictionary is the placeholder node of the graph to be fed,
                # and the value is the numpy array to feed to it.
                feed_dict = {self.tf_ph_dataset : batch_data, self.tf_ph_labels : batch_labels}
                self.var2 = feed_dict
                self.var3 = batch_data
                self.var4 = labels_1_hot.shape[0]

                startTime = time.time()
                _, l, predictions = session.run([optimizer_op, tf_loss, prediction_op], feed_dict=feed_dict)
                stopTime = time.time()
                #print step

                if (step % 500 == 0):
                    epochTime = stopTime-startTime
                    print("Minibatch loss at step %d took %s (s) and is: %f time remaining is %s (min)" % (step,str(round(epochTime,3)), l, str(round(epochTime*(self.num_steps-step)/60,1)) ) )
                    self.pred_lst.append(accuracy(predictions, batch_labels))
                    self.loss_lst.append(l)

                    print("Minibatch accuracy: %.1f%%" % accuracy(predictions, batch_labels))

                    v_data = valid_data_arry#[:batch_size,:]
                    v_data = valid_data_arry[:batch_size*10,:]
                    v_data = np.reshape(v_data,[v_data.shape[0],v_data.shape[1],1,1])
                    v_labels = valid_labels_1_hot#[:batch_size,:]
                    v_labels = valid_labels_1_hot[:batch_size*10,:]

                    feed_dict = {tf_ph_dataset : v_data, tf_ph_labels : v_labels}
                    Vpredictions, self.kernels, self.hid_weights, self.conv1, self.learning_rate = session.run([prediction_op,tf_kernel,tf_hid_weights,tf_conv1,self.tf_learning_rate],feed_dict=feed_dict)

                    #self.var1 = batch_data
                    #self.var2 = v_labels
                    #self.var3 = v_data
                    #self.var4 = Vpredictions

                    #calculate the summary op and then write to the file to be used in the
                    # Tensorboard.


                    self.vpred_lst.append(accuracy(Vpredictions, v_labels))
                    print "learning rate: ", self.learning_rate
                    print("Validation accuracy: %.1f%%" % accuracy(Vpredictions, v_labels))

                    [summary_str] = session.run([summary_op],feed_dict=feed_dict)

                    summary_writer.add_summary(summary_str,step)

                    # Save the variables to disk.
                    full_model_path = self.model_path + "MODEL_" + self.get_run_desc()
                    save_path = saver_op.save(session, full_model_path,global_step=step)
                    print("Model saved in file: %s" % save_path)
                    self.current_model_path = save_path
                    if (len(self.vpred_lst) > 6):   #average the last 5 learning 500 epochs
                        vpred_arry = np.asarray(self.vpred_lst)
                        average_num = 5
                        #a = vpred_arry[-1:-1-average_num:-1] # happened soonest
                        #b = vpred_arry[-2:-2-average_num:-1] # happened before a
                        self.ave_cur_std_dev = np.std(vpred_arry[-1:-1-average_num:-1]) # change from b going to a
                        #self.ave_cur_learn_rate = (np.average(self.vpred_lst[-1:-5:-1])-np.average(self.vpred_lst[-5:-9:-1]))/5
                        print "The current learning std_dev over 4 values is: ", self.ave_cur_std_dev, "% points per 500 epochs"

                step += 1 #increment the number of steps

            filePath = self.kernel_path + "KERNELS_" + self.get_run_desc() + ".PKL"
            pickle.dump(self.kernels,open(filePath,"wb"))
            filePath = self.kernel_path + "HID_WEIGHTS_" + self.get_run_desc() + ".PKL"
            pickle.dump(self.hid_weights,open(filePath,"wb"))
            #Need to save a training "record" show the loss and learning curves
            filePath = self.kernel_path + "VPRED_" + self.get_run_desc() + ".PKL"
            pickle.dump(self.vpred_lst,open(filePath,"wb"))

            filePath = self.kernel_path + "LOSS_" + self.get_run_desc() + ".PKL"
            pickle.dump(self.loss_lst,open(filePath,"wb"))

            filePath = self.kernel_path + "PRED_" + self.get_run_desc() + ".PKL"
            pickle.dump(self.pred_lst,open(filePath,"wb"))

            last_saved_model = save_path
            last_saved_kernels = filePath

            return last_saved_model,last_saved_kernels,self.loss_lst,self.pred_lst,self.vpred_lst,self.hid_weights,self.conv1
    #%%
    def restore_session(self):
        """ This function opens up a tensorflow session
        this will open a session network needs to be defined current_model_path
        and graph params set correctly.
        """
        logging.debug('restore_session')
        print ">>>>>>>>restore a session<<<<<<<"

        self.session =  tf.InteractiveSession(graph=self.graph)
        tf.initialize_all_variables().run(session=self.session)
        print("Initialized")
        #tf.train.write_graph(sess.graph_def, '/home/daniel/Documents/Projetos/Prorum/ProgramasEmPython/TestingTensorFlow/fileGraph', 'graph.pbtxt')

        #TO RESTORE SAVED MODEL
        saver = tf.train.Saver()
        saver.restore(self.session, self.current_model_path)
        print("Model restored.")
        # Do some work with the model

    def close_session(self):
        """ This function will close an existing session"""
        logging.debug('close_session')
        print ">>>>>>>close session<<<<<<<"
        self.session.close()

    def run_graph(self,data_arry,labels_1_hot):
        """ This function runs the graph for the classification of a dataset
        This function:
        1. restores the graph saved at self.current_model_path (this should be
            current for a recently trained graph, so no need to update
            manually)
        2. prepares the feed_dictionary
        3. creates a session based on self.graph
        3. calls the prediction_operation
        4. returns the prediction class
        """
        logging.debug('run_graph')
        print ">>>>>>>>>>CREATE THE SESSION AND RUN INSTANCE<<<<<<<<<<"
        #

        graph = self.graph
        tf_ph_dataset = self.tf_ph_dataset
        tf_ph_labels = self.tf_ph_labels
        prediction_op = self.prediction_op
        batch_size = self.batch_size

        tf_kernel = self.tf_kernel

        #graph = define_graph()
        with tf.Session(graph=graph) as session:#graph=graph
        #session = tf.Session(graph=graph)
        #with session:
        #session = tf.InteractiveSession(graph=graph)
            tf.initialize_all_variables().run(session=session)
            #TO RESTORE SAVED MODEL
            saver = tf.train.Saver()
            saver.restore(session, self.current_model_path)

            # Do some work with the model

            step = 1
            batch_data = data_arry
            if data_arry.shape[0] == batch_size:
                offset = 0
            else:
                offset = (step * batch_size) % (data_arry.shape[0] - batch_size)
            batch_data = np.copy(data_arry[offset:(offset + batch_size), :])
            batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
            batch_labels = labels_1_hot[offset:(offset + batch_size), :]

            feed_dict = {tf_ph_dataset : batch_data, tf_ph_labels : batch_labels}

            startTime = time.time()
            predictions,kernels = session.run([prediction_op,tf_kernel], feed_dict=feed_dict)
            stopTime = time.time()

            return [predictions],(stopTime-startTime),kernels
#%% SESSION CREATION AND INSTANCE CREATED
    def run_graph_set_kernels(self,data_arry,labels_1_hot,kernels):
        """ This function runs the graph for the classification of a dataset
        This function:
        1. restores the graph saved at self.current_model_path (this should be
            current for a recently trained graph, so no need to update
            manually)
        2. prepares the feed_dictionary
        3. creates a session based on self.graph
        3. calls the prediction_operation
        4. returns the prediction class
        """
        print ">>>>>>>>>>run the graph with set kernels<<<<<<<<<<"

        graph = self.graph
        tf_ph_dataset = self.tf_ph_dataset
        tf_ph_labels = self.tf_ph_labels
        prediction_op = self.prediction_op
        batch_size = self.batch_size
        tf_kernel = self.tf_kernel
        tf_ph_kernels = self.tf_ph_kernels

        #graph = define_graph()
        with tf.Session(graph=graph) as session:#graph=graph
        #session = tf.Session(graph=graph)
        #with session:
        #session = tf.InteractiveSession(graph=graph)
            tf.initialize_all_variables().run(session=session)

            #TO RESTORE SAVED MODEL
            saver = tf.train.Saver()
            saver.restore(session, self.current_model_path)
            # Do some work with the model

            step = 1
            offset = (step * batch_size) % (labels_1_hot.shape[0] - batch_size)
            batch_data = np.copy(data_arry[offset:(offset + batch_size), :])
            batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
            batch_labels = labels_1_hot[offset:(offset + batch_size), :]

            feed_dict = {tf_ph_dataset : batch_data, tf_ph_labels : batch_labels, tf_ph_kernels : kernels}

            startTime = time.time()
            predictions = session.run([prediction_op], feed_dict=feed_dict)
            stopTime = time.time()

        return predictions,(stopTime-startTime),kernels

    def run_graph_open_session(self,data_arry,labels_1_hot):
        """ This function runs the graph for the classification of a dataset
        This function:
        1. restores the graph saved at self.current_model_path (this should be
            current for a recently trained graph, so no need to update
            manually)
        2. prepares the feed_dictionary
        3. creates a session based on self.graph
        3. calls the prediction_operation
        4. returns the prediction class
        """
        logging.debug('run_open_session_graph')
        print ">>>>>>>>>>run_graph_open_session<<<<<<<<<<"
        #

        graph = self.graph
        tf_ph_dataset = self.tf_ph_dataset
        tf_ph_labels = self.tf_ph_labels
        prediction_op = self.prediction_op
        batch_size = self.batch_size

        tf_kernel = self.tf_kernel

        if self.session == 0: #there isn't a session and one needs to be created
        #with tf.Session(graph=graph) as session:#graph=graph
        #session = tf.Session(graph=graph)
        #with session:
            self.session = tf.InteractiveSession(graph=graph)
            tf.initialize_all_variables().run(session=self.session)

            #TO RESTORE SAVED MODEL
            saver = tf.train.Saver()
            saver.restore(self.session, self.current_model_path)
            # Do some work with the model
        else:               #a session has been created and we don't need to create one
            step = 1
            batch_data = data_arry
            if data_arry.shape[0] == batch_size:
                offset = 0
            else:
                offset = (step * batch_size) % (data_arry.shape[0] - batch_size)
            batch_data = np.copy(data_arry[offset:(offset + batch_size), :])
            batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
            batch_labels = labels_1_hot[offset:(offset + batch_size), :]
            feed_dict = {tf_ph_dataset : batch_data, tf_ph_labels : batch_labels}

            startTime = time.time()
            predictions,kernels = self.session.run([prediction_op,tf_kernel], feed_dict=feed_dict)
            stopTime = time.time()

        return [predictions],(stopTime-startTime),kernels
    def confusion_matrix_unbalanced(self,data):
        """Calculate the Unbalanced Confusion Matrix for the HSI-Classifier

        1. format data
            data should be a HSI_data_io object
                data.data <- contains datapoints
                data.labels <- contains labels
                data.num_labels <- ""
                data.num_data <- ""
                See hsi_data_io for more memeber and support functions
        2. open session
        3. restore model stored at self.current_model_path
        4. classify all data within valid dataset
        5. go through each piece of data and add that prediction to the
            confusion matrix - basically
            conf_mat[row][col]
            conf_mat[predicted_class][actual class]
            conf_mat[pred_nums[x]][valid_labels_num[x]] += 1

        Args:
            self.graph
            self.valid_data
            self.valid_labels

        Return:
            confusion_data      = the data consists of
                      <-------------+actual class+-------------------->
                ^    +--------------------------------------------------+
                |    |--------------------------------------------------|
                +    |--------------------------------------------------|
     predicted class |------data----------------------------------------|
                +    |--------------------------------------------------|
                |    |--------------------------------------------------|
                v    |--------------------------------------------------|
                     +--------------------------------------------------+

            NOTE: for unbalanced confusion matrcies the each number of
            actual classes does not need to be equal.
        """
        logging.debug('calculate unbalanced confusion matrix')
        print ">>>>>>>>>calculate unbalanced confusion matrix<<<<<<<<"
        batch_size = self.batch_size
        num_data = data.num_data
        num_steps = num_data/batch_size
        graph = self.graph

        valid_labels_num = labels_to_num(data.labels, data.set_labels)
        valid_labels_1_hot = reformatOneHot(data.labels_num,data.num_labels)
        pred_lst = []

        with tf.Session(graph=graph) as session:#graph=graph
        #session = tf.Session(graph=graph)
        #with session:
        #session = tf.InteractiveSession(graph=graph)
            tf.initialize_all_variables().run(session=session)
            print("Initialized")
            #tf.train.write_graph(sess.graph_def, '/home/daniel/Documents/Projetos/Prorum/ProgramasEmPython/TestingTensorFlow/fileGraph', 'graph.pbtxt')

            #TO RESTORE SAVED MODEL
            saver = tf.train.Saver()

            saver.restore(session, self.current_model_path)
            print("Model restored.")
            for step in range(num_steps):

                offset = (step * batch_size) % (valid_labels_1_hot.shape[0] - batch_size)
                batch_data = np.copy(data.data[offset:(offset + batch_size), :])
                batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                batch_labels = valid_labels_1_hot[offset:(offset + batch_size), :]
                feed_dict = {self.tf_ph_dataset : batch_data, self.tf_ph_labels : batch_labels}
                prediction = session.run([self.prediction_op], feed_dict=feed_dict)
                pred_lst.extend(prediction[0])
                accuracy(np.asarray(prediction[0]), batch_labels)

                #print("Validation accuracy: %.1f%%" % accuracy(np.asarray(prediction[0]), batch_labels))#,dict_idx2cls,step))

            batch_size = num_data%num_steps #capture the last bit of the data
            offset = (step * batch_size) % (valid_labels_1_hot.shape[0] - batch_size)
            batch_data = np.copy(data.data[offset:(offset + batch_size), :])
            batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
            batch_labels = valid_labels_1_hot[offset:(offset + batch_size), :]
            feed_dict = {self.tf_ph_dataset : batch_data, self.tf_ph_labels : batch_labels}
            prediction = session.run([self.prediction_op], feed_dict=feed_dict)
            pred_lst.extend(prediction[0])
            accuracy(np.asarray(prediction[0]), batch_labels)

            #print("Validation accuracy: %.1f%%" % accuracy(np.asarray(prediction[0]), batch_labels))#,dict_idx2cls,step))

        pred_arry = np.asarray(pred_lst) # (batch_size by 20) use np.argmax to get back to number
        pred_nums = np.argmax(pred_arry,axis=1)
        valid_labels_num.shape
        conf_mat = np.zeros((self.num_labels,self.num_labels))
        for x in range(len(pred_arry)):
            conf_mat[valid_labels_num[x]][pred_nums[x]] += 1
        self.conf_mat_unbal = np.asarray(conf_mat,dtype=int)

        return self.conf_mat_unbal

    def confusion_matrix_balanced(self,data):
        """Calculate the balanced Confusion Matrix for the HSI-Classifier

        1. format data - sort
        2. determine the max number of data points that all the data contains
            min(bincount(dataset))
        3. open session
        4. restore model stored at self.current_model_path
        5. classify all data within the sliced dataset so that its a balanced
            dataset
        6. go through each piece of data and add that prediction to the
            confusion matrix - basically
            conf_mat[row][col]
            conf_mat[predicted_class][actual class]
            conf_mat[pred_nums[x]][valid_labels_num[x]] += 1

        Args:
            self.graph
            self.valid_data
            self.valid_labels
                Note:subsets of the valid_data and valid_labels will be created

        Return:
            confusion_data      = the data consists of
                      <-------------+actual class+-------------------->
                ^    +--------------------------------------------------+
                |    |--------------------------------------------------|
                +    |--------------------------------------------------|
     predicted class |------data----------------------------------------|
                +    |--------------------------------------------------|
                |    |--------------------------------------------------|
                v    |--------------------------------------------------|
                     +--------------------------------------------------+

            NOTE: for unbalanced confusion matrcies the each number of
            actual classes does not need to be equal.
        """
        logging.debug('calculate balanced confusion matrix')
        print ">>>>>>>>>calculate balanced confusion matrix<<<<<<<<"

        org_batch_size = self.batch_size

        zipped_label_data = zip(data.labels_num,data.data)
        zipped_label_data = sorted(zipped_label_data,key=lambda class_data:class_data[0])

        sorted_valid_labels_num = [zipped_label_data[x][0] for x in range(len(zipped_label_data))]
        sorted_valid_data = [zipped_label_data[x][1] for x in range (len(zipped_label_data))]
        sorted_valid_one_hot_labels = reformatOneHot(np.asarray(sorted_valid_labels_num),self.num_labels)

        strt_idx = 0
        stop_idx = 0
        confus_data = []
        bincounts = np.bincount(sorted_valid_labels_num)
        per_class_accuracy = []
        print ">>>>>>>>>for each class capture the predictions<<<<<<<<"
        self.batch_size = np.min(bincounts)
        num_bal = np.min(bincounts)
        for i in range(self.num_labels):
            strt_idx = np.sum(bincounts[:i])
            stop_idx = np.sum(bincounts[:i+1])# + np.min(bincounts)#np.sum(bincounts[:i+1])
            data_range = np.copy(sorted_valid_data[strt_idx:strt_idx + num_bal])
            label_range = np.copy(sorted_valid_one_hot_labels[strt_idx:strt_idx + num_bal])

            prediction,run_duration,_ = self.run_graph(data_range,label_range)

            prediction_arry = np.asarray(prediction[0])
            per_class_accuracy.append(accuracy(prediction_arry, label_range[:self.batch_size]))
            print 'the accuracy for class : ', i, ' is ',  per_class_accuracy[i]
            confus_data.append(prediction[0])

        confus_data_arry = np.asarray(confus_data)

        confus_data_accuracy = []
        confus_data_count = []
        for j in range(self.num_labels):#for each class calclate the accuracy for the other classes
            confus_data_accuracy.append([])#create a new entry for this classes confusion accuracies
            confus_data_count.append([])#create a new entry for this classes confusion accuracies
            for i in range(self.num_labels):
                confus_data_accuracy[j].append(np.sum(np.argmax(confus_data_arry[j],axis=1)==i)/float(confus_data_arry[i].shape[0]))#append each accuracy
                confus_data_count[j].append(np.sum(np.argmax(confus_data_arry[j],axis=1)==i))#/float(confus_data_arry[i].shape[0]))#append each accuracy
        confus_data_accuracy_arry = np.asarray(confus_data_accuracy)
        confus_data_count_arry = np.asarray(confus_data_count)
        self.batch_size = org_batch_size

        self.conf_mat_bal = confus_data_count
        return confus_data_count_arry,per_class_accuracy #,confus_data_arry
    def confusion_matrix_balanced_zeroed_kernel(self,kernels,data):
        """Calculate the balanced Confusion Matrix for the HSI-Classifier

        1. format data - sort
        2. determine the max number of data points that all the data contains
            min(bincount(dataset))
        3. open session
        4. restore model stored at self.current_model_path
        5. classify all data within the sliced dataset so that its a balanced
            dataset
        6. go through each piece of data and add that prediction to the
            confusion matrix - basically
            conf_mat[row][col]
            conf_mat[predicted_class][actual class]
            conf_mat[pred_nums[x]][valid_labels_num[x]] += 1

        Args:
            self.graph
            self.valid_data
            self.valid_labels
                Note:subsets of the valid_data and valid_labels will be created

        Return:
            confusion_data      = the data consists of
                      <-------------+actual class+-------------------->
                ^    +--------------------------------------------------+
                |    |--------------------------------------------------|
                +    |--------------------------------------------------|
     predicted class |------data----------------------------------------|
                +    |--------------------------------------------------|
                |    |--------------------------------------------------|
                v    |--------------------------------------------------|
                     +--------------------------------------------------+

            NOTE: for unbalanced confusion matrcies the each number of
            actual classes does not need to be equal.
        """
        logging.debug('calculate balanced confusion matrix zeroed kernels')
        print ">>>>>>>>>calculate balanced confusion matrix zeroed kernels<<<<<<<<"

        org_batch_size = self.batch_size

        zipped_label_data = zip(data.labels_num,data.data)
        zipped_label_data = sorted(zipped_label_data,key=lambda class_data:class_data[0])

        sorted_valid_labels_num = [zipped_label_data[x][0] for x in range(len(zipped_label_data))]
        sorted_valid_data = [zipped_label_data[x][1] for x in range (len(zipped_label_data))]
        sorted_valid_one_hot_labels = reformatOneHot(np.asarray(sorted_valid_labels_num),self.num_labels)

        strt_idx = 0
        stop_idx = 0
        confus_data = []
        bincounts = np.bincount(sorted_valid_labels_num)
        per_class_accuracy = []
        #print ">>>>>>>>>for each class capture the predictions<<<<<<<<"
        self.batch_size = np.min(bincounts)
        #print "batch_size for zeroed_kernel: ",self.batch_size
        num_bal = np.min(bincounts)
        for i in range(self.num_labels):
            strt_idx = np.sum(bincounts[:i])
            stop_idx = np.sum(bincounts[:i]) + num_bal #np.sum(bincounts[:i+1])
            data_range = np.copy(sorted_valid_data[strt_idx:stop_idx])
            label_range = np.copy(sorted_valid_one_hot_labels[strt_idx:stop_idx])

            prediction,run_duration,kernels = self.run_graph_set_kernels(data_range,label_range,kernels)

            prediction_arry = np.asarray(prediction[0])
            per_class_accuracy.append(accuracy(prediction_arry, label_range[:self.batch_size]))
            print 'the accuracy for class : ', i, ' is ',  per_class_accuracy[i]
            confus_data.append(prediction[0])

        confus_data_arry = np.asarray(confus_data)

        confus_data_accuracy = []
        confus_data_count = []
        for j in range(self.num_labels):#for each class calclate the accuracy for the other classes
            confus_data_accuracy.append([])#create a new entry for this classes confusion accuracies
            confus_data_count.append([])#create a new entry for this classes confusion accuracies
            for i in range(self.num_labels):
                confus_data_accuracy[j].append(np.sum(np.argmax(confus_data_arry[j],axis=1)==i)/float(confus_data_arry[i].shape[0]))#append each accuracy
                confus_data_count[j].append(np.sum(np.argmax(confus_data_arry[j],axis=1)==i))#/float(confus_data_arry[i].shape[0]))#append each accuracy
        confus_data_accuracy_arry = np.asarray(confus_data_accuracy)
        confus_data_count_arry = np.asarray(confus_data_count)
        self.batch_size = org_batch_size
        return confus_data_count_arry,per_class_accuracy



    def plot_confusion_matrix(self,confus_data_count_arry,num_classes,plot_note):
        """Plot the Confusion Matrix in a Repeatable Format

        Args:
            confusion_data      = the data consists of
                      <-------------+input class+--------------------->
                ^    +--------------------------------------------------+
                |    |--------------------------------------------------|
                +    |--------------------------------------------------|
     predicted class |------data----------------------------------------|
                +    |--------------------------------------------------|
                |    |--------------------------------------------------|
                v    |--------------------------------------------------|
                     +--------------------------------------------------+
            number of classes - this dictates the size of the confusion matrix
            plot_note - this is an additional description to be added to the
                        title of the graph, this is useful to uniquely
                        identify the graph - which run - which classifier etc
        Return: Figure with confusion matrix plotted
        """
        logging.debug('plot_confusion_matrix')
        print ">>>>>>>>>plot_confusion_matrix<<<<<<<<"
        total_classified_items = np.sum(confus_data_count_arry,axis=1)
        total_classified_items2 = np.sum(confus_data_count_arry,axis=0)
        for i in range(len(total_classified_items)):
            if total_classified_items[i] == 0:
                total_classified_items[i] = 1
            else:
                pass
            if total_classified_items2[i] == 0:
                total_classified_items2[i] = 1
            else:
                pass

        A = np.copy(confus_data_count_arry)#.reshape(num_classes,num_classes))
        # Confusion Matrix
        B = np.copy(A)
        row_sum = np.sum(B,axis=0,keepdims=True)
        col_sum = np.sum(B,axis=1,keepdims=True)

        producer_accuracy = np.reshape(np.asarray(np.round([confus_data_count_arry[i,i]/float(total_classified_items[i])*100 for i in range(num_classes)],2)),(12,1))
        user_accuracy = np.reshape(np.asarray(np.round([confus_data_count_arry[i,i]/float(total_classified_items2[i])*100 for i in range(num_classes)],2)),(1,12))

        #for user/producer accuracies
        B = np.hstack((B,producer_accuracy))
        B = np.vstack((B,np.hstack((user_accuracy,[[0]]))))

        #for counts use the code below
        #B = np.hstack((B,col_sum))
        #B = np.vstack((B,np.hstack((row_sum,[[np.sum(row_sum)]]))))

        fig = plt.figure(figsize=(11.0, 8.0))
        ax1 = fig.add_subplot(111)
        major_ticks = np.arange(0,num_classes,1)
        minor_ticks = np.arange(-.5,num_classes-.5,1)


        ax1.set_xticks(major_ticks)
        ax1.set_xticks(minor_ticks, minor=True)
        ax1.set_yticks(major_ticks)
        ax1.set_yticks(minor_ticks, minor=True)
        #ax1.grid(which='both')
        #ax1.xaxis.tick_top()
        ax1.xaxis.set_view_interval(0,22,ignore=True)
        ax1.set_xlim(-.5,num_classes+1.5)#21.5)
        ax1.set_ylim(-.5,num_classes+.5)#20.5)
        ax1.set_ylabel("Output Classified Class Index")
        ax1.set_xlabel("Input Data Class Index")
        accuracy = [ A[i][i] for i in range(len(total_classified_items))]
        accuracy = np.sum(accuracy)/float(np.sum(A))
        #ax1.set_title("Confusion Matrix " +plot_note+ "\n Kappa = " + str(np.round(kappa(A),2)) +
        ax1.set_title(plot_note+ "\n Kappa = " + str(np.round(kappa(A),2))
                                + "\n Accuracy = " + str(np.round(accuracy,3)*100)+'%')#np.sum(np.diag(A))/float(np.sum(np.sum(A,axis=1)))

        ax1.grid(b = True,which = 'minor')

        #Color bar settings
        cmap = cm.get_cmap('jet') # jet doesn't have white color, but dark when printed
        #cmap.set_bad('w') # default value is 'k'
        cax1 = ax1.imshow(np.transpose(A), interpolation="nearest",alpha = .3, cmap=cmap)
        cbar = fig.colorbar(cax1,ticks=np.arange(0,np.max(confus_data_count_arry),np.max(confus_data_count_arry)/float(10)))
        cbar.ax.set_ylabel('Classified Data Count',labelpad = 20,rotation= 270)
        #cax1.set_clim(0,1)

        for i in range(num_classes+1):
            for j in range(num_classes+1):
                ax1.annotate(str(B[i][j]),xy=(i,j),size = 10, xycoords = 'data',
                                 xytext=(i-.35,j))#, textcoords='offset points')

        ax1.annotate("User\n Accuracy %",xy=(i,j),size = 12, xycoords = 'data',
                        xytext=(num_classes-.5,-1.35))#, textcoords='offset points')

        ax1.annotate("Producer\n Accuracy %",xy=(i,j),size = 12, xycoords = 'data',
                        xytext=(-2.75,num_classes))#, textcoords='offset points')

        for i in range(num_classes):
            for j in range(num_classes):
                if i==j:
                    if (i<(num_classes/2)) and i != num_classes+1 and j != num_classes+1:
                        ax1.annotate(str(100*round(B[i][j]/float(total_classified_items[i]),3))+"%", xy=(i,j), xycoords='data',
                                     xytext=(i+50,j), textcoords='offset points',
                                     bbox=dict(boxstyle="round", fc="0.8"),
                                     arrowprops=dict(arrowstyle="fancy",facecolor='white'))
                    elif (i != num_classes+1 and j != num_classes+1):
                        ax1.annotate(str(100*round(B[i][j]/float(total_classified_items[i]),3))+"%", xy=(i,j), xycoords='data',
                                     xytext=(i-100,j-25), textcoords='offset points',
                                     bbox=dict(boxstyle="round", fc="0.8"),
                                     arrowprops=dict(arrowstyle="fancy",facecolor='white'))
        plt.show()
        plt.draw()
        return plt

    def plot_kernel_matrix(self,accuracy_arry,num_classes,note = ''):
        """Plot the Confusion Matrix in a Repeatable Format

        Args:
            confusion_data      = the data consists of
                      <-------------+input class+--------------------->
                ^    +--------------------------------------------------+
                |    |--------------------------------------------------|
                +    |--------------------------------------------------|
                predicted class |------data----------------------------------------|
                +    |--------------------------------------------------|
                |    |--------------------------------------------------|
                v    |--------------------------------------------------|
                     +--------------------------------------------------+
            number of classes - this dictates the size of the confusion matrix
            plot_note - this is an additional description to be added to the
                        title of the graph, this is useful to uniquely
                        identify the graph - which run - which classifier etc
        Return: Figure with confusion matrix plotted
        """
        logging.debug('plot_kernel_matrix')
        print ">>>>>>>>>plot the kernel impact matrix<<<<<<<<"

        A = np.copy(accuracy_arry)#.reshape(num_classes,num_classes))
        #from matplotlib import cm as cm #imported earlier
        # Kernel Importance Matrix
        B = np.copy(A)
        row_sum = np.sum(A,axis=0,keepdims=True)#/num_classes
        #col_sum = np.sum(A,axis=1,keepdims=True)#/num_classes
        col_ave = np.sum(A,axis=1,keepdims=True)/num_classes#/num_classes

        B = np.hstack((B,col_ave))

        B = np.hstack((np.zeros(col_ave.shape),B))
        B = np.vstack((np.zeros(B.shape[1]),B))

        A = np.hstack((np.zeros((A.shape[0],1)),A))
        A = np.vstack((np.zeros(A.shape[1]),A))
        #B = np.vstack((B,np.hstack((row_sum,[[0]]))))

        fig = plt.figure(figsize=(11.0, 5.0))
        x_major_ticks = np.arange(1,num_classes+1,1)
        y_major_ticks = np.arange(1,self.num_kernel+1,1)
        x_minor_ticks = np.arange(.5,num_classes+1.5,1)
        y_minor_ticks = np.arange(.5,self.num_kernel+1.5,1)

        ax1 = fig.add_subplot(111)
        ax1.set_xticks(x_major_ticks)
        ax1.set_xticks(x_minor_ticks, minor=True)
        ax1.set_yticks(y_major_ticks)
        ax1.set_yticks(y_minor_ticks, minor=True)


        #ax1.grid(which='both')
        #ax1.xaxis.tick_top()
        ax1.xaxis.set_view_interval(0,40,ignore=True)
        ax1.set_xlim(.5,num_classes+1.5)#20.5)
        ax1.set_ylim(.5,self.num_kernel+.5)#19.5)
        ax1.set_ylabel("Kernel Removed")
        ax1.set_xlabel("Class Index")
        #accuracy_txt = [ A[i][i] for i in range(len(total_classified_items))]
        #accuracy = np.sum(accuracy)/float(np.sum(A))
        ax1.set_title("Kernel Importance Matrix \n % Change from Baseline Classifier" + note + "\n")#np.sum(np.diag(A))/float(np.sum(np.sum(A,axis=1)))

        ax1.grid(b = True,which = 'minor')

        #Color bar settings
        cmap = cm.get_cmap('RdYlBu_r') # jet doesn't have white color but is dark when blue
        #cmap.set_bad('w') # default value is 'k'
        norm = MidpointNormalize(midpoint=0)
        cax1 = ax1.imshow(A, interpolation="nearest", cmap=cmap, norm = norm, alpha = .5)
        #ax2 = fig.add_axes([.8,.1,.03,.5]) #left, bottom, width height
        cbar = fig.colorbar(cax1,ticks= np.round((np.arange(np.min(A),np.max(A),(np.max(A)-np.min(A))/10)),1))
        cbar.ax.set_ylabel('% Change from Baseline Classifier',labelpad = 20,rotation= 270)

        #cax1.set_clim(0,1)

        ax1.annotate("Ave\n % Impact",xy=(1,1),size = 10,xycoords = 'data',xytext = (num_classes+.75,-.15))
        plt.show()
        plt.draw()
        #for each cell, annotaed
        for i in range(1,num_classes+2):
            for j in range(1,self.num_kernel+1):
                ax1.annotate(str(round(B[j][i],1)),xy=(i,j),size = 10, xycoords = 'data',
                                 xytext=(i-0.35,j-0.125))#, textcoords='offset points')


        plt.show()
        plt.draw()
        return plt

    def kernel_impact(self,data):
        """"
            self.kernel_importance()
            PROCESS
            1. Load:
            1.1 Trained Network
            1.2 Trained Network Definitions
            1.2 Test Data (truth)
            2. Capture the initial confusion matrix (capture the diagonals this is the baseline)
            3 The initial kernels
            4 For each kernel:
            4.1 Zero our each kernel
            4.2 calculate the confusion matrix; capture the diagonals)
            4.3 difference each kernel's diagonals and compile them into a matrix
            5. Plot the compiled accuracy difference matrix (format color map)
        """
        logging.debug('calculate kernel impact matrix')
        print ">>>>>>>>>calculate kerenel impact matrix<<<<<<<<"

        graph = self.graph
        tf_ph_dataset = self.tf_ph_dataset
        tf_ph_labels = self.tf_ph_labels
        tf_loss = self.tf_loss
        prediction_op = self.prediction_op
        optimizer_op = self.optimizer_op
        summary_op = self.summary_op
        saver_op = self.saver_op
        batch_size = self.batch_size
        num_steps = self.num_steps
        tf_kernel = self.tf_kernel
        tf_hid_weights = self.tf_hid_weights
        tf_conv1 = self.tf_conv1
        tf_unshape_hid_weights = self.tf_unshape_hid_weights
        data_arry = self.train_data
        labels_1_hot = self.train_labels_one_hot

        valid_data_arry = data.data
        valid_labels_1_hot = data.labels_one_hot

        step = 0
        #with tf.Session(graph=graph) as session:#graph=graph
        # or
        #session = tf.Session(graph=graph)
        #with session:
        # or
        session = tf.InteractiveSession(graph=graph)
        if True: #Absorb the indent
            """LOAD NETWORK"""
            if self.restore_graph == True:
                #TO RESTORE SAVED MODEL
                tf.initialize_all_variables().run(session=session) #included to initialize new vars
                saver = tf.train.Saver()
                saver.restore(session, self.current_model_path)
                print 'Model restored.'
            else:
                tf.initialize_all_variables().run(session=session)
                print("Initialized")

            """LOAD NETWORK PARAMETERS"""
            #Default valudes are loaded, this is manual process automation should be implemented
            #1. Manual Saving the file, possibly pickle the all parameters per saved model?

            """ENSURE DATA IS LOADED"""
            assert(len(valid_data_arry) != 0)
            assert(len(valid_labels_1_hot) != 0)

            """CAPTURE INITIAL CONFUSION MATRIX"""
            print ">>>>>>>>Create the balanced confusion matrix data<<<<<<<<"
                #the confusion matrix utilizes the full data set, it should only use
                #the validation data
                #has this been corrected [ ]
            confus_data_arry_base,per_class_accuracy_base = self.confusion_matrix_balanced(data = data)
            print ">>>>>>>>balanced confusion matrix data created<<<<<<<<"
            print ">>>>>>>>Plot the confusion matrix plot<<<<<<<<"
            fig = self.plot_confusion_matrix(confus_data_arry_base,self.num_labels,plot_note = ': HSIcNet - balanced')
       #%%
            """CAPTURE THE INITIAL KERNELS"""
            print 'capture the intial kernels'
            _,_,kernels_base = self.run_graph(valid_data_arry,valid_labels_1_hot) #Ignore the predictions capture the Kernels
            session.close()
        #%%
        """ZERO-OUT EACH KERNEL,DEFINE GRAPH AND TEST EACH ZEROED KERNEL"""
        #1. Add "zero out each kernel" code
        #for i in rane(self.num_labels):
        conf_matrix_per_kernel = []
        per_class_accuracy_per_kernel = []
        for i in range(self.num_kernel):
            conf_matrix_per_kernel.append([])
            per_class_accuracy_per_kernel.append([])
            kernels = np.copy(kernels_base)
            print 'kernels.shape: ', kernels.shape
            kernels[:,:,:,i] = 0 #Set the first kernel to zero

            self.define_graph_set_kernel(kernels)
            print '>>>>>>>>>Create interactive session for setting kernels<<<<<<<'
            session = tf.InteractiveSession(graph=graph)
            if True: #Absorb the indent

                """LOAD NETWORK"""
                print 'load the network'
                if self.restore_graph == True:
                    #TO RESTORE SAVED MODEL
                    tf.initialize_all_variables().run(session=session) #included to initialize new vars
                    saver = tf.train.Saver()
                    saver.restore(session, self.current_model_path)
                    print 'Model restored ready to set kernels.'
                else:
                    tf.initialize_all_variables().run(session=session)
                    print("Initialized")
                self.run_graph_set_kernels(valid_data_arry,valid_labels_1_hot,kernels)#runs the graphs and inputs kernels
                conf_matrix_per_kernel[i],per_class_accuracy_per_kernel[i] = self.confusion_matrix_balanced_zeroed_kernel(kernels,data)
                #DON"T PLOT EACH CONF MAT# fig = self.plot_confusion_matrix(conf_matrix_per_kernel[i],self.num_labels,plot_note = ': HSIcNet - balanced')
                #2. Add calculate per_class_accuracies
                """CALCULATE THE DIFFERENCES BETWEEN THE BASELINE AND EACH KERNEL"""
                """PLOT THE DIFFERENCES"""
            session.close()
        #%%
        a = np.asarray(per_class_accuracy_base)
        b = np.asarray(per_class_accuracy_per_kernel)
        c = [ b[i]-a for i in range(self.num_kernel)]
        fig = self.plot_kernel_matrix(c,self.num_labels,note = '')
        #RESTORE THE NETWORK BACK TO ITS ORIGINAL CONFIGURATION
        self.define_graph()
        return per_class_accuracy_base,per_class_accuracy_per_kernel,c,fig#,row_average

    def classify_envi_data(self, filename = 0,scaler = 1):
        """This function will take in filename and create a geotiff output it will also return a classified set of pixels"""
        logging.debug('classify_envi_data')
        print ">>>>>>>>classify envi data<<<<<<<<"

        hyspiri = hsi_data.hsi_data()
        hyspiri.standardized == False
        data_chunk = self.batch_size
        hyspiri.hyspiri_filename = os.path.basename(filename)

        ds = gdal.Open(filename)
        if ds is None:
            print 'Could not open ' + filename
            #sys.exit(1)

        cols = ds.RasterXSize
        rows = ds. RasterYSize
        bands = ds.RasterCount

        print "cols: ", cols," rows: ",rows," bands: ",bands
        logging.debug('cols: '+str(cols)+' rows: '+str(rows)+' bands: ' + str(bands))

        geotransform = ds.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]
        start_x, start_y = 0,0
        total_pred = []
        mask = []

        self.restore_session()

        t1 = time.time()
        classed_rows = 0
        hyspiri.scaler = scaler

        num_rows_per_classification = 200 #Arbitrary it just needs to fit within memory
        while classed_rows < rows: #for j in range(rows): #(rows):
            if classed_rows > rows - num_rows_per_classification:
                logging.debug('In the last Chunk classed_rows = '+str(classed_rows))
                num_rows_per_classification = rows - classed_rows
                logging.debug('The last chunk contiains: '+str(num_rows_per_classification) +' rows')
                start_y =classed_rows
                hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
                hyspiri.data = np.swapaxes(hyspiri.data,0,1)
                hyspiri.data = np.swapaxes(hyspiri.data,1,2)
                hyspiri.data,frst,scnd,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
                for i in range(len(hyspiri.data)):
                    if hyspiri.data[i].any() == 1:#modified to be any() not all()
                        mask.append(1) #if any piece of data contains a value then data is valid.
                    else:
                        mask.append(0)

                hyspiri.data,_ = hyspiri.standardize(scaler = 'exists')#self.standardize_by_spec(hyspiri.data)
                # Classify - The Remainder of the data
                # Input Batch_Size that is appropriate to the system
                # Input classification_data
                # Initizlied self
                classed_data = 0
                while classed_data < hyspiri.data.shape[0]:
                    if classed_data > hyspiri.data.shape[0] - self.batch_size:
                        self.batch_size = hyspiri.data.shape[0] - classed_data
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+self.batch_size,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        self.batch_size = data_chunk
                        classed_data += self.batch_size
                    else:
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        classed_data += self.batch_size

                    total_pred.extend(prediction[0])
                classed_rows += num_rows_per_classification
                logging.debug('classed_rows = '+str(classed_rows))

                print 'Classified Line: ', classed_rows, 'out of :', rows

            else:
                logging.debug('In std total Rows Loop i = '+str(classed_rows))

                start_y =classed_rows
                hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
                hyspiri.data = np.swapaxes(hyspiri.data,0,1)
                hyspiri.data = np.swapaxes(hyspiri.data,1,2)

                hyspiri.data,frst,scnd,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
                logging.debug('size of block classifying :' +str(sys.getsizeof(hyspiri.data)))
                for i in range(np.asarray(hyspiri.data).shape[0]):
                    if hyspiri.data[i].any() == 1:
                        mask.append(1)
                    else:
                        mask.append(0)

                hyspiri.data,_ = hyspiri.standardize(scaler = 'exists')#self.standardize_by_spec(hyspiri.data)
                # Classify - Full Frame
                # Input Batch_Size that is appropriate to the system
                # Input classification_data
                # Initizlied self

                classed_data = 0
                while classed_data < hyspiri.data.shape[0]:
                    if classed_data > hyspiri.data.shape[0] - self.batch_size:
                        self.batch_size = hyspiri.data.shape[0] - classed_data
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+self.batch_size,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        self.batch_size = data_chunk
                        classed_data += self.batch_size
                    else:
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        #print 'classified : ' , classed_data/data_chunk, ' data chuncks of: ', rows
                        classed_data += self.batch_size
                    total_pred.extend(prediction[0])
                classed_rows += num_rows_per_classification
                logging.debug('classed_rows = '+str(classed_rows))
                print 'Classified Lines: ', classed_rows , 'out of :', rows
        self.close_session()
        t2 = time.time()
        t3 = t2-t1
        logging.debug('Total Time to classify file: '+str(t3))
        #%% Format the Classified Probabilities
        mask = np.asarray(mask)

        #ADDED FOR SALT PEPPER ISSUE 22MAR17
        #reshaped_mask = np.reshape(mask,(rows,cols)) #Class
        #plt.figure()
        #plt.title('MASK for '+hyspiri.hyspiri_filename)
        #plt.imshow(np.uint8(reshaped_mask))
        #mask = np.asarray([1 for i in range(len(mask))])# this is to get rid of the mask mod 22mar17
        #END 22MAR17 ISSUE

        cnn_pred_prob = np.asarray(total_pred)
        cnn_pred_prob = np.asarray([cnn_pred_prob[i]*mask[i] for i in range(len(cnn_pred_prob))]) #apply mask

        cnn_pred_nums = np.argmax(cnn_pred_prob,axis=1)+1
        cnn_pred_nums = np.asarray([cnn_pred_nums[i]*mask[i] for i in range(len(cnn_pred_nums))]) #apply mask

        cnn_pred_confid = np.max(cnn_pred_prob,axis=1)
        cnn_pred_confid = np.asarray([cnn_pred_confid[i]*mask[i] for i in range(len(cnn_pred_confid))]) #apply mask

        bin = np.bincount(cnn_pred_nums)
        plt.figure()
        plt.title('CNN-Classification Distribution')
        plt.stem(bin)

        B = np.reshape(cnn_pred_nums,(rows,cols)) #Class
        C = np.reshape(cnn_pred_confid,(rows,cols)) #Confidence

        D = np.reshape(cnn_pred_prob, (rows,cols,cnn_pred_prob.shape[1])) #Per Class Confidence
        D = np.uint8(D*100)
        #%%Plot the map output from classification
        plt.figure()
        plt.title('CNN-Map'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(B))
        BB = np.uint8(B)

        plt.figure()
        plt.title('CNN-Confidences'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(C*100))
        CC = np.uint8(C*100)

        #%%GTiff -Output - 2 band

        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())
        outfile = driver.Create("CNN-ClassConfid-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, 2, GDT_Byte)
        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        outfile.GetRasterBand(1).WriteArray(BB, 0, 0)
        outfile.GetRasterBand(2).WriteArray(CC, 0, 0)

        outfile = None
        #%%GTIFF - Output - Multi Band Confidences

        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())

        numD = 12 #D.shape[2]  # weird quark with gdal... doesn't like accepting a D.shape[2]
        outfile = driver.Create("CNN-Confidences-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, numD , GDT_Byte)

        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        for i in range(numD):
            tmp = D[:,:,i]
            outfile.GetRasterBand(int(i+1)).WriteArray(tmp, 0, 0)
        outfile = None
    def classify_envi_data_summer(self, filename = 0,scaler = 1):
        """This function will take in filename and create a geotiff output it will also return a classified set of pixels"""
        logging.debug('classify_envi_data')
        print ">>>>>>>>classify envi data<<<<<<<<"

        hyspiri = hsi_data.hsi_data()
        hyspiri.standardized == False
        data_chunk = self.batch_size
        hyspiri.hyspiri_filename = os.path.basename(filename)

        ds = gdal.Open(filename)
        if ds is None:
            print 'Could not open ' + filename
            #sys.exit(1)

        cols = ds.RasterXSize
        rows = ds. RasterYSize
        bands = ds.RasterCount

        print "cols: ", cols," rows: ",rows," bands: ",bands
        logging.debug('cols: '+str(cols)+' rows: '+str(rows)+' bands: ' + str(bands))

        geotransform = ds.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]
        start_x, start_y = 0,0
        total_pred = []
        mask = []

        self.restore_session()

        t1 = time.time()
        classed_rows = 0
        hyspiri.scaler = scaler

        num_rows_per_classification = 200 #Arbitrary it just needs to fit within memory
        while classed_rows < rows: #for j in range(rows): #(rows):
            if classed_rows > rows - num_rows_per_classification:
                logging.debug('In the last Chunk classed_rows = '+str(classed_rows))
                num_rows_per_classification = rows - classed_rows
                logging.debug('The last chunk contiains: '+str(num_rows_per_classification) +' rows')
                start_y =classed_rows
                hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
                hyspiri.data = np.swapaxes(hyspiri.data,0,1)
                hyspiri.data = np.swapaxes(hyspiri.data,1,2)
                ####Next line modified to suit only capturing second season
                three_season_data,frst,hyspiri.data,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
                for i in range(len(hyspiri.data)):
                    if hyspiri.data[i].any() == 1:#modified to be any() not all()
                        mask.append(1) #if any piece of data contains a value then data is valid.
                    else:
                        mask.append(0)

                hyspiri.data,_ = hyspiri.standardize(scaler = 'exists')#self.standardize_by_spec(hyspiri.data)
                # Classify - The Remainder of the data
                # Input Batch_Size that is appropriate to the system
                # Input classification_data
                # Initizlied self
                classed_data = 0
                while classed_data < hyspiri.data.shape[0]:
                    if classed_data > hyspiri.data.shape[0] - self.batch_size:
                        self.batch_size = hyspiri.data.shape[0] - classed_data
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+self.batch_size,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        self.batch_size = data_chunk
                        classed_data += self.batch_size
                    else:
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        classed_data += self.batch_size

                    total_pred.extend(prediction[0])
                classed_rows += num_rows_per_classification
                logging.debug('classed_rows = '+str(classed_rows))

                print 'Classified Line: ', classed_rows, 'out of :', rows

            else:
                logging.debug('In std total Rows Loop i = '+str(classed_rows))

                start_y =classed_rows
                hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
                hyspiri.data = np.swapaxes(hyspiri.data,0,1)
                hyspiri.data = np.swapaxes(hyspiri.data,1,2)
                ####Next line modified to suit only capturing second season
                three_season_data,frst,hyspiri.data,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
                logging.debug('size of block classifying :' +str(sys.getsizeof(hyspiri.data)))
                for i in range(np.asarray(hyspiri.data).shape[0]):
                    if hyspiri.data[i].any() == 1:
                        mask.append(1)
                    else:
                        mask.append(0)

                hyspiri.data,_ = hyspiri.standardize(scaler = 'exists')#self.standardize_by_spec(hyspiri.data)
                # Classify - Full Frame
                # Input Batch_Size that is appropriate to the system
                # Input classification_data
                # Initizlied self

                classed_data = 0
                while classed_data < hyspiri.data.shape[0]:
                    if classed_data > hyspiri.data.shape[0] - self.batch_size:
                        self.batch_size = hyspiri.data.shape[0] - classed_data
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+self.batch_size,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        self.batch_size = data_chunk
                        classed_data += self.batch_size
                    else:
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                        batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        #print 'classified : ' , classed_data/data_chunk, ' data chuncks of: ', rows
                        classed_data += self.batch_size
                    total_pred.extend(prediction[0])
                classed_rows += num_rows_per_classification
                logging.debug('classed_rows = '+str(classed_rows))
                print 'Classified Lines: ', classed_rows , 'out of :', rows
        self.close_session()
        t2 = time.time()
        t3 = t2-t1
        logging.debug('Total Time to classify file: '+str(t3))
        #%% Format the Classified Probabilities
        mask = np.asarray(mask)

        #ADDED FOR SALT PEPPER ISSUE 22MAR17
        #reshaped_mask = np.reshape(mask,(rows,cols)) #Class
        #plt.figure()
        #plt.title('MASK for '+hyspiri.hyspiri_filename)
        #plt.imshow(np.uint8(reshaped_mask))
        #mask = np.asarray([1 for i in range(len(mask))])# this is to get rid of the mask mod 22mar17
        #END 22MAR17 ISSUE

        cnn_pred_prob = np.asarray(total_pred)
        cnn_pred_prob = np.asarray([cnn_pred_prob[i]*mask[i] for i in range(len(cnn_pred_prob))]) #apply mask

        cnn_pred_nums = np.argmax(cnn_pred_prob,axis=1)+1
        cnn_pred_nums = np.asarray([cnn_pred_nums[i]*mask[i] for i in range(len(cnn_pred_nums))]) #apply mask

        cnn_pred_confid = np.max(cnn_pred_prob,axis=1)
        cnn_pred_confid = np.asarray([cnn_pred_confid[i]*mask[i] for i in range(len(cnn_pred_confid))]) #apply mask

        bin = np.bincount(cnn_pred_nums)
        plt.figure()
        plt.title('CNN-Classification Distribution')
        plt.stem(bin)

        B = np.reshape(cnn_pred_nums,(rows,cols)) #Class
        C = np.reshape(cnn_pred_confid,(rows,cols)) #Confidence

        D = np.reshape(cnn_pred_prob, (rows,cols,cnn_pred_prob.shape[1])) #Per Class Confidence
        D = np.uint8(D*100)
        #%%Plot the map output from classification
        plt.figure()
        plt.title('CNN-Map'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(B))
        BB = np.uint8(B)

        plt.figure()
        plt.title('CNN-Confidences'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(C*100))
        CC = np.uint8(C*100)

        #%%GTiff -Output - 2 band

        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())
        outfile = driver.Create("CNN-ClassConfid-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, 2, GDT_Byte)
        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        outfile.GetRasterBand(1).WriteArray(BB, 0, 0)
        outfile.GetRasterBand(2).WriteArray(CC, 0, 0)

        outfile = None
        #%%GTIFF - Output - Multi Band Confidences

        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())

        numD = 12 #D.shape[2]  # weird quark with gdal... doesn't like accepting a D.shape[2]
        outfile = driver.Create("CNN-Confidences-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, numD , GDT_Byte)

        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        for i in range(numD):
            tmp = D[:,:,i]
            outfile.GetRasterBand(int(i+1)).WriteArray(tmp, 0, 0)
        outfile = None
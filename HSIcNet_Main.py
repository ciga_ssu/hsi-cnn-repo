# -*- coding: utf-8 -*-
"""
Created on Sat Jun 25 12:01:16 2016

Rev 0.1 - 13-Sept15
This is a snap shot to be submitted with the thesis.

@author: dgiudici

This script is written to test the HSIcNet class

This script will:

1. Take in Data - Functionality Provided by hsi_data_io class
2. Define Classifier
3. Train Classifier - Functionality provided by HSIcNet_Classifier class
4. Create Visuals of performance - Functionality provided by HSIcNet_Classifier class
    -Confusion Matrix (balanced)
    -Confusion Matrix (unbalanced)
    -Kernel Impact/Importance Matrix
5. Classify data - Functionality provided by HSIcNet_Classifier class
6. Create Map

"""


import numpy as np
import time as time
import csv
import matplotlib.pyplot as plt
import os
import sys
import time as time
import HSIcNet as hcn_class
HCN2 = hcn_class.HSIcNet()

import hsi_data as hsi_data
train = hsi_data.hsi_data()
valid = hsi_data.hsi_data()

import logging
logging.basicConfig(filename='LOG_HSI_Classifier_main.txt',level = logging.DEBUG, format= '%(asctime)s - %(levelname)s - %(message)s')
logging.debug('Log for HSIcNet_Main')


#%%######################################
###1. IMPORT DATA
logging.debug('1. Import Training Data')
logging.debug('Import Data')
train.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_training_filtered.csv'
valid.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'
train.data,train.labels, header,wavelength = train.load_data_any_3season(train.input_filename)
valid.data,valid.labels, header,wavelength = valid.load_data_any_3season(valid.input_filename)
#Randomize Data For Training and Testing
#%%
train.data,train.labels = hsi_data.randomize(train.data,train.labels)
valid.data,valid.labels = hsi_data.randomize(valid.data,valid.labels)
#%%
#Standardize the data (optional)
_,train_scaler = train.standardize(0)   #determine the scaler to use for all data also stored in train.scaler
valid.scaler = train_scaler
valid.standardize(scaler = train_scaler)
#%%
train.create_variants_labels() # important to do other wise data sets won't match after randomize
valid.create_variants_labels()
HCN2.standardized = train.standardized
HCN2.num_labels = train.num_labels   #carry over variables these are required for cNet definition
HCN2.data_length = train.data_length #carry over variables these are required for cNet definition


#%%######################################
###2. Define the Classifier
logging.debug('2. Define the Classifier Architecture')
#CNN Architecture
logging.debug('Set Hyper Parameters')
HCN2.kernel_size =       10         # good results were had with kernel_size = 9
HCN2.num_kernel =        7
HCN2.num_hidden =        100         #number of hidden nodes
HCN2.reg_level_hidden =  1e-4        #Regularization on the hidden layer
HCN2.reg_level_kernel =  1e-5        #i #1e-2 #np.power(float(10),-(2)) #np.power(1,-1*i) #<== Not verified

HCN2.pooling =           True        #Flag for Hyper Param Saving
HCN2.threeseason =       True        #Flag for Hyper Param Saving
HCN2.dropout_level =     .5          #50% dropout rate
#HCN2.learning_rate =     .1    #This value will get over written
HCN2.num_steps =         10000#4e6 + 1     #Total number of batched epochs learned by the network
HCN2.batch_size =        20          #This is how many data points are subjected to the graph at a time (smaller enables quick snapshots of how the graph is training; larger will make bigger training jumps)
HCN2.init_lrn_rate =     .7          #The starting learning rate (this will be reduced by the train_size factor)
HCN2.train_size =        2000000     #This is the factor by which the learning rate decays

HCN2.restore_graph =     True       # Restore A high performing classifier

#This is the saved classifier used for thesis
HCN2.current_model_path = '/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/CNN_stdzd_Classifier-3Season'
graph_params_filePath = '/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/GraphParams_CNN_stdzd_Classifier_3Season.PKL'

HCN2.HyperParamNote = 'InsertNote'

#See if directory exists if not create it
if HCN2.restore_graph == True:
    note = 'KernelExp'
else:
    note = 'KernelExp'

run_directory = '/home/dgiudici/repos/hsi-cnn-repo/TrainingRuns/HSI_MOD_'+note+'_'+HCN2.get_run_desc()+'/'
if not os.path.exists(run_directory):
    os.makedirs(run_directory)
    os.makedirs(run_directory+'models')
    os.makedirs(run_directory+'kernels')

HCN2.model_path = run_directory+'models/'
HCN2.kernel_path = run_directory +'kernels/'

if HCN2.restore_graph == False: #If not restoring graph params create one
    graph_params_filePath = HCN2.set_graph_def_params(baseFilePath = run_directory)#Saves the graph definition parameters to a pickle

#Import graph definition parameter sets HCN2.data and the respective parameters
logging.debug('Import Hyper Parameters')
HCN2.get_graph_def_params(graph_params_filePath)
print HCN2.imported_params

#Define the Graph - HCN2.graph will contain a un restored graph architecture
logging.debug('Define Graph')
HCN2.define_graph()

#%%3. Train Classifier
train_graph = True
if train_graph == True:
    print ">>>>>>>>>>Train Graph<<<<<<<<<"
    print "batch_size: ", HCN2.batch_size
    start_time = time.time()
    trained_graph, trained_kernels,loss_lst,pred_lst,vpred_lst,hid_weights,conv1 = HCN2.train_graph(data = train, v_data = valid)
    stop_time = time.time()
    HCN2.current_model_path = trained_graph
    print "the total time to train the network 4mil epoch of size 20 batch: ", stop_time-start_time

#%%3. or Run Classifier
logging.debug('3. Run Classifier')
run_individual_run = True
if run_individual_run == True:
    HCN2.restore_session()
    HCN2.run_graph_open_session(train.data,train.labels_one_hot)
    HCN2.close_session()

#%%######################################
###4. Test Classifier - Create Visuals
logging.debug('5. Test Classifier')
test_classifier = True
if test_classifier ==True:
    # Create unbalanced confusion matrix
    print ">>>>>>>>Create the unbalanced confusion matrix <<<<<<<<"
    confus_data_count_arry = HCN2.confusion_matrix_unbalanced(data = valid)
    print ">>>>>>>>Unbalanced confusion matrix Created<<<<<<<<"
    print ">>>>>>>>Plot the confusion matrix plot<<<<<<<<"
    #fig = HCN2.plot_confusion_matrix(confus_data_count_arry,HCN2.num_labels,plot_note = ': HSIcNet - unbalanced')
    fig = HCN2.plot_confusion_matrix(confus_data_count_arry,HCN2.num_labels,plot_note = 'CNN')
    print ">>>>>>>>NOTE: may need to delete the existing .png in the directory or rename"
    fig.savefig(run_directory + HCN2.get_run_desc() +'unbal.png')
    print ">>>>>>>>File:", HCN2.get_run_desc()+ " has been created"

    # Create balanced confusion matrix
    print ">>>>>>>>Create the balanced confusion matrix data<<<<<<<<"
    confus_data_count_arry,_ = HCN2.confusion_matrix_balanced(data = valid)
    print ">>>>>>>>balanced confusion matrix data created<<<<<<<<"
    print ">>>>>>>>Plot the confusion matrix plot<<<<<<<<"
    fig = HCN2.plot_confusion_matrix(confus_data_count_arry,HCN2.num_labels,plot_note = ': HSIcNet - balanced')

    print ">>>>>>>>may need to delete the existing .png in the directory or rename"
    fig.savefig(run_directory + HCN2.get_run_desc() +'bal.png')
    print ">>>>>>>>File:", HCN2.get_run_desc()+ " has been created"
    # Create kernel_impact_matrix
#%%
    per_class_accuracy_base,per_class_accuracy_per_kernel,C,fig = HCN2.kernel_impact(data = valid) #this may take some time
    fig.savefig(run_directory + HCN2.get_run_desc() +'Kernel_Impact.png')

#%%######################################
###5. Classify data
logging.debug('5. CLASSIFY DATA')

import gdal
from gdalconst import *

hyspiri = hsi_data.hsi_data()


classify_single_file = False

if classify_single_file == True:

    hyspiri.hyspiri_filename = 'BayArea_59-1_f150430t01p00r17_f150611t01p00r09_f151002t01p00r20_masked_30m'
    hyspiri.hyspiri_filepath= '/media/dgiudici/Seagate Backup Plus Drive/raw_2015_data/'

    start_time = time.time()
    ds = gdal.Open(hyspiri.hyspiri_filepath + hyspiri.hyspiri_filename)
    if ds is None:
        print 'Could not open ' + hyspiri.hyspiri_filepath+hyspiri.hyspiri_filename
        #sys.exit(1)

    cols = ds.RasterXSize
    rows = ds. RasterYSize
    bands = ds.RasterCount

    print "cols: ", cols," rows: ",rows," bands: ",bands
    logging.debug('cols: '+str(cols)+' rows: '+str(rows)+' bands: ' + str(bands))
    ##%%
    geotransform = ds.GetGeoTransform()
    originX = geotransform[0]
    originY = geotransform[3]
    pixelWidth = geotransform[1]
    pixelHeight = geotransform[5]

    #already defined from above
    #param_filepath = '/home/dgiudici/HSI/BestClassifiers/HSI_MODnk7_ks10_nh500_ns4000001_rlh1e-05_rlk1e-05_dol0.5_lr0.1_pTrue_tsTrue/GRAPH_PARAMS_nk7_ks10_nh500_ns4000001_rlh1e-05_rlk1e-05_dol0.5_lr0.1_pTrue_tsTrue.PKL'
    #model_filepath = '/home/dgiudici/HSI/BestClassifiers/HSI_MODnk7_ks10_nh500_ns4000001_rlh1e-05_rlk1e-05_dol0.5_lr0.1_pTrue_tsTrue/models/MODEL_nk7_ks10_nh500_ns4000001_rlh1e-05_rlk1e-05_dol0.5_lr0.0899585_pTrue_tsTrue-4000000'

    print ">>>>>>>Define classifier graph<<<<<<<<"
    HCN2.get_graph_def_params(graph_params_filePath)
    #HCN2.current_model_path = model_filepath
    data_chunk = 7500 #This chunk is the max size for the graph (suspect graphics memory limitation)
    HCN2.batch_size = data_chunk
    HCN2.data_length = train.data.shape[1]
    HCN2.define_graph()
    HCN2.restore_graph = True
    print ">>>>>>>Graph is defined<<<<<<<"

    ##%% HSI MULTISEGMENT LOOP
    logging.debug('Process HSI Single Segment')
    print ">>>>>>>>Process HSI SingleSegment"

    ##Classifiy single dataset
    HCN2.classify_envi_data(filename = hyspiri.hyspiri_filepath+hyspiri.hyspiri_filename ,scaler = train.scaler)
    stop_time = time.time()
    print "the time it took to classify:", hyspiri.hyspiri_filepath+hyspiri.hyspiri_filename , " is: ", stop_time - start_time

################################################################################
##Process many multisegment files
#%%

import os
a = []

ClassifyMapData = False

if ClassifyMapData == True:
    logging.debug('Process HSI MultiFile')
    print ">>>>>>>>Process HSI MultiFile"

    folder = '/media/dgiudici/Seagate Backup Plus Drive/raw_2015_data/'
    hyspiri.hyspiri_filepath = folder

    print ">>>>>>>Define classifier graph<<<<<<<<"
    HCN2.get_graph_def_params(graph_params_filePath)
    #HCN2.current_model_path = model_filepath
    data_chunk = 7500 #This chunk is the max size for the graph (suspect graphics memory limitation)
    HCN2.batch_size = data_chunk
    HCN2.data_length = train.data.shape[1]
    HCN2.define_graph()
    HCN2.restore_graph = True
    print ">>>>>>>Graph is defined<<<<<<<"

    ####################################
    ##Capture the files
    for file in os.listdir(folder):
        if file.endswith('m'):
            a.append(file)

    ###################################
    ##Process each file
    for i in a:
        print 'processing: ' , i
        logging.debug('Processing: ' + i )
        T1 = time.time()
        hyspiri.hyspiri_filename = i
        HCN2.classify_envi_data(filename = hyspiri.hyspiri_filepath+ str(i) ,scaler = train.scaler)
        T2 = time.time()
        T3 = T2-T1
        logging.debug('Processing Complete, it took: ' + str(T3) + ' seconds')
    print 'HSIcNet_Main script is has finished running'
    #%%
    '''
    start_x, start_y = 0,0
    total_pred = []
    mask = []

    HCN.restore_session()

    t1 = time.time()
    classed_rows = 0
    hyspiri.scaler = train.scaler

    num_rows_per_classification = 200 #Arbitrary it just needs to fit within memory
    while classed_rows < rows: #for j in range(rows): #(rows):
        if classed_rows > rows - num_rows_per_classification:
            logging.debug('In the last Chunk classed_rows = '+str(classed_rows))
            num_rows_per_classification = rows - classed_rows
            logging.debug('The last chunk contiains: '+str(num_rows_per_classification) +' rows')
            start_y =classed_rows
            hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
            hyspiri.data = np.swapaxes(hyspiri.data,0,1)
            hyspiri.data = np.swapaxes(hyspiri.data,1,2)
            hyspiri.data,frst,scnd,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
            for i in range(len(hyspiri.data)):
                if hyspiri.data[i].all() == 0:
                    mask.append(0)
                else:
                    mask.append(1)

            hyspiri.data,_ = hyspiri.standardize()#HCN.standardize_by_spec(hyspiri.data)
            # Classify - The Remainder of the data
            # Input Batch_Size that is appropriate to the system
            # Input classification_data
            # Initizlied HCN
            classed_data = 0
            while classed_data < hyspiri.data.shape[0]:
                if classed_data > hyspiri.data.shape[0] - HCN.batch_size:
                    HCN.batch_size = hyspiri.data.shape[0] - classed_data
                    batch_data = np.copy(hyspiri.data[classed_data:classed_data+HCN.batch_size,:])
                    L = batch_data
                    batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                    batch_label = np.zeros((batch_data.shape[0],HCN.num_labels),dtype = float)
                    #prediction,run_duration,_ = HCN.run_graph(batch_data,batch_label)
                    prediction,run_duration,_ = HCN.run_graph_open_session(batch_data,batch_label)
                    logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                    HCN.batch_size = data_chunk
                    classed_data += HCN.batch_size
                else:
                    batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                    batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                    batch_label = np.zeros((batch_data.shape[0],HCN.num_labels),dtype = float)
                    #prediction,run_duration,_ = HCN.run_graph(batch_data,batch_label)
                    prediction,run_duration,_ = HCN.run_graph_open_session(batch_data,batch_label)
                    logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                    classed_data += HCN.batch_size

                total_pred.extend(prediction[0])
            classed_rows += num_rows_per_classification
            logging.debug('classed_rows = '+str(classed_rows))

            print 'Classified Line: ', classed_rows, 'out of :', rows

        else:
            logging.debug('In std total Rows Loop i = '+str(classed_rows))

            start_y =classed_rows
            hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
            hyspiri.data = np.swapaxes(hyspiri.data,0,1)
            hyspiri.data = np.swapaxes(hyspiri.data,1,2)

            hyspiri.data,frst,scnd,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
            logging.debug('size of block classifying :' +str(sys.getsizeof(hyspiri.data)))
            for i in range(np.asarray(hyspiri.data).shape[0]):
                if hyspiri.data[i].all() == 0:
                    mask.append(0)
                else:
                    mask.append(1)
            hyspiri.data,_ = hyspiri.standardize()
            # Classify - Full Frame
            # Input Batch_Size that is appropriate to the system
            # Input classification_data
            # Initizlied HCN

            classed_data = 0
            while classed_data < hyspiri.data.shape[0]:
                if classed_data > hyspiri.data.shape[0] - HCN.batch_size:
                    HCN.batch_size = hyspiri.data.shape[0] - classed_data
                    batch_data = np.copy(hyspiri.data[classed_data:classed_data+HCN.batch_size,:])
                    L = batch_data
                    batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                    batch_label = np.zeros((batch_data.shape[0],HCN.num_labels),dtype = float)
                    #prediction,run_duration,_ = HCN.run_graph(batch_data,batch_label)
                    prediction,run_duration,_ = HCN.run_graph_open_session(batch_data,batch_label)
                    logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                    HCN.batch_size = data_chunk
                    classed_data += HCN.batch_size
                else:
                    batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                    batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                    batch_label = np.zeros((batch_data.shape[0],HCN.num_labels),dtype = float)
                    #prediction,run_duration,_ = HCN.run_graph(batch_data,batch_label)
                    prediction,run_duration,_ = HCN.run_graph_open_session(batch_data,batch_label)
                    logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                    #print 'classified : ' , classed_data/data_chunk, ' data chuncks of: ', rows
                    classed_data += HCN.batch_size
                total_pred.extend(prediction[0])
            classed_rows += num_rows_per_classification
            logging.debug('classed_rows = '+str(classed_rows))
            print 'Classified Lines: ', classed_rows , 'out of :', rows
    HCN.close_session()
    t2 = time.time()
    t3 = t2-t1
    logging.debug('Total Time to classify file: '+str(t3))#%%
    from sklearn.cross_validation import cross_val_score

    scores = cross_val_score(clf, iris.data, iris.target)
    scores.mean()
    #%% Format the Classified Probabilities
    mask = np.asarray(mask)
    cnn_pred_prob = np.asarray(total_pred)
    cnn_pred_prob = np.asarray([cnn_pred_prob[i]*mask[i] for i in range(len(cnn_pred_prob))]) #apply mask
    cnn_pred_nums = np.argmax(cnn_pred_prob,axis=1)+1
    cnn_pred_nums = np.asarray([cnn_pred_nums[i]*mask[i] for i in range(len(cnn_pred_nums))]) #apply mask
    cnn_pred_confid = np.max(cnn_pred_prob,axis=1)
    cnn_pred_confid = np.asarray([cnn_pred_confid[i]*mask[i] for i in range(len(cnn_pred_confid))]) #apply mask

    bin = np.bincount(cnn_pred_nums)
    plt.figure()
    plt.title('CNN-Classification Distribution')
    plt.stem(bin)

    B = np.reshape(cnn_pred_nums,(rows,cols)) #Class
    C = np.reshape(cnn_pred_confid,(rows,cols)) #Confidence

    D = np.reshape(cnn_pred_prob, (rows,cols,cnn_pred_prob.shape[1])) #Per Class Confidence
    D = np.uint8(D*100)
    #%%Plot the map output from classification
    plt.figure()
    plt.title('CNN-Map')
    plt.imshow(np.uint8(B))
    BB = np.uint8(B)

    plt.figure()
    plt.title('CNN-Confidences')
    plt.imshow(np.uint8(C*100))
    CC = np.uint8(C*100)

    #%%GTiff -Output - 2 band

    driver = gdal.GetDriverByName("GTiff")
    #driver.Register()
    driver.SetMetadata(ds.GetMetadata())
    outfile = driver.Create("CNN-ClassConfid-"+hyspiri.hyspiri_filename+".tiff", cols, rows, 2, GDT_Byte)
    outfile.SetGeoTransform(geotransform)
    outfile.SetProjection(ds.GetProjectionRef())
    outfile.GetRasterBand(1).WriteArray(BB, 0, 0)
    outfile.GetRasterBand(2).WriteArray(CC, 0, 0)

    outfile = None
    #%%GTIFF - Output - Multi Band Confidences

    driver = gdal.GetDriverByName("GTiff")
    #driver.Register()
    driver.SetMetadata(ds.GetMetadata())

    numD = 12 #D.shape[2]  # weird quark with gdal... doesn't like accepting a D.shape[2]
    outfile = driver.Create("CNN-Confidences-"+hyspiri.hyspiri_filename+".tiff", cols, rows, numD , GDT_Byte)

    outfile.SetGeoTransform(geotransform)
    outfile.SetProjection(ds.GetProjectionRef())
    for i in range(numD):
        tmp = D[:,:,i]
        outfile.GetRasterBand(int(i+1)).WriteArray(tmp, 0, 0)
    outfile = None
    '''
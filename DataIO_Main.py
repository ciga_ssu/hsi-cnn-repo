# -*- coding: utf-8 -*-
"""
Created on Thu Jun 23 23:00:56 2016

@author: dgiudici

This script is written to test the data IO class

The HSI_data_io class needs to support:

1. Input of data into class
    - Single Season Data
    - Three Season Data   [done 24jun16]
2. Pre Processing of data
    - Standardization     [done 24jun16]
3. Output of data
    -Creation of classified .csv files (not prioirty)
    -Creation of classified Gtiff maps

"""

import numpy as np
import time as time
import csv
import matplotlib.pyplot as plt
import os
import copy as copy
#import HSIcNetLib_27_OpenSession as HcN
#HCN = HcN.HSIcNet()

import hsi_data as hsi_data
train = hsi_data.hsi_data()
valid = hsi_data.hsi_data()

import logging
logging.basicConfig(filename='LOG_DataIO_main.txt',level = logging.DEBUG, format= '%(asctime)s - %(levelname)s - %(message)s')
logging.debug('Log for DataIO_main script')

#%%1. IMPORT DATA

train.input_filename = '/home/ciga/CNN-Project/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_summer_training_filtered.csv'
valid.input_filename = '/home/ciga/CNN-Project/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'

train.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_training_filtered.csv'
valid.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'

#3season data
#train.data,train.labels, header,wavelength = train.load_data_any_3season(train.input_filename)
#valid.data,valid.labels, header,wavelength = valid.load_data_any_3season(valid.input_filename)
#1season data
train.data,train.labels, header,wavelength = train.load_data_any_3season(train.input_filename)
valid.data,valid.labels, header,wavelength = valid.load_data_any_3season(valid.input_filename)
#Randomize Data For Training and Testing
    #train.data,train.labels = hsi_data.randomize(train.data,train.labels)
    #valid.data,valid.labels = hsi_data.randomize(valid.data,valid.labels)
#Standardize the data (optional)

train_unstd = copy.deepcopy(train)

_,train_scaler = train.standardize(0)   #determine the scaler to use for all data also stored in train.scaler
valid.standardize(train_scaler)

train_unstd.create_variants_labels()
train.create_variants_labels() # important to do other wise data sets won't match after randomize
valid.create_variants_labels()

#%%2. Evaluate and Critique data (optional)

#%%
train_bincount = np.bincount(train.labels_num)
valid_bincount = np.bincount(valid.labels_num)
num_classes_train = len(set(train.labels)) # this is the number of classes
num_classes_valid = len(set(valid.labels))
ind_train = np.arange(num_classes_train)                # the x locations for the groups
ind_valid = np.arange(num_classes_valid)                # the x locations for the groups
width = .35
plt.figure(200,figsize = (12,8))
#plt.suptitle('Distribution of Training and Validation Data Sets',fontsize = 12)

labels = [1,2,3,4,5,6,7,8,9,0,1,2]
labels[0] = 'Annual\n Crops'
labels[1] = 'Bare'
labels[2] = 'Built-Up'
labels[3] = 'Evergreen\n Needleleaf\n Trees (ENT)'
labels[4] = 'Deciduous\n Broadleaf\n Trees (DBT)'
labels[5] = 'Evergreen\n Broadleaf\n Trees (EBT)'
labels[6] = 'Shrubs'
labels[7] = 'Dune\n Vegetation'
labels[8] = 'Perennial\n Crops'
labels[9] = 'Tidal Marsh'
labels[10] = 'Herbaceous'
labels[11] = 'Urban Vegetated\n Area'

plt.subplot(211)
plt.title('Training')
plt.ylabel('Count of Data of Each Class')
plt.bar(ind_train-width/2, train_bincount, width,color='black')
plt.xticks(range(len(ind_train)),ind_train)
plt.xticks(np.arange(-.25,len(ind_train),1),labels, rotation=45)
plt.xlabel('Class')
plt.xlim([-.5,num_classes_train-1+width/2])
plt.grid()

#plot % reflectance
plt.subplot(212)
plt.title('Validation')
plt.ylabel('Count of Data of Each Class')
plt.bar(ind_train-width/2, valid_bincount, width,color='black')
plt.xticks(range(len(ind_train)),ind_train)
plt.xticks(np.arange(-.25,len(ind_train),1),labels, rotation=45)
plt.xlabel('Class')
plt.xlim([-width/2,num_classes_train-1+width/2])
plt.xlim([-.5,num_classes_train-1+width/2])
#plt.subplots_adjust(top=.65)
plt.grid()


'''Plot Count
plt.subplot(212)
plt.title('Validation')
plt.ylabel('Count of Data of Each Class')
plt.bar(ind_train-width/2, valid_bincount, width,color='black')
plt.xticks(range(len(ind_train)),ind_train)
plt.xticks(np.arange(-.25,len(ind_train),1),labels, rotation=45)
plt.xlabel('Class Index')
plt.xlim([-width/2,num_classes_train-1+width/2])
plt.xlim([-.5,num_classes_train-1+width/2])
#plt.subplots_adjust(top=.65)
plt.grid()
'''
#plt.tight_layout()#rect=[0,.03,1,.95])
plt.tight_layout(rect=[0,0,1,.95])
plt.show()

#%% Plot each Class 25 times

zipped_label_data = zip(train_unstd.labels_num,train_unstd.data)
#x = [x for y,x in np.sort(zip(train_labels_arry,train_data_arry),]
zipped_label_data = sorted(zipped_label_data,key=lambda class_data: class_data[0])
sorted_labels = [zipped_label_data[x][0] for x in range(len(zipped_label_data))]
sorted_data = [zipped_label_data[x][1] for x in range(len(zipped_label_data))]

bincounts = np.bincount(sorted_labels)

cum_sum = np.cumsum(bincounts)

'''
plt.figure()
sorted_data = np.asarray(sorted_data)
x_label_idx = [0,32,52,68,95,96,150,171,199]
x_labels = [365,655,850,1005,1263,1452,1791,1967,2257]
x_labels_full = x_labels*3
x_label_idx_arry = np.asarray(x_label_idx)
x_label_idx_full = list(x_label_idx_arry) + list(x_label_idx_arry+224) + list(x_label_idx_arry+224*2)
'''
plt.figure(2,figsize=(23,11))
#REMOVE TITLE FOR PAPER
#plt.suptitle('Sample of 25 Spectral Profiles From Each Class (Single Season)',weight = 'bold', fontsize = 14)
sorted_data = np.asarray(sorted_data)
for j in range(25):
    for i in range(len(cum_sum)):
        plt.subplot(3,4,i+1)
        plt.title(labels[i],weight = 'bold', y = 1.05,fontsize = 12)
        if i == 0:
            plt.plot(sorted_data[i+j,:]/100)
            #plt.xticks(x_label_idx_full,x_labels_full, rotation = 'vertical',fontsize = 8)
            #plt.yticks(fontsize = 8)
            plt.xlabel('Three Season Spectral Band #')
            plt.ylabel('% Reflectance')
            plt.grid()
            plt.ylim([0,70])
        else:
            plt.xlabel('Three Season Spectral Band #')
            plt.ylabel('% Reflectance')
            plt.plot(sorted_data[cum_sum[i-1]+j,:]/100)
            #plt.xticks(x_label_idx_full,x_labels_full, rotation = 'vertical',fontsize = 8)
            #plt.yticks(fontsize = 8)
            plt.ylim([0,70])
            plt.grid()
plt.tight_layout(rect=[.1,0,.9,.95])

'''
#%%######################
###Plot Standardized Data


#import hsi_data_io_1 as hsi_data
train = hsi_data.hsi_data()
valid = hsi_data.hsi_data()

#%%1. IMPORT DATA
train.input_filename = '/home/ciga/CNN-Project/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_summer_training_filtered.csv'
valid.input_filename = '/home/ciga/CNN-Project/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'
train.data,train.labels, header,wavelength = train.load_data_any_3season(train.input_filename)
valid.data,valid.labels, header,wavelength = valid.load_data_any_3season(valid.input_filename)

#Randomize Data For Training and Testing
train.data,train.labels = hsi_data.randomize(train.data,train.labels)
valid.data,valid.labels = hsi_data.randomize(valid.data,valid.labels)
#Standardize the data (optional)

train_unstd = copy.copy(train)

_,train_scaler = train.standardize(0)   #determine the scaler to use for all data also stored in train.scaler
valid.standardize(train_scaler)
train.create_variants_labels() # important to do other wise data sets won't match after randomize
valid.create_variants_labels()

train_unstd.create_variants_labels()

fig = plt.figure()
fig.suptitle('Standardized and Non-Standardized Data Example')
ax = fig.add_subplot(111)
ax.plot(train_unstd.data[100], 'b', label = 'Non-Standardized Data')
ax.plot(0,'g', label = 'Standardized Data')
ax2 = ax.twinx()
ax2.plot(train.data[100], 'g', label = 'Standardized Data')


ax.grid()
ax.set_xlabel("Spectra")
ax2.set_ylim([-2.5,4])
ax.set_ylim([-5000,6000])
ax2.grid()
ax.legend(loc = 0)
ax.set_ylabel('Non-Standardized Reflectance')
ax2.set_ylabel('Standardized Reflectance')

plt.draw()
plt.show()

#%%Create some backgrounds shades
import matplotlib

num_bands = 186


fig = plt.figure()
fig.suptitle('Standardized and Non-Standardized Data Example')
ax = fig.add_subplot(111)
rect1 = matplotlib.patches.Rectangle((num_bands*0,-5000), num_bands, 11000, alpha = .2, color='green')
rect2 = matplotlib.patches.Rectangle((num_bands*1,-5000), num_bands, 11000, alpha = .2, color='yellow')
rect3 = matplotlib.patches.Rectangle((num_bands*2,-5000), num_bands, 11000, alpha = .2, color='grey')
ax.add_patch(rect1)
ax.add_patch(rect2)
ax.add_patch(rect3)
ax.annotate("Spring", xy=(0.15, 0.05), xycoords="axes fraction")
ax.annotate("Summer", xy=(0.45, 0.05), xycoords="axes fraction")
ax.annotate("Fall", xy=(0.8, 0.05), xycoords="axes fraction")
ax.plot(train_unstd.data[100], 'b', label = 'Non-Standardized Data')
ax.plot(0,'g', label = 'Standardized Data')
ax2 = ax.twinx()
ax2.plot(train.data[100], 'g', label = 'Standardized Data')


ax.grid()
ax.set_xlabel("Spectra")
ax2.set_ylim([-2.5,5])
ax.set_ylim([-5000,6000])
ax2.grid()
ax.legend(loc = 0)
ax.set_ylabel('Non-Standardized Reflectance')
ax2.set_ylabel('Standardized Reflectance')

ax.set_xlim([0,num_bands*3])

plt.draw()
plt.show()'''
# -*- coding: utf-8 -*-
"""
Rev 0.1 - 13-Sept15
This is a snap shot to be submitted with the thesis.


KERNEL VISUALIZATIONS
- This script will read in a file from a HSIcNet## and will visualize this data



Proposed methods are:
1. Heat map via occluding data
2. observing kernel within averaged data.
Created on Sat Mar 12 12:36:40 2016

Pre-req:
1. Data-balance.#.py needs to be ran to create the averaged data pickle
2. HSIcNet.#.py needs to be ran to create the kernel picle

@author: dgiudici
"""

import numpy as np
import time as time
import csv
import matplotlib.pyplot as plt
import os
import sys
from six.moves import cPickle as pickle
import scipy.signal as sp
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import copy as copy
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import ticker
#import matplotlib.pyplot as plt

import HSIcNet as hcn_class
HCN = hcn_class.HSIcNet()
#%%
import hsi_data as hsi_data
train = hsi_data.hsi_data()
valid = hsi_data.hsi_data()

import logging
logging.basicConfig(filename='LOG_HSI_Classifier_main_single_season.txt',level = logging.DEBUG, format= '%(asctime)s - %(levelname)s - %(message)s')
logging.debug('Log for HSIcNet_Main (single season plot generation)')


#%%######################################
###1. IMPORT DATA
logging.debug('1. Import Training Data')
logging.debug('Import Data')
train.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_summer_training_filtered.csv'
valid.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'
train.data,train.labels, header,wavelength = train.load_data_any_3season(train.input_filename)
valid.data,valid.labels, header,wavelength = valid.load_data_any_3season(valid.input_filename)
#Randomize Data For Training and Testing
train.data,train.labels = hsi_data.randomize(train.data,train.labels)
valid.data,valid.labels = hsi_data.randomize(valid.data,valid.labels)
#Standardize the data (optional)

train_unstd = copy.deepcopy(train)
valid_unstd = copy.deepcopy(valid)
#%%
_,train_scaler = train.standardize(0)   #determine the scaler to use for all data also stored in train.scaler
valid.scaler = train_scaler
valid.standardize(scaler = train_scaler)

valid_unstd.create_variants_labels()
train_unstd.create_variants_labels()
train.create_variants_labels() # important to do other wise data sets won't match after randomize
valid.create_variants_labels()

HCN.standardized = train.standardized
HCN.num_labels = train.num_labels   #carry over variables these are required for cNet definition
HCN.data_length = train.data_length #carry over variables these are required for cNet definition

modified_labels = ["Annual Crops", \
                   "Bare" , \
                   "Built-Up", \
                   "Evergreen Needleleaf Trees (ENT)", \
                   "Deciduous Broadleaf Trees (DBT)", \
                   "Evergreen Broadleaf Trees (EBT)", \
                   "Shrubs", \
                   "Dune Vegetation", \
                   "Perennial Crops", \
                   "Tidal Marsh", \
                   "Herbaceous", \
                   "Urban Vegetated"]

###READ IN KERNELS
#%%
#fileName = "ker9-kreg_HSIcNet.15.pickle" #<<<=====  change the file name to evaluate
#kernels = pickle.load(open(str(os.path.dirname(__file__))+ "/ModelVars/" + fileName, 'rb'))
kernels = pickle.load(open('/home/dgiudici/repos/hsi-cnn-repo/TrainingRuns/HSI_MOD_KernelExp_nk7_ks10_nh100_ns10000_rlh0.0001_rlk1e-05_dol0.5_lr0.1_pTrue_tsTrue/kernels/KERNELS_nk7_ks10_nh500_ns10000_rlh1e-05_rlk1e-05_dol0.5_lr0.0899585_pTrue_tsTrue.PKL','rb'))


#%%##READ IN AVERAGED_DATA
#fileName = "averagedClassData.pickle" #<<<=====  change the file name to evaluate
#averaged_class_data = pickle.load(open(str(os.path.dirname(__file__))+ "/ModelVars/" + fileName, 'rb'))

#averaged_class_data = pickle.load(open('/home/dgiudici/HSI/HSI-Code/ModelVars/averagedClassData3Season.pickle', 'rb'))

#%% perfrom the convolution of each kernel with each one piece of data from each class

zipped_label_data = zip(valid.labels_num,valid.data)
zipped_label_data = sorted(zipped_label_data,key=lambda class_data:class_data[0])

sorted_valid_labels_num = [zipped_label_data[x][0] for x in range(len(zipped_label_data))]
sorted_valid_data = [zipped_label_data[x][1] for x in range (len(zipped_label_data))]
#sorted_valid_one_hot_labels = HcN.reformatOneHot(np.asarray(sorted_valid_labels_num),self.num_labels)
sorted_valid_data = np.asarray(sorted_valid_data)
bincounts = np.bincount(sorted_valid_labels_num)

zipped_label_data2 = zip(valid_unstd.labels_num,valid_unstd.data)
zipped_label_data2 = sorted(zipped_label_data2,key=lambda class_data:class_data[0])

sorted_valid_labels_num2 = [zipped_label_data2[x][0] for x in range(len(zipped_label_data2))]
sorted_valid_data2 = [zipped_label_data2[x][1] for x in range (len(zipped_label_data2))]
#sorted_valid_one_hot_labels = HcN.reformatOneHot(np.asarray(sorted_valid_labels_num),self.num_labels)
sorted_valid_data2 = np.asarray(sorted_valid_data2)



#%%

class_strt_idx =np.asarray( [np.sum(bincounts[:i]) for i in range(len(bincounts))]) # the start index for each class

k = kernels[0,0,0,0]
#acd = averaged_class_data[0]
conv = []
'''
for i in range(class_strt_idx.shape[0]):
    conv.append([])
    for j in range(kernels.shape[3]):
        expanded =  np.asarray(list(sorted_valid_data[class_strt_idx[i]][0:96]) + [0]*20\
                    + list(sorted_valid_data[class_strt_idx[i]][96:96+38]) + [0]*18 \
                    + list(sorted_valid_data[class_strt_idx[i]][96+38:186]) \
                    +   list(sorted_valid_data[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                    +   list(sorted_valid_data[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                    +   list(sorted_valid_data[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                    +      list(sorted_valid_data[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                    +      list(sorted_valid_data[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                    +      list(sorted_valid_data[class_strt_idx[i]][96+38+186*2:186+186*2]))
        #conv[i].append(np.convolve(sorted_valid_data[class_strt_idx[i]],kernels[:,0,0,j],mode = 'same'))
        conv[i].append(np.convolve(expanded,kernels[:,0,0,j],mode = 'same'))
    #conv[i = class data peices][j = kernel]
'''

conv_big = []
conv = []

ave_conv = []
ave_conv_ext = []
NUM_AVE = 83
for r in range (NUM_AVE):    #for x number of items in an average            ||| 25
    for i in range(class_strt_idx.shape[0]):    #for each class start index ||| 12 Classes
        conv.append([])
        conv_big.append([])
        for j in range(kernels.shape[3]):       #for each kernel            ||| 7 kernels

            conv[i].append(list(np.convolve(sorted_valid_data[class_strt_idx[i]],kernels[:,0,0,j],mode = 'same')))
            ave_conv.append(list(np.convolve(sorted_valid_data[class_strt_idx[i]+r],kernels[:,0,0,j],mode = 'same')))

            #        expanded =  np.asarray(list(sorted_valid_data[class_strt_idx[i]][0:96]) + [0]*20\
            #                    + list(sorted_valid_data[class_strt_idx[i]][96:96+38]) + [0]*18 \
            #                    + list(sorted_valid_data[class_strt_idx[i]][96+38:186]) \
            #                    +   list(sorted_valid_data[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
            #                    +   list(sorted_valid_data[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
            #                    +   list(sorted_valid_data[class_strt_idx[i]][96+38+186*1:186+186*1]) \
            #                    +      list(sorted_valid_data[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
            #                    +      list(sorted_valid_data[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
            #                    +      list(sorted_valid_data[class_strt_idx[i]][96+38+186*2:186+186*2]))
            conv_big[i].append(np.asarray(list(conv[i][j][0:96]) + [0]*20 \
                        + list(conv[i][j][96:96+38]) + [0]*18 \
                        + list(conv[i][j][96+38:186]) \
                        +   list(conv[i][j][0+186*1:96+186*1]) + [0]*20\
                        +   list(conv[i][j][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(conv[i][j][96+38+186*1:186+186*1]) \
                        +      list(conv[i][j][0+186*2:96+186*2]) + [0]*20 \
                        +      list(conv[i][j][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(conv[i][j][96+38+186*2:186+186*2])))
            ave_conv_ext.append(np.asarray(list(ave_conv[-1][0:96]) + [0]*20 \
                        + list(ave_conv[-1][96:96+38]) + [0]*18 \
                        + list(ave_conv[-1][96+38:186]) \
                        +   list(ave_conv[-1][0+186*1:96+186*1]) + [0]*20\
                        +   list(ave_conv[-1][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(ave_conv[-1][96+38+186*1:186+186*1]) \
                        +      list(ave_conv[-1][0+186*2:96+186*2]) + [0]*20 \
                        +      list(ave_conv[-1][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(ave_conv[-1][96+38+186*2:186+186*2])))
        # class_strt_idx +=1 This modifieds class_strt_idx... why is it needed...

conv = np.asarray(conv_big)
ave_conv_ext_arry = np.asarray(ave_conv_ext)
ave_conv_arry = np.asarray(ave_conv)

rs_ave_conv_ext_arry = np.reshape(ave_conv_ext_arry,(NUM_AVE,12,7,672))
rs_ave_conv_arry = np.reshape(ave_conv_arry,(NUM_AVE,12,7,558))


#%% STANDARDIAZED, SCALED, MAGINTUDE MAP EXCITATIONS
# Goal subtract the mean feature map away from the individual feature maps
# this should provide an indication of the uniquness of each of the plots
# Capture average feature map for each class

ave_feature_map = np.average(rs_ave_conv_ext_arry,axis=0)
ave_class_feature_map = np.average(ave_feature_map,axis=0)*0

mean_subed_conv = []
for j in range(ave_feature_map.shape[1]): #each class 12
    mean_subed_conv.append([ave_feature_map[:,j,i]-ave_class_feature_map[j][i] for i in range (ave_feature_map.shape[2])])

conv_exp = np.asarray(mean_subed_conv)
i = 0
fig = plt.figure(facecolor='white')
#fig.suptitle('Standardized, Scalled, Magnitude Map Excitation of Feature Maps for Each Class')
#conv_arry = np.asarray(conv)
#%%

num_row = 4
num_col = 3
for i in range(conv_exp.shape[2]):#for each class

    ax = fig.add_subplot(num_row,num_col,i+1,projection='3d') #fig.gca(projection = '3d')
    conv_arry = np.asarray(conv_exp)

    ##Plot the wire frame
    X = np.arange(0,conv_arry.shape[1],1)
    X = np.arange(0,672,1)
    Y = np.ones(1)*(kernels.shape[3])
    X,Y = np.meshgrid(X,Y)

    #### PLOT A GENERIC PROFILE ### ->>> ###plt.plot(range(sorted_valid_data2[class_strt_idx[4]].shape[0]),sorted_valid_data2[class_strt_idx[0]])
    #Expanded is to increase the blank areas within the data (remove the bad data sections)
    expanded = np.asarray(list(sorted_valid_data2[class_strt_idx[i]][0:96]) + [0]*20\
                        + list(sorted_valid_data2[class_strt_idx[i]][96:96+38]) + [0]*18 \
                        + list(sorted_valid_data2[class_strt_idx[i]][96+38:186]) \

                        +   list(sorted_valid_data2[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                        +   list(sorted_valid_data2[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(sorted_valid_data2[class_strt_idx[i]][96+38+186*1:186+186*1]) \

                        +      list(sorted_valid_data2[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                        +      list(sorted_valid_data2[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(sorted_valid_data2[class_strt_idx[i]][96+38+186*2:186+186*2]))

    Z = expanded
    #Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
    cset = ax.plot_wireframe(X, Y, np.transpose(Z), cmap=cm.coolwarm)
    #cset = ax.plot_wireframe(X, Y, Z, cmap=cm.coolwarm)
    ax.clabel(cset, fontsize=9, inline=1)
    ##Plot the Surface
    X = np.arange(0,conv_arry.shape[1],1)
    Y = np.arange(1,kernels.shape[3]+1,1)

    X,Y = np.meshgrid(X,Y)

    #Z = np.transpose(Z)
    vmax = np.max(Z)
    vmin = np.min(Z)
    cmax = np.max(conv_arry)*.55
    Z = np.abs( conv_arry[:,:,i]/cmax)*vmax
    surf = ax.plot_surface(X,Y,Z, rstride=1,cstride =1, cmap=cm.coolwarm,linewidth=0,antialiased = False,vmin = vmin,vmax = vmax)


    #Z = sorted_valid_data[class_strt_idx[0]]
    '''
    expanded = np.asarray(list(sorted_valid_data[class_strt_idx[i]][0:96]) + [0]*20\
                        + list(sorted_valid_data[class_strt_idx[i]][96:96+38]) + [0]*18 \
                        + list(sorted_valid_data[class_strt_idx[i]][96+38:186]) \
                        +   list(sorted_valid_data[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                        +   list(sorted_valid_data[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(sorted_valid_data[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                        +      list(sorted_valid_data[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                        +      list(sorted_valid_data[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(sorted_valid_data[class_strt_idx[i]][96+38+186*2:186+186*2]))
    #Z = sorted_valid_data[class_strt_idx[0]]

    expanded = np.asarray(list(sorted_valid_data2[class_strt_idx[i]][0:96]) + [0]*20\
                        + list(sorted_valid_data2[class_strt_idx[i]][96:96+38]) + [0]*18 \
                        + list(sorted_valid_data2[class_strt_idx[i]][96+38:186]) \
                        +   list(sorted_valid_data2[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                        +   list(sorted_valid_data2[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(sorted_valid_data2[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                        +      list(sorted_valid_data2[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                        +      list(sorted_valid_data2[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(sorted_valid_data2[class_strt_idx[i]][96+38+186*2:186+186*2]))

    Z = expanded
    #    Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
    cset = ax.plot_wireframe(X, Y, np.transpose(Z), cmap=cm.coolwarm)
    ax.clabel(cset, fontsize=9, inline=1)
    '''

    #PLOT THE BACKGROUNDS FOR THE SEASONS
    x = [0,0,224,224]
    y = [7,7,7,7]
    z = [0,6000,6000,0]
    verts = [zip(x,y,z)]
    season1 = Poly3DCollection(verts, alpha = .2)
    season1.set_color('green')
    ax.add_collection3d(season1)

    x = [224,224,224*2,224*2]
    y = [7,7,7,7]
    z = [0,6000,6000,0]
    verts = [zip(x,y,z)]
    season1 = Poly3DCollection(verts, alpha = .2)
    season1.set_color('yellow')
    ax.add_collection3d(season1)

    x = [224*2,224*2,224*3,224*3]
    y = [7,7,7,7]
    z = [0,6000,6000,0]
    verts = [zip(x,y,z)]
    season1 = Poly3DCollection(verts, alpha = .2)
    season1.set_color('grey')
    ax.add_collection3d(season1)
    #ax.view_init(elev=30, azim=-80) #to view data/seasonal variation
    ax.view_init(elev=30, azim=-100) #to view data/seasonal variation
    #ax.view_init(elev=10, azim=-10) #to view kernel variation

    #ax.set_zlim3d(-5,5)#vmin,vmax) #-20000,20000)

    #ax.zaxis.set_major_locator(LinearLocator(10))
    #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    #ax.zaxis.set_scientific(True)
    #formatter = ticker.ScalarFormatter()
    #formatter.set_scientific(True)
    #formatter= ticker.ScalarFormatter(useMathText='sci',useOffset = False)

    #ax.ticklabel_format(axis="z", style="sci", scilimits=(0,0))

    #ax.zaxis.set_major_formatter(formatter)
    ax.set_xlabel('Band #')
    ax.set_xlim(0,672)
    ax.set_ylabel('Kernel #')
    ax.set_zlabel('Data Convolution')
    ax.set_zlim(0, 6000)
    ax.xaxis._axinfo['label']['space_factor'] = 2.75
    ax.yaxis._axinfo['label']['space_factor'] = 1.5 #kernels
    ax.zaxis._axinfo['label']['space_factor'] = 3.25#excitations
    ax.yaxis._axinfo['ticklabel']['space_factor'] = .5  #kernels
    ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.75 #excitations
    ax.yaxis.set_ticks(np.arange(1,7+1,2))
    #ax.yaxis.set_tick_params(labelsize = 'small')

    #Draw Rectangles Behind the Wireframe
    #Rectangles x = spectra, y = kernel, z = data convolution accesss
    # color 1 x = [0,186]
    # color 1 y = [0,0]
    # color 1 z = [0,6000]


    #plt.title('Class ' + str(i) +' Data')
    #plt.title(train.set_labels[i])
    plt.title(modified_labels[i])
    #fig.colorbar(surf, shrink=0.5, aspect=5)
'''
Colorbar Settings
fig.subplots_adjust(right =0.8)
cbar_ax = fig.add_axes([.85,.15,.05,.7])
cbar = fig.colorbar(surf, cax = cbar_ax)
cbar.ax.set_ylabel('Excitation',labelpad = 20, rotation = 270)
'''

plt.draw()
plt.show()


#%%
fig = plt.figure(facecolor='white')
i = 0
#fig.suptitle('Standardized, Scaled, Magnitude Map Excitation of Feature Map')
conv_arry = np.asarray(conv)
ax = fig.add_subplot(1,1,1,projection='3d') #fig.gca(projection = '3d')
conv_arry = np.asarray(conv_exp)

#PLOT WIRE FRAME
X = np.arange(0,conv_arry.shape[1],1)
Y = np.ones(1)*(kernels.shape[3])
X,Y = np.meshgrid(X,Y)
Z = sorted_valid_data[class_strt_idx[0]]

expanded = np.asarray(list(sorted_valid_data[class_strt_idx[i]][0:96]) + [0]*20\
                    + list(sorted_valid_data[class_strt_idx[i]][96:96+38]) + [0]*18 \
                    + list(sorted_valid_data[class_strt_idx[i]][96+38:186]) \
                    +   list(sorted_valid_data[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                    +   list(sorted_valid_data[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                    +   list(sorted_valid_data[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                    +      list(sorted_valid_data[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                    +      list(sorted_valid_data[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                    +      list(sorted_valid_data[class_strt_idx[i]][96+38+186*2:186+186*2]))
#Z = sorted_valid_data[class_strt_idx[0]]

expanded = np.asarray(list(sorted_valid_data2[class_strt_idx[i]][0:96]) + [0]*20\
                    + list(sorted_valid_data2[class_strt_idx[i]][96:96+38]) + [0]*18 \
                    + list(sorted_valid_data2[class_strt_idx[i]][96+38:186]) \
                    +   list(sorted_valid_data2[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                    +   list(sorted_valid_data2[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                    +   list(sorted_valid_data2[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                    +      list(sorted_valid_data2[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                    +      list(sorted_valid_data2[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                    +      list(sorted_valid_data2[class_strt_idx[i]][96+38+186*2:186+186*2]))

Z = expanded
#    Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
cset = ax.plot_wireframe(X, Y, np.transpose(Z), cmap=cm.coolwarm)

#PLOT SURFACE

X = np.arange(0,conv_arry.shape[1],1)
Y = np.arange(1,kernels.shape[3]+1,1)

X,Y = np.meshgrid(X,Y)


#Z = np.transpose(Z)
vmax = np.max(Z)
vmin = np.min(Z)
cmax = np.max(conv_arry)
Z = np.abs( conv_arry[:,:,i]/cmax)*vmax
surf = ax.plot_surface(X,Y,Z, rstride=1,cstride =1, cmap=cm.coolwarm,linewidth=0,antialiased = False,vmin = 0,vmax = np.max(Z))


'''
rect1 = matplotlib.patches.Rectangle((num_bands*0,-5000), num_bands, 11000, alpha = .2, color='green')
rect2 = matplotlib.patches.Rectangle((num_bands*1,-5000), num_bands, 11000, alpha = .2, color='yellow')
rect3 = matplotlib.patches.Rectangle((num_bands*2,-5000), num_bands, 11000, alpha = .2, color='grey')
fig = plt.figure()
'''
x = [0,0,224,224]
y = [7,7,7,7]
z = [0,vmax,vmax,0]
verts = [zip(x,y,z)]
season1 = Poly3DCollection(verts, alpha = .2)
season1.set_color('green')
ax.add_collection3d(season1)

x = [224,224,224*2,224*2]
y = [7,7,7,7]
z = [0,vmax,vmax,0]
verts = [zip(x,y,z)]
season1 = Poly3DCollection(verts, alpha = .2)
season1.set_color('yellow')
ax.add_collection3d(season1)

x = [224*2,224*2,224*3,224*3]
y = [7,7,7,7]
z = [0,vmax,vmax,0]
verts = [zip(x,y,z)]
season1 = Poly3DCollection(verts, alpha = .2)
season1.set_color('grey')
ax.add_collection3d(season1)


ax.clabel(cset, fontsize=9, inline=1)


#ax.view_init(elev=30, azim=-80) #to view data/seasonal variation

#Good for all...
#ax.view_init(elev=10, azim=-90) #to view data/seasonal variation
#ax.view_init(elev=10, azim=-10) #to view kernel variation

ax.view_init(elev=20, azim=-100) #to view data/seasonal variation
#ax.set_zlim3d(-5,5)#vmin,vmax) #-20000,20000)

#ax.zaxis.set_major_locator(LinearLocator(10))
#ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
#ax.zaxis.set_scientific(True)
#formatter = ticker.ScalarFormatter()
#formatter.set_scientific(True)
#formatter= ticker.ScalarFormatter(useMathText='sci',useOffset = False)

#ax.ticklabel_format(axis="z", style="sci", scilimits=(0,0))

#ax.zaxis.set_major_formatter(formatter)
ax.set_xlabel('Band #')
ax.set_xlim(0,672)
ax.set_ylabel('Kernel #')
ax.set_zlabel('Data Convolution')
ax.set_zlim(0, vmax)
ax.xaxis._axinfo['label']['space_factor'] = 2.75
ax.yaxis._axinfo['label']['space_factor'] = 1.5 #kernels
ax.zaxis._axinfo['label']['space_factor'] = 3.25#excitations
ax.yaxis._axinfo['ticklabel']['space_factor'] = .5  #kernels
ax.zaxis._axinfo['ticklabel']['space_factor'] = 1.75 #excitations
ax.yaxis.set_ticks(np.arange(1,7+1,2))
#ax.yaxis.set_tick_params(labelsize = 'small')
#plt.title('Zero u: Class ' + str(i) +' Data')
plt.title(train.set_labels[i])
#%% END OF EXPERIMENT



False














#%%
"""
plt.figure()
plt.title('Convolution with Data-Single')
plt.plot(range(len(conv[0][0])),conv[0][0])
#%%
from mpl_toolkits.mplot3d import axes3d
from matplotlib import cm
from matplotlib.ticker import LinearLocator, FormatStrFormatter
from matplotlib import ticker
#import matplotlib.pyplot as plt
import numpy as np

#for the first class i = 0
i = 0
fig = plt.figure()
fig.suptitle('3-D Excitation of Feature Maps for Class '+str(i)+' Data (scaled by 1400)')
ax = fig.gca(projection = '3d')
conv_arry = np.asarray(conv)
conv_arry[:,:,96:117] = 0
conv_arry[:,:,152:171] = 0
conv_arry[:,:,96+224:117+224] = 0
conv_arry[:,:,152+224:171+224] = 0
conv_arry[:,:,96+224*2:117+224*2] = 0
conv_arry[:,:,152+224*2:171+224*2] = 0


X = np.arange(0,conv_arry.shape[2],1)
Y = np.arange(0,kernels.shape[3],1)
X,Y = np.meshgrid(X,Y)
Z = conv_arry[i,:,:]
surf = ax.plot_surface(X,Y,Z, rstride=1,cstride =1, cmap=cm.coolwarm,linewidth=0,antialiased = False)
#%%
X = np.arange(0,conv_arry.shape[2],1)
Y = np.ones(1)*(kernels.shape[3]-1)
X,Y = np.meshgrid(X,Y)

expanded = np.asarray(list(sorted_valid_data[class_strt_idx[0]][0:96]) + [0]*20\
                    + list(sorted_valid_data[class_strt_idx[0]][96:96+38]) + [0]*18 \
                    + list(sorted_valid_data[class_strt_idx[0]][96+38:186]) \
                    +   list(sorted_valid_data[class_strt_idx[0]][0+186*1:96+186*1]) + [0]*20\
                    +   list(sorted_valid_data[class_strt_idx[0]][96+186*1:96+38+186*1]) + [0]*18 \
                    +   list(sorted_valid_data[class_strt_idx[0]][96+38+186*1:186+186*1]) \
                    +      list(sorted_valid_data[class_strt_idx[0]][0+186*2:96+186*2]) + [0]*20\
                    +      list(sorted_valid_data[class_strt_idx[0]][96+186*2:96+38+186*2]) + [0]*18 \
                    +      list(sorted_valid_data[class_strt_idx[0]][96+38+186*2:186+186*2]))

#Z = sorted_valid_data[class_strt_idx[0]]
Z = expanded
#Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
Z = np.reshape(Z,(1,expanded.shape[0]))
cset = ax.plot_wireframe(X, Y, Z, cmap=cm.coolwarm)
ax.clabel(cset, fontsize=9, inline=1)
#ax.set_zlim([-5,5])
ax.set_xlabel('Spectra')
ax.set_ylabel('Kernel #')
ax.set_zlabel('Data Convolution')
ax.xaxis._axinfo['label']['space_factor'] = 2.
ax.yaxis._axinfo['label']['space_factor'] = 2.
ax.zaxis._axinfo['label']['space_factor'] = 2.25


x_label_idx = [0,11,32,52,95,150,171,199]
x_labels = [365,472,655,850,1263,1452,1791,1967,2257]
x_labels_full = x_labels*3
x_label_idx_arry = np.asarray(x_label_idx)
x_label_idx_full = list(x_label_idx_arry) + list(x_label_idx_arry+224) + list(x_label_idx_arry+224*2)
plt.xticks(x_label_idx_full,x_labels_full, rotation = 'vertical')

#ax.view_init(elev=30, azim=-80) #to view data/seasonal variation
#ax.view_init(elev=10, azim=-10) #to view kernel variation
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

fig.colorbar(surf, shrink=0.5, aspect=5)
plt.title('All Kernels Convolved with Class: ' + str(i) +' Data')
plt.draw()
plt.show()

#%%
Z2 =  list(sorted_valid_data[class_strt_idx[0]][0:96]) + [0]*20\
    + list(sorted_valid_data[class_strt_idx[0]][96:96+38]) + [0]*18 \
    + list(sorted_valid_data[class_strt_idx[0]][96+38:186])

#%%PLOT THE COUNTOUR
'''fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

X = np.arange(0,sorted_valid_data.shape[1],1)
Y = np.zeros(1)
X,Y = np.meshgrid(X,Y)
Z = sorted_valid_data[class_strt_idx[0]]
Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
cset = ax.plot_wireframe(X, Y, Z, cmap=cm.coolwarm)
ax.clabel(cset, fontsize=9, inline=1)
plt.draw()
plt.show()
'''
#%%

#%% PLOT 3-D Views of Kernel Excitations - Data Variation View
i = 0
fig = plt.figure()
fig.suptitle('Excitation of Feature Maps for Each Classes Data\n X axis = Num Class, Y axis = Spectral Location\n View Tailored for Spectral Excitation',fontsize = 16)
fig.subplots_adjust(top=0.88)
conv_arry = np.asarray(conv)

num_row = 4
num_col = 3
for i in range(conv_arry.shape[0]):#for each class


    ax = fig.add_subplot(num_row,num_col,i+1,projection='3d') #fig.gca(projection = '3d')
    conv_arry = np.asarray(conv)
    X = np.arange(0,conv_arry.shape[2],1)
    Y = np.arange(0,kernels.shape[3],1)
    X,Y = np.meshgrid(X,Y)
    Z =  conv_arry[i,:,:]

    vmax = np.max(Z)
    vmin = np.min(Z)
    surf = ax.plot_surface(X,Y,Z, rstride=1,cstride =1, cmap=cm.coolwarm,linewidth=0,antialiased = False,vmin = vmin,vmax = vmax)

    X = np.arange(0,conv_arry.shape[2],1)
    Y = np.ones(1)*(kernels.shape[3])
    X,Y = np.meshgrid(X,Y)
    Z = sorted_valid_data[class_strt_idx[0]]

    expanded = np.asarray(list(sorted_valid_data[class_strt_idx[i]][0:96]) + [0]*20\
                        + list(sorted_valid_data[class_strt_idx[i]][96:96+38]) + [0]*18 \
                        + list(sorted_valid_data[class_strt_idx[i]][96+38:186]) \
                        +   list(sorted_valid_data[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                        +   list(sorted_valid_data[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(sorted_valid_data[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                        +      list(sorted_valid_data[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                        +      list(sorted_valid_data[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(sorted_valid_data[class_strt_idx[i]][96+38+186*2:186+186*2]))

    #Z = sorted_valid_data[class_strt_idx[0]]
    Z = expanded
    #    Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
    cset = ax.plot_wireframe(X, Y, Z, cmap=cm.coolwarm)
    ax.clabel(cset, fontsize=9, inline=1)

    ax.view_init(elev=10, azim=-90) #to view data/seasonal variation
    #ax.view_init(elev=10, azim=-10) #to view kernel variation
    #ax.set_zlim([-1000,1000])
    ax.set_xlabel('Spectra')
    ax.set_ylabel('Kernel #')
    ax.set_zlabel('Data Convolution')
    ax.xaxis._axinfo['label']['space_factor'] = 2.
    ax.yaxis._axinfo['label']['space_factor'] = 2.
    ax.zaxis._axinfo['label']['space_factor'] = 2.25
    #ax.set_zlim3d(-5,5)#vmin,vmax) #-20000,20000)

    #ax.zaxis.set_major_locator(LinearLocator(10))
    #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    #ax.zaxis.set_scientific(True)
    #formatter = ticker.ScalarFormatter()
    #formatter.set_scientific(True)
    #formatter= ticker.ScalarFormatter(useMathText='sci',useOffset = False)

    #ax.ticklabel_format(axis="z", style="sci", scilimits=(0,0))

    #ax.zaxis.set_major_formatter(formatter)

    plt.title('Class ' + str(i) +' Data')
    plt.tight_layout(pad=1, w_pad=0.5, h_pad=0)
    #fig.colorbar(surf, shrink=0.5, aspect=5)


fig.subplots_adjust(right =0.8,top=0.88)

cbar_ax = fig.add_axes([.85,.15,.05,.7])
fig.colorbar(surf, cax = cbar_ax)

plt.draw()
plt.show()

#%% PLOT 3-D Views of Kernel Excitations - Kernel Variation View
i = 0
fig = plt.figure()
fig.suptitle('Excitation of Feature Maps for Each Classes Data\n X axis = Num Class, Y axis = Spectral Location\n View Tailored for Kernel Excitation')
conv_arry = np.asarray(conv)

num_row = 4
num_col = 3
for i in range(conv_arry.shape[0]):#for each class

    ax = fig.add_subplot(num_row,num_col,i+1,projection='3d') #fig.gca(projection = '3d')
    conv_arry = np.asarray(conv)
    X = np.arange(0,conv_arry.shape[2],1)
    Y = np.arange(0,kernels.shape[3],1)
    X,Y = np.meshgrid(X,Y)
    Z = conv_arry[i,:,:]*300

    vmax = np.max(Z)
    vmin = np.min(Z)
    surf = ax.plot_surface(X,Y,Z, rstride=1,cstride =1, cmap=cm.coolwarm,linewidth=0,antialiased = False,vmin = vmin,vmax = vmax)

    X = np.arange(0,conv_arry.shape[2],1)
    Y = np.ones(1)*(kernels.shape[3])
    X,Y = np.meshgrid(X,Y)
    Z = sorted_valid_data[class_strt_idx[0]]

    expanded = np.asarray(list(sorted_valid_data[class_strt_idx[i]][0:96]) + [0]*20\
                        + list(sorted_valid_data[class_strt_idx[i]][96:96+38]) + [0]*18 \
                        + list(sorted_valid_data[class_strt_idx[i]][96+38:186]) \
                        +   list(sorted_valid_data[class_strt_idx[i]][0+186*1:96+186*1]) + [0]*20\
                        +   list(sorted_valid_data[class_strt_idx[i]][96+186*1:96+38+186*1]) + [0]*18 \
                        +   list(sorted_valid_data[class_strt_idx[i]][96+38+186*1:186+186*1]) \
                        +      list(sorted_valid_data[class_strt_idx[i]][0+186*2:96+186*2]) + [0]*20\
                        +      list(sorted_valid_data[class_strt_idx[i]][96+186*2:96+38+186*2]) + [0]*18 \
                        +      list(sorted_valid_data[class_strt_idx[i]][96+38+186*2:186+186*2]))

    #Z = sorted_valid_data[class_strt_idx[0]]
    Z = expanded
    #    Z = np.reshape(Z,(1,sorted_valid_data.shape[1]))
    cset = ax.plot_wireframe(X, Y, Z, cmap=cm.coolwarm)
    ax.clabel(cset, fontsize=9, inline=1)

    #ax.view_init(elev=30, azim=-80) #to view data/seasonal variation
    ax.view_init(elev=10, azim=-10) #to view kernel variation

    #ax.set_zlim3d(-5,5)#vmin,vmax) #-20000,20000)

    #ax.zaxis.set_major_locator(LinearLocator(10))
    #ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
    #ax.zaxis.set_scientific(True)
    #formatter = ticker.ScalarFormatter()
    #formatter.set_scientific(True)
    #formatter= ticker.ScalarFormatter(useMathText='sci',useOffset = False)

    #ax.ticklabel_format(axis="z", style="sci", scilimits=(0,0))

    #ax.zaxis.set_major_formatter(formatter)
    ax.set_xlabel('Spectra')
    ax.set_ylabel('Kernel #')
    ax.set_zlabel('Data Convolution')
    ax.xaxis._axinfo['label']['space_factor'] = 2.
    ax.yaxis._axinfo['label']['space_factor'] = 2.
    ax.zaxis._axinfo['label']['space_factor'] = 2.25
    plt.title('Class ' + str(i) +' Data')

    #fig.colorbar(surf, shrink=0.5, aspect=5)

fig.subplots_adjust(right =0.8)
cbar_ax = fig.add_axes([.85,.15,.05,.7])
fig.colorbar(surf, cax = cbar_ax)

plt.draw()
plt.show()





#%% TO SEE WHAT EACH KERNEL EXCITES CONVOLVE EACH WITH A CHIRP FUNCTION THE
#RESULTING OUTPUT SHOULD HAVE THE KERNEL BE EXCITED BY THE MOST RELEVANT FEATURE
t = np.linspace(0,sorted_valid_data.shape[1],(sorted_valid_data.shape[1]+1))
chirp_fx = sp.chirp(t,f0 = 1.25, t1 = sorted_valid_data.shape[1], f1 = .0001)


fig = plt.figure()
fig.suptitle("Chirp Function Convolved with Each Learned Kernel")
plt.subplot(3,3,1)
conv = []

scaled_sorted_valid_data = sorted_valid_data[:]/np.max(sorted_valid_data)*10 +5

for j in range(kernels.shape[3]):
    conv.append(np.convolve(chirp_fx,kernels[:,0,0,j],mode = 'same'))

for j in range(kernels.shape[3]):

    plt.subplot(3,3, j +1)
    plt.ylim([-15,15])
    if (j % 5 == 0):
        plt.ylabel("Excitation of Kernel")
    if (j > 15-1):
        plt.xlabel("Spectral Domain")
    c_fx, = plt.plot(range(len(chirp_fx)),chirp_fx,'b')
    c, = plt.plot(range(len(conv[0])),conv[j],'r')
    s_acd, = plt.plot(range(scaled_sorted_valid_data.shape[1]),scaled_sorted_valid_data[0],'g')

fig.legend((c,c_fx,s_acd),("Convolved Kernel","Chirp Function","Scaled Averaged Datapoint"),'upper right')

#%%
fig = plt.figure()
fig.suptitle("Chirp Function Convolved with Class 0 Data")
c, = plt.plot(range(len(conv[0])),conv[0],'r')
c_fx, = plt.plot(range(len(chirp_fx)),chirp_fx,'b')
s_acd, = plt.plot(range(scaled_sorted_valid_data.shape[1]),scaled_sorted_valid_data[0],'g')
plt.ylabel("Excitation of Kernel")
plt.xlabel("Spectral Domain")
fig.legend((c,c_fx,s_acd),("Convolved Kernel","Chirp Function","Scaled Averaged Datapoint"),'upper right')


#%% PLOTTING THE IMPULSE RESPONSE
#SIMILAR TO THE PREVIOUS WORK THIS EFFECTIVELY TREATS THE KERNEL AS
# A DIGITAL FILTER

from scipy import signal

plt.figure()
plt.suptitle('Impulse Response for Each Kernel')
for i in range(kernels.shape[3]):
    plt.subplot(3,3,i+1)
    plt.title('Kernel ' + str(i))
    flt = np.reshape(kernels[:,:,:,i],kernels.shape[0])
    plt.plot(flt)
#%%
plt.figure()
plt.suptitle('Step Response for Each Kernel')
for i in range(kernels.shape[3]):
    plt.subplot(3,3,i+1)
    plt.title('Kernel ' + str(i))
    flt = np.cumsum(np.reshape(kernels[:,:,:,i],kernels.shape[0]))
    plt.plot(flt)


#%%
plt.figure()
i = 1
plt.title('Kernel ' + str(i) + 'Impulse Response')
flt = np.reshape(kernels[:,:,:,i],kernels.shape[0])
plt.stem(flt)
#%% Step_resp

def impz(b,a=1):
    l = len(b)
    impulse = np.repeat(0.,l); impulse[0] =1.
    x = np.arange(0,l)
    response = signal.lfilter(b,a,impulse)
    plt.subplot(211)
    plt.plot(x, response)
    plt.ylabel('Amplitude')
    plt.xlabel(r'n (samples)')
    plt.title(r'Impulse response')
    plt.subplot(212)
    step = np.cumsum(response)
    plt.stem(x, step)
    plt.ylabel('Amplitude')
    plt.xlabel(r'n (samples)')
    plt.title(r'Step response')
    plt.subplots_adjust(hspace=0.5)
    plt.show()

#for i in range(kernels.shape[3]):
#    plt.figure()
#    impz(np.reshape(kernels[:,:,:,i],kernels.shape[0]))
#    plt.show()
#%%

#%% PLOTTING THE FREQUENCY RESPONSE OF A KERNEL
fig = plt.figure()
#fig,axes = plt.subplots(nrows=4,ncols=5)
#fig.tight_layout()
fig.suptitle('Frequency Response of Each Kernel \n' +
                 '(Treat Each Kernel As A Digital Filter)')
from matplotlib.ticker import MultipleLocator
for i in range(kernels.shape[3]):
    b = kernels[:,0,0,i]    #grab one kernel
    fs = sorted_valid_data.shape[1]/float(2500-400) # 188samples/2100nm => sample/nm
    #N = 188
    #fs = 1
    w, h = signal.freqz(b,a = [1], worN=188, whole=False)
    #fig = plt.figure()
    ax1 = plt.subplot(3,3,i+1)
    plt.subplots_adjust(wspace = .5, hspace = .5)
    spacing = .005
    minor_locator = MultipleLocator(spacing)
    ax1.xaxis.set_minor_locator(minor_locator)
    #plt.axis('tight')
    if i > 14:
        #plt.xlabel('Frequency [rad/sample]') # when plotting W
        plt.xlabel('Frequency [cycles/nm]') #when plottting against w(rad/sample)*(sample/nm)*1/(2*pi)(cycles/radian)


    plt.xticks(rotation='vertical')
    if (i%5 == 0): #for left hand side
        plt.ylabel('Amplitude [dB]',color = 'b')

    #plt.ylim([-20,20])
    #plt.plot(w, 20*np.log10(abs(h)),'b')
    plt.plot(w*fs/float(2*np.pi), 20*np.log10(abs(h)),'b')

    ax2 = ax1.twinx()#switch to the right hand side
    plt.ylim([-55,5])


    if ((i+1)%5 == 0): #for right hand side
        plt.ylabel('Angle (radians)',color='g')
    #if (i>14):


    angles = np.unwrap(np.angle(h))
    #plt.plot(w,angles,'g')#Normalized Digital Frequency radians / sample
    plt.plot(w*fs/float(2*np.pi),angles,'g') # frequency cycles / nm
    ax1.xaxis.grid(True)
    plt.grid()

    plt.show()
#%%
fig = plt.figure()
#fig,axes = plt.subplots(nrows=4,ncols=5)
#fig.tight_layout()
fig.suptitle('Frequency Response of Kernel 19')
#ax1.subplots_adjust(wspace = .5, hspace = .5)
ax1 = plt.subplot(1,1,1)
spacing = .005
minor_locator = MultipleLocator(spacing)
ax1.xaxis.set_minor_locator(minor_locator)
ax1.set_xlabel('Frequency [cycles/nm]')
ax1.set_ylabel('Amplitude [dB]',color = 'b')

#ax1.set_ylim([-20,20])

#plt.plot(w, 20*np.log10(abs(h)),'b')
ax1.plot(w*fs/float(2*np.pi), 20*np.log10(abs(h)),'b')
ax1.xaxis.grid(True)

#%%
ax2 = ax1.twinx()#switch to the right hand side
ax2.set_ylim([-60,15])
ax2.set_ylabel('Angle (radians)',color='g')
angles = np.unwrap(np.angle(h))
#plt.plot(w,angles,'g')#Normalized Digital Frequency radians / sample
ax2.plot(w*fs/float(2*np.pi),angles,'g') # frequency cycles / nm

#plt.grid()
plt.grid()

plt.show()
#%%
# Pretty Print table in tabular format
def PrettyPrint(table, justify = "R", columnWidth = 0):
    # To get exportable tables .....>>>> print(PrettyPrint(confus_data_arry))
    # Not enforced but
    # if provided columnWidth must be greater than max column width in table!
    if columnWidth == 0:
        # find max column width
        for row in table:
            for col in row:
                width = len(str(col))
                if width > columnWidth:
                    columnWidth = width

    outputStr = ""
    for row in table:
        rowList = []
        for col in row:
            if justify == "R": # justify right
                rowList.append(str(col).rjust(columnWidth))
            elif justify == "L": # justify left
                rowList.append(str(col).ljust(columnWidth))
            elif justify == "C": # justify center
                rowList.append(str(col).center(columnWidth))
        outputStr += ' '.join(rowList) + "\n"
    return outputStr

#%%Plot the impl4se response of each kernel/filter
#The impulse response of a digitl filter is it plotted against the
#kroncker delta function d(n-k) ... just plot the kernel
'''
fig = plt.figure()
fig.suptitle("Impluse Response Of Digital Filter")
plt.subplot(3,3,1)

for i in range(kernels.shape[3]):
    plt.subplot(3,3,i)
    plt.plot(kernels[:,0,0,i])
'''
"""
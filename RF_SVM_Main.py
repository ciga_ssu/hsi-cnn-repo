# -*- coding: utf-8 -*-
"""

Rev 0.1 - 13-Sept15
This is a snap shot to be submitted with the thesis.

Created on Tue May 17 21:19:11 2016

@author: dgiudici

This script will :
1.Import Data
    1.1. Take in Single Season HSI data
    1.2. Remove all but the 12 classes define by LCCS
    1.3. Pre-process the data this may consist of:
        1.3.1 creating one hot labels - required
        1.3.2 standardize per pixel
        1.3.3 standardize per spectra
        1.3.4 expand data for under represented classes
        1.3.5 reduce data for under represented classes
    1.4. Plot the distributions of the data

2. Train Classifier
    2.1. Define a suitable CNN for classification
    2.2. Train the CNN classifier

3. Test Classifier
    3.1. Create Confusion Matrix (test the classifier)
    3.2. Create Kernel Impact Matrix (remove each kernel and identify respective impact)
        -Subsquent Analysis of Kernel should be performed by Kernel_Eval_Lib.py

4. Create Classified Maps
"""

import HSIcNet as HcN
import numpy as np
import time as time
import csv
import matplotlib.pyplot as plt
import os
import copy
from six.moves import cPickle as pickle
HCN = HcN.HSIcNet()

import gdal
from gdalconst import *
import sys
import os

import hsi_data as hsi_data
train = hsi_data.hsi_data()
valid = hsi_data.hsi_data()
hyspiri = hsi_data.hsi_data()

import logging
logging.basicConfig(filename='LOG_SVM_RF_Classifier_main.txt',level = logging.DEBUG, format= '%(asctime)s - %(levelname)s - %(message)s')
logging.debug('Log for RF_SVM_Class_3_Refact')

Import_Data = True
Train_RF_Classifier = False#True
Train_SVM_Classifier = False#True
Test_RF_Classifier = True
Test_SVM_Classifier = False
classify_data = False



#%%######################################
###1. IMPORT DATA
if Import_Data == True:
    logging.debug('1. Import Training Data')
    logging.debug('Import Data')

    #train.input_filename = '/home/ciga/CNN-Project/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_training_filtered.csv'
    #valid.input_filename = '/home/ciga/CNN-Project/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'

    train.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_training_filtered.csv'
    valid.input_filename = '/home/dgiudici/HSI/HSI_data/2015-Data/Bay_Area_2015_hyspiri_reflectance_30m_3season_testing.csv'

    train.data,train.labels, header,wavelength = train.load_data_any_3season(train.input_filename)
    valid.data,valid.labels, header,wavelength = valid.load_data_any_3season(valid.input_filename)
    #Randomize Data For Training and Testing
    train.data,train.labels = hsi_data.randomize(train.data,train.labels)
    train_unstd= copy.deepcopy(train)
    valid.data,valid.labels = hsi_data.randomize(valid.data,valid.labels)
    #Standardize the data (optional)
    _,train_scaler = train.standardize(scaler = 0)   #determine the scaler to use for all data also stored in train.scaler
    valid.scaler = train_scaler
    valid.standardize(scaler = 'exists')
    train.create_variants_labels() # important to do other wise data sets won't match after randomize
    valid.create_variants_labels()

    HCN.standardized = train.standardized
    HCN.num_labels = train.num_labels   #carry over variables these are required for cNet definition
    HCN.data_length = train.data_length #carry over variables these are required for cNet definition

    #%%
    train_bincount = np.bincount(train.labels_num)
    valid_bincount = np.bincount(valid.labels_num)
    num_classes_train = len(set(train.labels)) # this is the number of classes
    num_classes_valid = len(set(valid.labels))
    ind_train = np.arange(num_classes_train)                # the x locations for the groups
    ind_valid = np.arange(num_classes_valid)                # the x locations for the groups
    width = .35
    plt.figure(4)
    plt.suptitle('Distribution of Training and Validation Data Sets')
    plt.subplot(211)
    plt.title('Training')
    plt.ylabel('Count of Data of Each Class')
    plt.bar(ind_train-width/2, train_bincount, width,color='black')
    plt.xticks(range(len(ind_train)),ind_train)
    plt.xlabel('Class Index')
    plt.xlim([-width/2,num_classes_train-1+width/2])
    plt.grid(axis='both')

    plt.subplot(212)
    plt.title('Validation')
    plt.ylabel('Count of Data of Each Class')
    plt.bar(ind_train-width/2, valid_bincount, width,color='black')
    plt.xticks(range(len(ind_train)),ind_train)
    plt.xlabel('Class Index')
    plt.xlim([-width/2,num_classes_train-1+width/2])
    plt.grid(axis='both')
    plt.show()


#%%1. TRAIN CLASSIFIERS

import sklearn.svm as SVM
from sklearn.cross_validation import StratifiedShuffleSplit
from sklearn.grid_search import GridSearchCV
import numpy as np
import matplotlib.pyplot as plt
import matplotlib
from matplotlib.colors import Normalize
from sklearn.ensemble import RandomForestClassifier
#%%
if Train_RF_Classifier == True:
    #%## SKLEARN Implementation RANDOM FORESTS
    #
    # This section of code will run the randomforest classifier
    # The logistic regression takes in training data in the form of an array
    # and the labels as well and performs the classificatoin.
    #
    # forest.fit(train_x,train_y) "trains" the classifier
    # forest.score(valid_x, valid_y) "scores" the classifier
    # forest.predict(x) will "predict" the class based on
    #                                    the trained classifier and the input data
    #

    a = [1] #np.arange(25,150,25)
    for i in a:
        forest = RandomForestClassifier(n_estimators =1000,max_features='sqrt',min_samples_leaf = i, oob_score=True)
        #Convert the labels into numbers
        print 'Train the Random Forests Classifier'

        #what about doubling the size of the training data?
        #newtrainingdata = np.reshape([train_data_arry,train_data_arry],(train_data_arry.shape[0]*2,train_data_arry.shape[1]))
        #newtraininglabels = np.reshape([train_labels_num,train_labels_num],(train_labels_num.shape[0]*2))
        startTime = time.time()
        forest.fit(np.float32(train.data),train.labels_num)
        stopTime = time.time()


        print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
        ##%%
        print 'Score the Random Forest Classifier'
        startTime = time.time()
        score = forest.score(np.float32(valid.data),valid.labels_num)
        stopTime = time.time()
        print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
        print 'The classifier achieved and accuracy of : ', score
#%%
    arr = forest.feature_importances_
    print 'The top 10 features for random forest are as follows'
    arr.argsort()[-10:][::-1]

    #%%Save the RF Classifier

    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/RF-Classifier31MAR.PKL"
    pkl_lst =  forest
    pickle.dump(pkl_lst,open(filePath,"wb"))
    print "SAVED RF Clasifier to:", filePath


#%%Restore and Run the RF Classifier
filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/RF-Classifier31MAR.PKL"
pkl_file = open(filePath,'rb')
forest = pickle.load(pkl_file)
#%%
forest.score(np.float32(valid.data),valid.labels_num)

#%%
print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
##%%
print 'Score the Random Forest Classifier'
startTime = time.time()
score = forest.score(np.float32(valid.data),valid.labels_num)
stopTime = time.time()
print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
print 'The classifier achieved and accuracy of : ', score
#%%PLOT Forest Feature Importances
arr = forest.feature_importances_
arr = arr*100
fig1 = plt.figure(facecolor= 'white')
ax = fig1.add_subplot(111)
#plt.plot(arr)



indx_c_c_conifer = list(train_unstd.labels).index('Closed-Canopy Conifer')
#scaled_data = train_unstd.data[indx_c_c_conifer]/np.max(train_unstd.data[indx_c_c_conifer])/10+1
scaled_data = train_unstd.data[indx_c_c_conifer]/100

plt.title('Feature Importance for Random Forest Classification')
plt.xlabel('Spectral Band #')
plt.ylabel('Relative Importance out of 100%')
num_bands = len(arr)/3
rect1 = matplotlib.patches.Rectangle((num_bands*0,0), num_bands, np.max(arr)*1.1, alpha = .2, color='green') # (x,y), width, heigth
rect2 = matplotlib.patches.Rectangle((num_bands*1,0), num_bands, np.max(arr)*1.1, alpha = .2, color='yellow')
rect3 = matplotlib.patches.Rectangle((num_bands*2,0), num_bands, np.max(arr)*1.1, alpha = .2, color='grey')
ax.add_patch(rect1)
ax.add_patch(rect2)
ax.add_patch(rect3)
ax.annotate("Spring", xy=(0.15, 0.75), xycoords="axes fraction")
ax.annotate("Summer", xy=(0.45, 0.75), xycoords="axes fraction")
ax.annotate("Fall", xy=(0.8, 0.75), xycoords="axes fraction")
plt.ylim((0,np.max(arr)*1.1))

ax.plot(arr, label="RF Feature Importance")
ax.plot([0,0], 'grey', label = "Nominal Spectral Profile")

ax.legend()
ax2 = ax.twinx()
ax2.plot(scaled_data,'grey', label = "Nominal Spectral Profile")
#ax2.legend()
ax2.set_ylabel('% Reflectance')
ax2.set_ylim(-20,100)

#greyline = mlines.Line2D([],[], color = 'grey',label = 'Nominal Spectral Profile')
#blueline = mlines.Line2D([],[], color = 'blue',label = 'RF Feature Importance')
#plt.legend(handles= [greyline])
plt.xlim((0,len(arr)))
plt.grid()
#plt.legend()
print 'The top 10 features for random forest are as follows'
arr.argsort()[-10:][::-1]
#%%
plt.close('all')
#%%
if Train_SVM_Classifier == True:
    #%%## SKLEARN Implementation SUPPORT VECTOR MACHINE (SVM)
    #
    # This section of code will run the randomforest classifier
    # The logistic regression takes in training data in the form of an array
    # and the labels as well and performs the classificatoin.
    #
    # forest.fit(train_x,train_y) "trains" the classifier
    # forest.score(valid_x, valid_y) "scores" the classifier
    # forest.predict(x) will "predict" the class based on
    #                                    the trained classifier and the input data
    #
    print '############################'
    print 'TRAIN THE SVM CLASSIFIER'
    print '############################'
    print '############################'
    print 'LINEAR SEARCH THROUGH C PARAM'
    print '############################'
    C_range = np.logspace(-2,10,13)

    #    for i in  C_range:
    #        clf = SVM.LinearSVC(C=i) #Default Training Params
    #            #gamma [1e-9,1e3]
    #            #c [e-2, s]
    #
    #        #Convert the labels into numbers
    #        print 'Train the SVM Classifier : i = ', i
    #
    #        startTime = time.time()
    #        clf.fit(np.float32(train.data),train.labels_num)
    #        stopTime = time.time()
    #        print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
    #        ##%% This classifier achieved xxx
    #        print 'Score the SVM SVMClassifier'
    #        startTime = time.time()
    #        score = clf.score(np.float32(valid.data),valid.labels_num)
    #        stopTime = time.time()
    #        print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
    #        print 'The classifier achieved and accuracy of : ', score
    #%%
    #A linear search was already performed to capture that i = .1 best classifier
    i = .10
    clf = SVM.LinearSVC(C=i) #Default Training Params
    startTime = time.time()
    clf.fit(np.float32(train.data),train.labels_num)
    stopTime = time.time()
    print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
    ##%% This classifier achieved xxx
    print 'Score the SVM SVMClassifier'
    startTime = time.time()
    score = clf.score(np.float32(valid.data),valid.labels_num)
    stopTime = time.time()
    print 'The Script took ' + str( stopTime-startTime ) + ' seconds'
    print 'The classifier achieved and accuracy of : ', score

    #%%Save the SVM Classifier
    '''
    '''
    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/SVM-Classifier31MAR.PKL"
    pkl_lst = clf
    pickle.dump(pkl_lst,open(filePath,"wb"))
    print "SAVED RF Clasifier to:", filePath
    '''
    '''

    #%%Restore and Run the SVM Classifier
    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/SVM-Classifier31MAR.PKL"
    pkl_file = open(filePath,'rb')
    SVM_clf = pickle.load(pkl_file)
    pkl_file.close()


    SVM_clf.score(np.float32(valid.data),valid.labels_num)
    #%%


if Test_RF_Classifier == True:
    #%%Restore and Run the RF Classifier
    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/RF-Classifier31MAR.PKL"
    pkl_file = open(filePath,'rb')
    forest = pickle.load(pkl_file)
    forest.score(np.float32(valid.data),valid.labels_num)

    #%%VALIDATION OF RANDOM FOREST CLASSIFIER
    #The validation data should be the same as the HSI data as well.
    #currently the first 500 data points from each class is used to create a
    #balenced confusion matrix - refer to HCN.confusion_matrix()
    #HCN.sorted_full_data
    zipped_label_data = zip(valid.labels_num,valid.data)
    #x = [x for y,x in np.sort(zip(train_labels_arry,train_data_arry),]
        #sort based off of the first entery in the zipped_label_data
    zipped_label_data = sorted(zipped_label_data,key=lambda class_data:class_data[0])


    sorted_valid_labels_num = [zipped_label_data[x][0] for x in range(len(zipped_label_data))]
    sorted_valid_data = [zipped_label_data[x][1] for x in range (len(zipped_label_data))]
    #sorted_valid_one_hot_labels = HcN.reformatOneHot(sorted_valid_labels_num,HCN.num_labels)

    #%%
    strt_idx = 0
    stop_idx = 0
    confus_data = []
    confus_data_one_hot = []
    bincounts = np.bincount(sorted_valid_labels_num)
    per_class_accuracy = []
    full_label_range = []

    print ">>>>>>>>>for each class capture the predictions<<<<<<<<"
    for i in range(HCN.num_labels):
        strt_idx = np.sum(bincounts[:i])
        stop_idx = np.sum(bincounts[:i+1])
        num_bal = np.min(bincounts)
        data_range = np.copy(sorted_valid_data[strt_idx:strt_idx+num_bal])
        label_range = np.copy(sorted_valid_labels_num[strt_idx:strt_idx+num_bal])

        prediction = forest.predict(data_range)
        ###CLASSIFIER
        ###prediction,run_duration = self.run_graph(data_range,label_range)
                                        #graph,
                                        #tf_ph_dataset,
                                        #tf_ph_labels,
                                        #prediction_op,
                                        #data_range,
                                        #label_range,
                                        #batch_size)
        prediction_arry = np.asarray(prediction)
        per_class_accuracy.append(HcN.accuracy(HcN.reformatOneHot(prediction_arry,HCN.num_labels), HcN.reformatOneHot(label_range,HCN.num_labels)))
        print 'the accuracy for class : ', i, ' is ',  per_class_accuracy[i]
        confus_data_one_hot.append(HcN.reformatOneHot(prediction,HCN.num_labels))
        confus_data.append(prediction)
        full_label_range.append(label_range)

    confus_data_arry =  np.asarray(confus_data)
    flt_confus_data_arry = np.reshape(confus_data_arry,confus_data_arry.shape[0]*confus_data_arry.shape[1])
    full_label_range_arry = np.asarray(full_label_range)
    flt_full_label_range_arry = np.reshape(full_label_range_arry,full_label_range_arry.shape[0]*full_label_range_arry.shape[1])
    conf_mat = np.zeros((HCN.num_labels,HCN.num_labels))
    #%%
    for x in range(len(flt_confus_data_arry)):
        print "confus_data_arry[x]: ",flt_confus_data_arry[x]," full_label_range_arry[x]: ",flt_full_label_range_arry[x]
        conf_mat[flt_full_label_range_arry[x]][flt_confus_data_arry[x]] += 1
    conf_mat = np.asarray(conf_mat,dtype=int)
    #%%
    fig = HCN.plot_confusion_matrix(conf_mat,HCN.num_labels,plot_note = 'RF-Balanced')


    #%%
    ###FOR UNBALANCED CONF MAT
    strt_idx = 0
    stop_idx = 0
    confus_data = []
    confus_data_one_hot = []
    bincounts = np.bincount(sorted_valid_labels_num)
    per_class_accuracy = []
    full_label_range = []

    print ">>>>>>>>>for each class capture the predictions<<<<<<<<"
    for i in range(HCN.num_labels):
        strt_idx = np.sum(bincounts[:i])
        stop_idx = np.sum(bincounts)
        if i != HCN.num_labels-1:
            stop_idx = np.sum(bincounts[:i+1])
        #num_bal = np.min(bincounts)
        data_range = np.copy(sorted_valid_data[strt_idx:stop_idx])
        label_range = np.copy(sorted_valid_labels_num[strt_idx:stop_idx])

        prediction = forest.predict(data_range)
        ###CLASSIFIER
        ###prediction,run_duration = self.run_graph(data_range,label_range)
                                        #graph,
                                        #tf_ph_dataset,
                                        #tf_ph_labels,
                                        #prediction_op,
                                        #data_range,
                                        #label_range,
                                        #batch_size)
        prediction_arry = np.asarray(prediction)
        per_class_accuracy.append(HcN.accuracy(HcN.reformatOneHot(prediction_arry,HCN.num_labels), HcN.reformatOneHot(label_range,HCN.num_labels)))
        print 'the accuracy for class : ', i, ' is ',  per_class_accuracy[i]
        confus_data_one_hot.append(HcN.reformatOneHot(prediction,HCN.num_labels))
        confus_data.append(prediction)
        full_label_range.append(label_range)
        print 'class: ', i
    #%%

    def flatten(array):
        """
            Returns a list o flatten elements of every inner lists (or tuples)
            ****RECURSIVE****
        """
        res = []
        for el in array:
            res.extend(el)
        return res


        #%%
    confus_data_arry =  np.asarray(confus_data)
    confus_data_arry.shape
    #%%
    flt_confus_data_arry = np.asarray(flatten(confus_data_arry[:])) #np.reshape(confus_data_arry,-1)
    flt_confus_data_arry.shape

    #%%
    full_label_range_arry = np.asarray(full_label_range)
    #%%
    flt_full_label_range_arry = np.asarray(flatten(full_label_range_arry))

    conf_mat = np.zeros((HCN.num_labels,HCN.num_labels))

    #%%
    for x in range(len(flt_confus_data_arry)):
        print "confus_data_arry[x]: ",flt_confus_data_arry[x]," full_label_range_arry[x]: ",flt_full_label_range_arry[x]
        conf_mat[flt_full_label_range_arry[x]][flt_confus_data_arry[x]] += 1
    conf_mat = np.asarray(conf_mat,dtype=int)
    #%%
    #fig = HCN.plot_confusion_matrix(conf_mat,HCN.num_labels,plot_note = 'RF-UnBalanced')
    fig = HCN.plot_confusion_matrix(conf_mat,HCN.num_labels,plot_note = 'RF')


#%%


if Test_SVM_Classifier == True:
    #%%Restore and Run the SVM Classifier
    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/SVM-Classifier31MAR.PKL"
    pkl_file = open(filePath,'rb')
    SVM_clf = pickle.load(pkl_file)
    pkl_file.close()
    SVM_clf.score(np.float32(valid.data),valid.labels_num)
    #%%VALIDATION OF SVM CLASSIFIER
    #The validation data should be the same as the HSI data as well.
    #currently the first minmum bincount of  data points from each class is used to create a
    #balenced confusion matrix - refer to HCN.confusion_matrix()
    #HCN.sorted_full_data
    zipped_label_data = zip(valid.labels_num,valid.data)
    #x = [x for y,x in np.sort(zip(train_labels_arry,train_data_arry),]
        #sort based off of the first entery in the zipped_label_data
    zipped_label_data = sorted(zipped_label_data,key=lambda class_data:class_data[0])


    sorted_valid_labels_num = [zipped_label_data[x][0] for x in range(len(zipped_label_data))]
    sorted_valid_data = [zipped_label_data[x][1] for x in range (len(zipped_label_data))]
    #sorted_valid_one_hot_labels = HcN.reformatOneHot(sorted_valid_labels_num,HCN.num_labels)

    #%%
    strt_idx = 0
    stop_idx = 0
    confus_data = []
    confus_data_one_hot = []
    bincounts = np.bincount(sorted_valid_labels_num)
    per_class_accuracy = []
    full_label_range = []

    print ">>>>>>>>>for each class capture the predictions<<<<<<<<"
    for i in range(HCN.num_labels):
        strt_idx = np.sum(bincounts[:i])
        stop_idx = np.sum(bincounts[:i+1])
        num_bal = np.min(bincounts)
        data_range = np.copy(sorted_valid_data[strt_idx:strt_idx+num_bal])
        label_range = np.copy(sorted_valid_labels_num[strt_idx:strt_idx+num_bal])

        prediction = SVM_clf.predict(data_range)
        ###CLASSIFIER
        ###prediction,run_duration = self.run_graph(data_range,label_range)
                                        #graph,
                                        #tf_ph_dataset,
                                        #tf_ph_labels,
                                        #prediction_op,
                                        #data_range,
                                        #label_range,
                                        #batch_size)
        prediction_arry = np.asarray(prediction)
        per_class_accuracy.append(HcN.accuracy(HcN.reformatOneHot(prediction_arry,HCN.num_labels), HcN.reformatOneHot(label_range,HCN.num_labels)))
        print 'the accuracy for class : ', i, ' is ',  per_class_accuracy[i]
        confus_data_one_hot.append(HcN.reformatOneHot(prediction,HCN.num_labels))
        confus_data.append(prediction)
        full_label_range.append(label_range)

    confus_data_arry =  np.asarray(confus_data)
    flt_confus_data_arry = np.reshape(confus_data_arry,confus_data_arry.shape[0]*confus_data_arry.shape[1])
    full_label_range_arry = np.asarray(full_label_range)
    flt_full_label_range_arry = np.reshape(full_label_range_arry,full_label_range_arry.shape[0]*full_label_range_arry.shape[1])
    conf_mat = np.zeros((HCN.num_labels,HCN.num_labels))
    #%%
    for x in range(len(flt_confus_data_arry)):
        print "confus_data_arry[x]: ",flt_confus_data_arry[x]," full_label_range_arry[x]: ",flt_full_label_range_arry[x]
        conf_mat[flt_full_label_range_arry[x]][flt_confus_data_arry[x]] += 1
    conf_mat = np.asarray(conf_mat,dtype=int)
    #%%
    fig = HCN.plot_confusion_matrix(conf_mat,HCN.num_labels,plot_note = 'SVM-Balanced')


    #%%
    ###FOR UNBALANCED CONF MAT
    strt_idx = 0
    stop_idx = 0
    confus_data = []
    confus_data_one_hot = []
    bincounts = np.bincount(sorted_valid_labels_num)
    per_class_accuracy = []
    full_label_range = []

    print ">>>>>>>>>for each class capture the predictions<<<<<<<<"
    for i in range(HCN.num_labels):
        strt_idx = np.sum(bincounts[:i])
        stop_idx = np.sum(bincounts)
        if i != HCN.num_labels-1:
            stop_idx = np.sum(bincounts[:i+1])
        #num_bal = np.min(bincounts)
        data_range = np.copy(sorted_valid_data[strt_idx:stop_idx])
        label_range = np.copy(sorted_valid_labels_num[strt_idx:stop_idx])

        prediction = SVM_clf.predict(data_range)
        ###CLASSIFIER
        ###prediction,run_duration = self.run_graph(data_range,label_range)
                                        #graph,
                                        #tf_ph_dataset,
                                        #tf_ph_labels,
                                        #prediction_op,
                                        #data_range,
                                        #label_range,
                                        #batch_size)
        prediction_arry = np.asarray(prediction)
        per_class_accuracy.append(HcN.accuracy(HcN.reformatOneHot(prediction_arry,HCN.num_labels), HcN.reformatOneHot(label_range,HCN.num_labels)))
        print 'the accuracy for class : ', i, ' is ',  per_class_accuracy[i]
        confus_data_one_hot.append(HcN.reformatOneHot(prediction,HCN.num_labels))
        confus_data.append(prediction)
        full_label_range.append(label_range)
        print 'class: ', i
    #%%
    confus_data_arry =  np.asarray(confus_data)
    confus_data_arry.shape
    #%%
    flt_confus_data_arry = np.asarray(flatten(confus_data_arry[:])) #np.reshape(confus_data_arabels, header_valid = HCN.load_data_any_3season(HCN.valid_data_filename)
    #%%

    #---delete this HCN.train_data,HCN.train_labels = HcN.randomize(HCN.train_data,HCN.train_labels)
    #---delete this flt_confus_data_arry.shape

    #%%
    full_label_range_arry = np.asarray(full_label_range)
    #%%
    flt_full_label_range_arry = np.asarray(flatten(full_label_range_arry))

    conf_mat = np.zeros((HCN.num_labels,HCN.num_labels))

    #%%
    for x in range(len(flt_confus_data_arry)):
        print "confus_data_arry[x]: ",flt_confus_data_arry[x]," full_label_range_arry[x]: ",flt_full_label_range_arry[x]
        conf_mat[flt_full_label_range_arry[x]][flt_confus_data_arry[x]] += 1
    conf_mat = np.asarray(conf_mat,dtype=int)
    #%%
    #fig = HCN.plot_confusion_matrix(conf_mat,HCN.num_labels,plot_note = 'SVM-UnBalanced')
    fig = HCN.plot_confusion_matrix(conf_mat,HCN.num_labels,plot_note = 'SVM')
#%%

if classify_data == True:
    ################################################################################
    ##Process many multisegment files RF and SVM
    #
    #import os
    start_time = time.time()

    files_to_process = []

    logging.debug('Process HSI MultiFile')
    print ">>>>>>>>Process HSI MultiFile"

    #%%
    folder = '/media/dgiudici/Seagate Backup Plus Drive/raw_2015_data/'
    hyspiri.hyspiri_filepath = folder

    #%%Restore and Run the SVM Classifier
    ####################################

    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/SVM-Classifier31MAR.PKL"
    pkl_file = open(filePath,'rb')
    SVM_clf = pickle.load(pkl_file)
    pkl_file.close()
    SVM_clf.score(np.float32(valid.data),valid.labels_num)

    #%%Restore and Run the RF Classifier
    ####################################
    filePath = "/home/dgiudici/repos/hsi-cnn-repo/TrainedClassifiers/RF-Classifier31MAR.PKL"
    pkl_file = open(filePath,'rb')
    RF_clf = pickle.load(pkl_file)
    RF_clf.score(np.float32(valid.data),valid.labels_num)
    hyspiri.hyspiri_filepath = filePath
    #%%
    data_chunk = 7500 #This chunk is the max size for the graph (suspect graphics memory limitation)
    HCN.batch_size = data_chunk

    #%%
    ####################################
    ##Capture the files
    for file in os.listdir(folder):
        if file.endswith('m'):
            files_to_process.append(file)

    #%%

    #process_single_file

    #files_to_process = ["BayArea_53-1_f150430t01p00r12_f150611t01p00r14_f151002t01p00r12_masked_30m"]

    ###################################
    ##Process each file
    run_duration = ' no_run_duration '
    for i in files_to_process:
        print 'processing: ' , i
        logging.debug('Processing: ' + i )
        T1 = time.time()
        hyspiri.hyspiri_filename = i
        filename = folder + i
        scaler = train.scaler
        #HCN.classify_envi_data(filename = hyspiri.hyspiri_filepath+ str(i) ,scaler = train.scaler)
        #insert classify envi data for non HCN classifiers
        """This function will take in filename and create a geotiff output it will also return a classified set of pixels"""
        logging.debug('classify_envi_data - SVM and RF Files')
        print ">>>>>>>>classify envi data<<<<<<<<"

        hyspiri = hsi_data.hsi_data()
        data_chunk = HCN.batch_size
        hyspiri.hyspiri_filename = os.path.basename(filename)

        ds = gdal.Open(filename)
        if ds is None:
            print 'Could not open ' + filename
            #sys.exit(1)

        cols = ds.RasterXSize
        rows = ds. RasterYSize
        bands = ds.RasterCount

        print "cols: ", cols," rows: ",rows," bands: ",bands
        logging.debug('cols: '+str(cols)+' rows: '+str(rows)+' bands: ' + str(bands))

        geotransform = ds.GetGeoTransform()
        originX = geotransform[0]
        originY = geotransform[3]
        pixelWidth = geotransform[1]
        pixelHeight = geotransform[5]
        start_x, start_y = 0,0
        RF_total_pred = []
        SVM_total_pred = []
        mask = []

        t1 = time.time()
        classed_rows = 0
        hyspiri.scaler = scaler

        num_rows_per_classification = 200 #Arbitrary it just needs to fit within memory
        while classed_rows < rows: #for j in range(rows): #(rows):
            if classed_rows > rows - num_rows_per_classification:
                logging.debug('In the last Chunk classed_rows = '+str(classed_rows))
                num_rows_per_classification = rows - classed_rows
                logging.debug('The last chunk contiains: '+str(num_rows_per_classification) +' rows')
                start_y =classed_rows
                hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
                hyspiri.data = np.swapaxes(hyspiri.data,0,1)
                hyspiri.data = np.swapaxes(hyspiri.data,1,2)
                hyspiri.data,frst,scnd,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
                #for i in range(len(hyspiri.data)):
                #    if hyspiri.data[i].all() == 0:
                #        mask.append(0)
                #    else:
                #        mask.append(1)
                for i in range(np.asarray(hyspiri.data).shape[0]):
                    if hyspiri.data[i].any() == 1:
                        mask.append(1)
                    else:
                        mask.append(0)
                hyspiri.data,_ = hyspiri.standardize(scaler = 'exists')#self.standardize_by_spec(hyspiri.data)
                # Classify - The Remainder of the data
                # Input Batch_Size that is appropriate to the system
                # Input classification_data
                # Initizlied self
                classed_data = 0
                while classed_data < hyspiri.data.shape[0]:
                    if classed_data > hyspiri.data.shape[0] - HCN.batch_size:
                        HCN.batch_size = hyspiri.data.shape[0] - classed_data
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+HCN.batch_size,:])
                        #HCN# batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        #HCN# batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #HCN# prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        #HCN# prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        SVM_prediction = SVM_clf.predict(batch_data)
                        RF_proba  = RF_clf.predict_proba(batch_data) #RF_clf.predict(batch_data) = RF_prediction_num = RF_proba.argmax(1)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        HCN.batch_size = data_chunk
                        classed_data += HCN.batch_size
                    else:
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                        #HCN# #batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        #HCN# #batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #HCN# #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        #HCN# prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        SVM_prediction = SVM_clf.predict(batch_data)
                        RF_proba  = RF_clf.predict_proba(batch_data) #RF_clf.predict(batch_data) = RF_prediction_num = RF_proba.argmax(1)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        classed_data += HCN.batch_size

                    #HCN# total_pred.extend(prediction[0])
                    SVM_total_pred.extend(SVM_prediction)
                    RF_total_pred.extend(RF_proba)
                classed_rows += num_rows_per_classification
                logging.debug('classed_rows = '+str(classed_rows))

                print 'Classified Line: ', classed_rows, 'out of :', rows

            else:
                logging.debug('In std total Rows Loop i = '+str(classed_rows))

                start_y =classed_rows
                hyspiri.data = ds.ReadAsArray(start_x,start_y,cols,num_rows_per_classification)
                hyspiri.data = np.swapaxes(hyspiri.data,0,1)
                hyspiri.data = np.swapaxes(hyspiri.data,1,2)

                hyspiri.data,frst,scnd,thrd = hyspiri.remove_bad_bands_hyspiri(np.reshape(hyspiri.data,(hyspiri.data.shape[0]*hyspiri.data.shape[1],hyspiri.data.shape[2])))
                logging.debug('size of block classifying :' +str(sys.getsizeof(hyspiri.data)))
                #for i in range(np.asarray(hyspiri.data).shape[0]):
                #    if hyspiri.data[i].all() == 0:
                #        mask.append(0)
                #    else:
                #        mask.append(1)
                for i in range(np.asarray(hyspiri.data).shape[0]):
                    if hyspiri.data[i].any() == 1:
                        mask.append(1)
                    else:
                        mask.append(0)
                hyspiri.data,_ = hyspiri.standardize(scaler = 'exists')
                # Classify - Full Frame
                # Input Batch_Size that is appropriate to the system
                # Input classification_data
                # Initizlied self

                classed_data = 0
                while classed_data < hyspiri.data.shape[0]:
                    if classed_data > hyspiri.data.shape[0] - HCN.batch_size:
                        HCN.batch_size = hyspiri.data.shape[0] - classed_data
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+HCN.batch_size,:])
                        #HCN# #batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        #HCN# #batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #HCN# #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        #HCN# #prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        SVM_prediction = SVM_clf.predict(batch_data)
                        RF_proba  = RF_clf.predict_proba(batch_data) #RF_clf.predict(batch_data) = RF_prediction_num = RF_proba.argmax(1)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        HCN.batch_size = data_chunk
                        classed_data += HCN.batch_size
                    else:
                        batch_data = np.copy(hyspiri.data[classed_data:classed_data+data_chunk,:])
                        #HCN# #batch_data = np.reshape(batch_data,[batch_data.shape[0],batch_data.shape[1],1,1])
                        #HCN# #batch_label = np.zeros((batch_data.shape[0],self.num_labels),dtype = float)
                        #HCN# #prediction,run_duration,_ = self.run_graph(batch_data,batch_label)
                        #HCN# #prediction,run_duration,_ = self.run_graph_open_session(batch_data,batch_label)
                        SVM_prediction = SVM_clf.predict(batch_data)
                        RF_proba  = RF_clf.predict_proba(batch_data) #RF_clf.predict(batch_data) = RF_prediction_num = RF_proba.argmax(1)
                        logging.debug('run_duration (s) = '+str(run_duration)+ ' classed_data = ' + str(classed_data))
                        #print 'classified : ' , classed_data/data_chunk, ' data chuncks of: ', rows
                        classed_data += HCN.batch_size

                    #HCN# total_pred.extend(prediction[0])
                    SVM_total_pred.extend(SVM_prediction)
                    RF_total_pred.extend(RF_proba)
                classed_rows += num_rows_per_classification
                logging.debug('classed_rows = '+str(classed_rows))
                print 'Classified Lines: ', classed_rows , 'out of :', rows

        t2 = time.time()
        t3 = t2-t1
        logging.debug('Total Time to classify file: '+str(t3))

        print "DONE Classifying The File : ", i," out of : "
        for j in files_to_process:
            print j
        #% Format the Classified Probabilities For Support Vector Machine SVM

        mask = np.asarray(mask)
        SVM_total_pred = np.asarray(SVM_total_pred)+1
        SVM_pred_nums = np.asarray([SVM_total_pred[i]*mask[i] for i in range(len(SVM_total_pred))]) #apply mask

        bin = np.bincount(SVM_pred_nums)
        plt.figure()
        plt.title('SVM-Classification Distribution')
        plt.stem(bin)

        B = np.reshape(SVM_pred_nums,(rows,cols)) #Class
        #HCN# C = np.reshape(RF_pred_confid,(rows,cols)) #Confidence

        #HCN# D = np.reshape(RF_pred_prob, (rows,cols,cnn_pred_prob.shape[1])) #Per Class Confidence
        #HCN# D = np.uint8(D*100)
        #%Plot the map output from classification
        #%
        plt.figure()
        plt.title('SVM-Map'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(B))
        BB = np.uint8(B)

        """#HCN#
        plt.figure()
        plt.title('RF-Confidences'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(C*100))
        CC = np.uint8(C*100)
        """

        #%GTiff -Output - 2 band
        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())
        outfile = driver.Create("SVM-Class-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, 1, GDT_Byte)
        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        outfile.GetRasterBand(1).WriteArray(BB, 0, 0)
        #HCN# outfile.GetRasterBand(2).WriteArray(CC, 0, 0)

        outfile = None
        #%GTIFF - Output - Multi Band Confidences
        #NOT APPLICABLE FOR SVM NO CONFIENCES#
        print 'Done Creating the SVM Map'
        stop_time = time.time()
        print "the time it took to classify:", files_to_process[0] , " is: ", stop_time - start_time
        #


        #% Format the Classified Probabilities For Random Forest
        mask = np.asarray(mask)
        RF_pred_prob = np.asarray(RF_total_pred)
        RF_pred_prob = np.asarray([RF_pred_prob[i]*mask[i] for i in range(len(RF_pred_prob))]) #apply mask
        RF_pred_nums = np.argmax(RF_pred_prob,axis=1)+1
        RF_pred_nums = np.asarray([RF_pred_nums[i]*mask[i] for i in range(len(RF_pred_nums))]) #apply mask
        RF_pred_confid = np.max(RF_pred_prob,axis=1)
        RF_pred_confid = np.asarray([RF_pred_confid[i]*mask[i] for i in range(len(RF_pred_confid))]) #apply mask

        bin = np.bincount(RF_pred_nums)
        plt.figure()
        plt.title('RF-Classification Distribution')
        plt.stem(bin)

        B = np.reshape(RF_pred_nums,(rows,cols)) #Class
        C = np.reshape(RF_pred_confid,(rows,cols)) #Confidence

        D = np.reshape(RF_pred_prob, (rows,cols,RF_pred_prob.shape[1])) #Per Class Confidence
        D = np.uint8(D*100)
        #%Plot the map output from classification
        plt.figure()
        plt.title('RF-Map'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(B))
        BB = np.uint8(B)

        plt.figure()
        plt.title('RF-Confidences'+hyspiri.hyspiri_filename)
        plt.imshow(np.uint8(C*100))
        CC = np.uint8(C*100)

        #%GTiff -Output - 2 band
        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())
        outfile = driver.Create("RF-ClassConfid-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, 2, GDT_Byte)
        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        outfile.GetRasterBand(1).WriteArray(BB, 0, 0)
        outfile.GetRasterBand(2).WriteArray(CC, 0, 0)

        outfile = None
        #%GTIFF - Output - Multi Band Confidences
        driver = gdal.GetDriverByName("GTiff")
        #driver.Register()
        driver.SetMetadata(ds.GetMetadata())

        numD = 12 #D.shape[2]  # weird quark with gdal... doesn't like accepting a D.shape[2]
        outfile = driver.Create("RF-Confidences-"+str(hyspiri.hyspiri_filename)+".tiff", cols, rows, numD , GDT_Byte)

        outfile.SetGeoTransform(geotransform)
        outfile.SetProjection(ds.GetProjectionRef())
        for i in range(numD):
            tmp = D[:,:,i]
            outfile.GetRasterBand(int(i+1)).WriteArray(tmp, 0, 0)
        outfile = None

        print 'Done Creating the RF Map'

        T2 = time.time()
        T3 = T2-T1
        logging.debug('Processing of',i,' file for SVM and RF Complete, it took: ' + str(T3) + ' seconds')
    #%%
    print 'Done creating all the maps'